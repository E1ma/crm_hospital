package crm.businesslogic.interfaces;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import crm.businessentity.CrmInfo;
import crm.businessentity.LoggedUser;
import crm.entity.*;

@Local
public interface ILogicCrmLocal {
	public void saveCustomer(CrmCustomer customer) throws Exception;
	public List<CrmCustomerFamily> getCustomerFamily(BigDecimal customerPkId) throws Exception;
	public BigDecimal setCustomerFamily(CrmCustomerFamily family) throws Exception;
	public List<CrmCustomerInfo> getCustomerInfo(BigDecimal customerPkId) throws Exception;
	public BigDecimal setCustomerInfo(CrmCustomerInfo Info) throws Exception;
	public List<CrmCustomerContactHistory> getCustomerContactHistory(BigDecimal customerPkId) throws Exception;
	public BigDecimal setCustomerContactHistory(CrmCustomerContactHistory Info) throws Exception;
	public boolean getCustomerByPkId(BigDecimal customerPkId) throws Exception;
	public List<CrmInfo> getTreatmentInfo(BigDecimal customerPkId) throws Exception;
	public List<CrmInfo> getExaminationResult(BigDecimal examPkId) throws Exception;
	public List<CrmCustomer> getAllCrmCustomer() throws Exception;
	public void deleteCrmCustomer(CrmCustomer customer) throws Exception;
	public List<Customer> getListCustomer(List<String> listCus) throws Exception;
	public List<String> getListCrmCustomer(List<BigDecimal> listCusPkId) throws Exception;
	public void insertCrmCustomerList(List<CrmCustomer> listCustomer) throws Exception;
	public List<CrmCustomer> getCustomerSearch(String cardNumber, String customerName, String phoneNumber, String customerAddress,
			String lifeCategory, boolean notInfo, boolean infoSms, boolean infoMail) throws Exception;
	public List<CrmChannelInfo> getChannelInfo(String channelType, String titleSearch, String valueSearch, String isActive) throws Exception;
	public void saveSmsInfo(CrmChannelInfo smsInfo, LoggedUser user) throws Exception;
	public List<CrmTreatmentCondition> getCrmTreatmentList() throws Exception;
	public void saveConditionTreatment(CrmTreatmentCondition condition, List<CrmTreatmentConditionDtl> listDtl) throws Exception;
	
	public <T> List<T> getByAnyField(Class<T> type, String field, Object fieldValue) throws Exception;
	public List<CrmTreatmentConditionResult> getListResult(Date searchDate, String conditionType) throws Exception;
	public List<Employee> getEmployees() throws Exception;
	public List<CrmTreatmentConditionResultDtl> getResultDtlList(BigDecimal resultPkId, BigDecimal subOrgPkId, BigDecimal searchEmployeePkId, String searchCustomerName) throws Exception;
	public <T> List<T> getAll(Class<T> type) throws Exception;
	public void autoSendMailOrSms() throws Exception;
}