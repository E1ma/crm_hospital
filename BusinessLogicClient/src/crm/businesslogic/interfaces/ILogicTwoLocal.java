package crm.businesslogic.interfaces;

import java.math.BigDecimal;

import javax.ejb.Local;

import crm.businessentity.LoggedUser;

@Local
public interface ILogicTwoLocal {
	
	public void changePassword(LoggedUser loggedUser, String newPassword) throws Exception;
}
