package crm.businesslogic.interfaces;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import crm.businessentity.CrmInfo;
import crm.businessentity.LoggedUser;
import crm.entity.*;

@Local
public interface ILogicAllCrmLocal {

	void saveEmployeeInfo(Employee employeeInfo, Users userInfo, LoggedUser user)throws Exception;
	List<SubOrganization> getListSubOrganization() throws Exception;
	List<Employee> getListOfEmployees() throws Exception;
	boolean checkRegNumber(String regNumber) throws Exception;
	List<CrmTreatmentCondition> getListCrmTreatmentCondition() throws Exception;
	List<CrmAutoSendConfig> getListOfCrmAutoSendConfig() throws Exception;
	List<SystemConfig> getSystemConfig() throws Exception;
	void saveSendingConfig(CrmAutoSendConfig crmAutoSendConfig,
			SystemConfig sysConf, LoggedUser user) throws Exception;
	void saveSendingConfig(CrmAutoSendConfig crmAutoSendConfig, LoggedUser user) throws Exception;
	
}