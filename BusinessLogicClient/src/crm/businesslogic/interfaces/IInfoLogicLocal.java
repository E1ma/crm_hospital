package crm.businesslogic.interfaces;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import crm.businessentity.LoggedUser;
import crm.entity.*;

@Local
public interface IInfoLogicLocal {
	
	public List<CrmCustomer> getCrmCustomers(int first, int count, String sortField, String sortType, Map<String, String> filters) throws Exception;
	public long getCrmCustomerCount(Map<String, String> filters) throws Exception;
	public List<SubOrganization> getListSubOrganization() throws Exception;
	public List<Menu> getMenus() throws Exception;
	public List<Menu> getMenusFilter(LoggedUser loggedUser) throws Exception;
	public List<Menu> getMenus(LoggedUser loggedUser) throws Exception;
	public List<Menu> getListMenus(LoggedUser loggedUser) throws Exception;
	public SystemConfig getLicense() throws Exception;
	public void saveLicense(SystemConfig config) throws Exception;
	public List<Customer> getCustomers(int first, int count, String sortField, String sortType, Map<String, String> filters)
			throws Exception;
	public long getCustomerCount(Map<String, String> filters) throws Exception;
	public <T> List<T> getAll(Class<T> type) throws Exception;
	public List<Employee> getEmployeeInfo() throws Exception;
}
