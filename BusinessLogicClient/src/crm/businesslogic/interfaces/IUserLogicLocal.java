package crm.businesslogic.interfaces;

import javax.ejb.Local;

import crm.businessentity.LoggedUser;

@Local
public interface IUserLogicLocal {
	public void logicTest() throws Exception;
	public LoggedUser login(LoggedUser loggedUser) throws Exception;
}
