package crm.businesslogic.interfaces;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import crm.businessentity.LoggedUser;
import crm.entity.*;

@Local
public interface ILogicHomeLocal {
	public List<Organization> getOrganizations() throws Exception;
	public List<Role> getAccessLaw() throws Exception;
	public List<Users> getUsers() throws Exception;
	public List<SubOrganization> getRoom() throws Exception;
	public List<Employee> getEmployee() throws Exception;
	
	public List<Customer> getCustomer() throws Exception;
	public List<SubOrganization> getSelectionSubOrganization() throws Exception;
	public List<Employee> getSelectionEmployee( List<BigDecimal > keyValue) throws Exception;
	public void setPostSave(Post p,String text,BigDecimal bigDecimal,List<BigDecimal>  receiverPkIds,Date  time) throws Exception;
	public List<Post> getPostView(BigDecimal id)throws Exception;
	public List<Post>  getPostMapReceiverPkId(BigDecimal  pkId) throws  Exception;
	public void setPostDeleteModify(Post  p)throws Exception;
	public List<Post> getPostPostPkId(BigDecimal id)throws Exception;
	public List<Post> getPostPostUpdate(BigDecimal id)throws Exception;
	public List<Post> getPostPostPkIdView(BigDecimal id)throws Exception;
	public List<UpgradingLog> getUpgrades() throws Exception;
	public void saveUpgradeLog(UpgradingLog log ,LoggedUser user) throws Exception;
	public UserRoleMap getLoggedUserRole(LoggedUser user) throws Exception;
}
