package crm.businesslogic;

import base.*;
import crm.businessentity.CrmInfo;
import crm.businessentity.LoggedUser;
import crm.businessentity.Tool;
import crm.businesslogic.interfaces.ILogicCrmLocal;
import crm.entity.*;

import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.SessionContext;

@Stateless(name = "LogicCrm", mappedName = "crm.businesslogic.LogicCrm")
public class LogicCrm extends base.BaseData
		implements crm.businesslogic.interfaces.ILogicCrm, ILogicCrmLocal {

	@Resource
	SessionContext sessionContext;
	
	public LogicCrm() {
		super("CRM");
	}

	public void setDataBaseInfo() throws Exception {
		// dataBaseName = "Hospital"
	}
	
	/**
	 * Mail, SMS АВТОМАТААР ИЛГЭЭХ ХЭСЭГ
	 * */
	@SuppressWarnings("deprecation")
	@Override
	public void autoSendMailOrSms() throws Exception
	{
		Date nowDate = new Date();
		Date beginDate = null;
		Date endDate = null;
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("nowDate", nowDate);
		
		List<CrmAutoSendConfig> listConfig = null;
		
		String jpql = "SELECT A FROM CrmAutoSendConfig A "
				+ "WHERE A.isActive = 'Y' AND A.configDate <= :nowDate ";
		listConfig = getByQuery(CrmAutoSendConfig.class, jpql, parameters);
		if(listConfig == null || listConfig.size() == 0) return;
		
		for (CrmAutoSendConfig config : listConfig) {
			if(config.getDayType().equals("Day"))
			{
				beginDate = new Date(nowDate.getYear(), nowDate.getMonth(), nowDate.getDate() + config.getDayCount(), 0, 0);
				endDate = new Date(nowDate.getYear(), nowDate.getMonth(), nowDate.getDate() + config.getDayCount(), 23, 59);
			}
			CrmTreatmentCondition condition = getByPkId(CrmTreatmentCondition.class, config.getCrmTreatmentConditionPkId());
			if(condition != null)
			{
				List<Customer> listCustomer = new ArrayList<Customer>();
				List<Customer> tmpCustomer = new ArrayList<Customer>();
				List<CrmChannelInfo> listChannel = new ArrayList<CrmChannelInfo>();
				List<CrmTreatmentConditionDtl> listConditionDtl = getByQuery(CrmTreatmentConditionDtl.class, "SELECT A FROM CrmTreatmentConditionDtl A WHERE A.hdrPkId = " + condition.getPkId(), null);
				for (CrmTreatmentConditionDtl conDtl : listConditionDtl) {
					parameters.clear();
					if(conDtl.getType().equals(Tool.CRMEMPLOYEE))
					{
						parameters.put("beginDate", beginDate);
						parameters.put("endDate", endDate);
						parameters.put("employeePkId", conDtl.getTypePkId());
						jpql = "SELECT B FROM EmployeeRequest A "
								+ "INNER JOIN Customer B ON A.customerPkId = B.pkId "
								+ "WHERE A.employeePkId = :employeePkId " +
								" AND A.beginDate BETWEEN :beginDate AND :endDate ";
						tmpCustomer = getByQuery(Customer.class, jpql, parameters);
						for (Customer customer : tmpCustomer) {
							if(!listCustomer.contains(customer))
								listCustomer.add(customer);
						}
					}
					else if(conDtl.getType().equals(Tool.CRMAGE))
					{
						tmpCustomer.clear();
						for (Customer customer : listCustomer) {
							if(customer.getAge() >= conDtl.getBeginAge() && customer.getAge() <= conDtl.getEndAge())
								tmpCustomer.add(customer);
						}
						listCustomer.clear();
						listCustomer.addAll(tmpCustomer);
					}
					else if(conDtl.getType().equals(Tool.CRMADDRESS))
					{
						tmpCustomer.clear();
						for (Customer customer : listCustomer) {
							if(customer.getAimagPkId().compareTo(conDtl.getAimagPkId()) == 0 && customer.getSumPkId().compareTo(conDtl.getSoumPkId()) == 0)
								tmpCustomer.add(customer);
						}
						listCustomer.clear();
						listCustomer.addAll(tmpCustomer);
					}
					else if(conDtl.getType().equals(Tool.CRMCHANNEL))
					{
						CrmChannelInfo tmpChannel = getByPkId(CrmChannelInfo.class, conDtl.getTypePkId());
						if(config.getChannelType().contains(tmpChannel.getChannelType()))
							listChannel.add(tmpChannel);
					}
				}
				if(listCustomer != null && listCustomer.size() > 0)
				{
					String mails = "";
					for (Customer tmp : listCustomer) {
						mails += tmp.getEmail();
					}
						
					for (CrmChannelInfo channel : listChannel) {
						emailSend(InternetAddress.parse(mails), channel.getTitle(), channel.getNote());
					}
				}
			}
		}
	}
	
	public void emailSend(InternetAddress[] mails, String title, String note) {
		final String username = Tool.CRMGMAIL;
		final String pass = Tool.CRMGMAILPASS;
		
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
					return new javax.mail.PasswordAuthentication(username, pass);
				}
			});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, mails);
			
			message.setSubject(title);
			message.setText(note);
			
			Transport.send(message);
		} catch (MessagingException e) {
	    	e.printStackTrace();
		}
	}

	@Override
	public void saveCustomer(CrmCustomer customer) throws Exception{
		
		if(customer.getStatus().equals(Tool.ADDED))
		{
			BigDecimal pkId = Tools.newPkId();
			customer.setPkId(pkId);
			insert(customer);
		}
		else if(customer.getStatus().equals(Tool.MODIFIED))
		{
			update(customer);
		}
		else
		{
			delete(customer);
		}
	}
	
	@Override
	public boolean getCustomerByPkId(BigDecimal customerPkId) throws Exception{
		
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("customerPkId", customerPkId);
		String jpql = "SELECT e FROM CrmCustomer e WHERE e.customerPkId = :customerPkId";
		List<CrmCustomer> listCus = getByQuery(CrmCustomer.class, jpql, parameters);
		return listCus != null && listCus.size() > 0;
	}
	
	@Override
	public List<CrmCustomerFamily> getCustomerFamily(BigDecimal customerPkId) throws Exception{
		
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("customerPkId", customerPkId);
		String jpql = "SELECT e FROM CrmCustomerFamily e WHERE e.crmCustomerPkId = :customerPkId";
		return getByQuery(CrmCustomerFamily.class, jpql, parameters);
	}

	@Override
	public BigDecimal setCustomerFamily(CrmCustomerFamily family) throws Exception{
		
		BigDecimal pkId = BigDecimal.ZERO;
		if(family.getStatus().equals(Tool.ADDED))
		{
			pkId = Tools.newPkId();
			family.setPkId(pkId);
			insert(family);
		}
		else if(family.getStatus().equals(Tool.MODIFIED))
		{
			pkId = family.getPkId();
			update(family);
		}
		else if(family.getStatus().equals(Tool.DELETE))
		{
			delete(family);
		}
		return pkId;
	}
	
	@Override
	public List<CrmCustomerInfo> getCustomerInfo(BigDecimal customerPkId) throws Exception{
		
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("customerPkId", customerPkId);
		String jpql = "SELECT e FROM CrmCustomerInfo e "
				+ "WHERE e.crmCustomerPkId = :customerPkId";
		return getByQuery(CrmCustomerInfo.class, jpql, parameters);
	}

	@Override
	public BigDecimal setCustomerInfo(CrmCustomerInfo Info) throws Exception{
		
		BigDecimal pkId = BigDecimal.ZERO;
		if(Info.getStatus().equals(Tool.ADDED))
		{
			pkId = Tools.newPkId();
			Info.setPkId(pkId);
			insert(Info);
		}
		else if(Info.getStatus().equals(Tool.MODIFIED))
		{
			pkId = Info.getPkId();
			update(Info);
		}
		else if(Info.getStatus().equals(Tool.DELETE))
		{
			delete(Info);
		}
		return pkId;
	}
	
	@Override
	public List<CrmCustomerContactHistory> getCustomerContactHistory(BigDecimal customerPkId) throws Exception{
		
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("customerPkId", customerPkId);
		String jpql = "SELECT e FROM CrmCustomerContactHistory e "
				+ "WHERE e.crmCustomerPkId = :customerPkId";
		return getByQuery(CrmCustomerContactHistory.class, jpql, parameters);
	}

	@Override
	public BigDecimal setCustomerContactHistory(CrmCustomerContactHistory Info) throws Exception{
		
		BigDecimal pkId = BigDecimal.ZERO;
		if(Info.getStatus().equals(Tool.ADDED))
		{
			pkId = Tools.newPkId();
			Info.setPkId(pkId);
			insert(Info);
		}
		else if(Info.getStatus().equals(Tool.MODIFIED))
		{
			pkId = Info.getPkId();
			update(Info);
		}
		else if(Info.getStatus().equals(Tool.DELETE))
		{
			delete(Info);
		}
		return pkId;
	}
	
	@Override
	public List<CrmInfo> getTreatmentInfo(BigDecimal customerPkId) throws Exception{
		
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("customerPkId", customerPkId);
		
		List<CrmInfo> listInfo = new ArrayList<CrmInfo>();
		
		//ХЭВТЭН ЭМЧЛҮҮЛЭХ
		String jpql = "SELECT new crm.businessentity.CrmInfo(A.beginDate, A.endDate, C.firstName, B.name, X.nameMn, D.outPatientHood) FROM InpatientHdr A "
				+ "INNER JOIN SubOrganization B ON A.subOrganizationPkId = B.pkId "
				+ "INNER JOIN Employee C ON A.employeePkId = C.pkId "
				+ "INNER JOIN CustomerDiagnose E ON A.inspectionPkId = E.inspectionPkId "
				+ "INNER JOIN Diagnose X ON E.diagnosePkId = X.pkId "
				+ "LEFT JOIN CustomerDispense D ON A.pkId = D.inpatientHdrPkId "
				+ "WHERE A.customerPkId = :customerPkId";
		listInfo.addAll(getByQuery(CrmInfo.class, jpql, parameters));
		
		//ШИНЖИЛГЭЭНИЙ ХАРИУ
		jpql = "SELECT new crm.businessentity.CrmInfo(D.name, C.requestDate, C.pkId, 'Request') FROM Customer A "
				+ "INNER JOIN ExaminationRequest C ON A.pkId = C.customerPkId "
				+ "INNER JOIN Examination D ON C.examinationPkId = D.pkId "
				+ "WHERE A.pkId = :customerPkId ";
				listInfo.addAll(getByQuery(CrmInfo.class, jpql, parameters));
		jpql = "SELECT new crm.businessentity.CrmInfo(D.name, C.requestDate, C.pkId, 'Active') FROM Customer A "
				+ "INNER JOIN ExaminationRequestActive C ON A.pkId = C.customerPkId "
				+ "INNER JOIN Examination D ON C.examinationPkId = D.pkId "
				+ "WHERE A.pkId = :customerPkId ";
				listInfo.addAll(getByQuery(CrmInfo.class, jpql, parameters));
		jpql = "SELECT new crm.businessentity.CrmInfo(D.name, C.requestDate, C.pkId, 'TempSave') FROM Customer A "
				+ "INNER JOIN ExaminationRequestTempSave C ON A.pkId = C.customerPkId "
				+ "INNER JOIN Examination D ON C.examinationPkId = D.pkId "
				+ "WHERE A.pkId = :customerPkId ";
				listInfo.addAll(getByQuery(CrmInfo.class, jpql, parameters));
		jpql = "SELECT new crm.businessentity.CrmInfo(D.name, C.requestDate, C.pkId, 'Completed') FROM Customer A "
				+ "INNER JOIN ExaminationRequestCompleted C ON A.pkId = C.customerPkId "
				+ "INNER JOIN Examination D ON C.examinationPkId = D.pkId "
				+ "WHERE A.pkId = :customerPkId ";
		listInfo.addAll(getByQuery(CrmInfo.class, jpql, parameters));
		
		//ХАГАЛГААНЫ МЭДЭЭЛЭЛ
		jpql = "SELECT new crm.businessentity.CrmInfo(B.name, A.surgeryDate, C.firstName, D.name) FROM CustomerSurgery A "
				+ "INNER JOIN Surgery B ON A.surgeryPkId = B.pkId "
				+ "INNER JOIN Employee C ON A.employeePkId = C.pkId "
				+ "INNER JOIN SubOrganization D ON C.subOrganizationPkId = D.pkId "
				+ ""
				+ "WHERE A.customerPkId = :customerPkId";
		listInfo.addAll(getByQuery(CrmInfo.class, jpql, parameters));
		return listInfo;
	}
	
	@Override
	public List<CrmInfo> getExaminationResult(BigDecimal examPkId) throws Exception{
		
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("examPkId", examPkId);
		
		List<CrmInfo> listInfo = new ArrayList<CrmInfo>();
		String jpql = "SELECT new crm.businessentity.CrmInfo(C.name, C.minValue, C.maxValue, D.measurement, A.value) "
				+ "FROM ExaminationValueHdr A "
				+ "INNER JOIN ExaminationRequestCompleted B ON A.requestPkId = B.pkId "
				+ "INNER JOIN ExaminationValueQuestion C ON A.questionPkId = C.pkId "
				+ "INNER JOIN ExaminationValueAnswer D ON A.answerPkId = D.pkId "
				+ "WHERE B.pkId = :examPkId";
		listInfo.addAll(getByQuery(CrmInfo.class, jpql, parameters));
		return listInfo;
	}
	
	@Override
	public List<CrmCustomer> getAllCrmCustomer() throws Exception{
		
		String query = "SELECT new crm.entity.CrmCustomer(B, A) FROM CrmCustomer A "
				+ "INNER JOIN Customer B ON A.customerPkId = B.pkId";
		return getByQuery(CrmCustomer.class, query, null);
	}
	
	@Override
	public void deleteCrmCustomer(CrmCustomer customer) throws Exception{
		
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("pkId", customer.getPkId());
		executeNonQuery("DELETE FROM CrmCustomer WHERE pkId = :pkId", parameters);
	}
	
	@Override
	public List<Customer> getListCustomer(List<String> listCus) throws Exception{
		CustomHashMap parameters = new CustomHashMap();
	    parameters.put("listCardNumber", listCus);
	    return getByQuery(Customer.class, "SELECT A FROM Customer A WHERE A.cardNumber IN :listCardNumber", parameters);
	}

	@Override
	public List<String> getListCrmCustomer(List<BigDecimal> listCusPkId) throws Exception{
		CustomHashMap parameters = new CustomHashMap();
	    parameters.put("listPkId", listCusPkId);
	    String query = "SELECT B.cardNumber FROM CrmCustomer A "
	    		+ "INNER JOIN Customer B ON A.customerPkId = B.pkId "
	    		+ "WHERE A.customerPkId IN :listPkId";
	    return getByQuery(String.class, query, parameters);
	}
	
	@Override
	public void insertCrmCustomerList(List<CrmCustomer> listCustomer) throws Exception{
		insert(listCustomer);
	}
	
	@Override
	public List<CrmCustomer> getCustomerSearch(String cardNumber, String customerName, String phoneNumber, String customerAddress,
			String lifeCategory, boolean notInfo, boolean infoSms, boolean infoMail) throws Exception{
		CustomHashMap parameters = new CustomHashMap();
		if(lifeCategory != null && !lifeCategory.equals(""))
			parameters.put("lifeCategory", lifeCategory);
		
		String query = "SELECT new crm.entity.CrmCustomer(B, A.lifeCategory, A.feedBackType) FROM CrmCustomer A "
	    		+ "INNER JOIN Customer B ON A.customerPkId = B.pkId "
	    		+ "WHERE 1 = 1 "
	    		+ (cardNumber != null && !cardNumber.equals("") ? "AND B.cardNumber LIKE '%" + cardNumber + "%' " : "")
	    		+ (customerName != null && !customerName.equals("") ? "AND B.firstName LIKE '%" + customerName + "%' " : "")
	    		+ (phoneNumber != null && !phoneNumber.equals("") ? "AND (B.phoneNumber LIKE '%" + phoneNumber + "%' OR B.telePhone LIKE '%" + phoneNumber + "%') " : "")
	    		+ (lifeCategory != null && !lifeCategory.equals("") ? "AND A.lifeCategory = :lifeCategory " : "")
	    		+ (notInfo ? "AND A.feedBackType = '' " : infoSms ? "AND A.feedBackType LIKE '%SMS%' " : infoMail ? "AND A.feedBackType LIKE '%Mail%' " : "")
	    		+ "GROUP BY B, A.lifeCategory, A.feedBackType";
	    return getByQuery(CrmCustomer.class, query, parameters);
	}
	
	@Override
	public List<CrmChannelInfo> getChannelInfo(String channelType, String titleSearch, String valueSearch, String isActive) throws Exception
	{
		CustomHashMap parameters = new CustomHashMap();
		if(channelType != null && !channelType.equals(""))
			parameters.put("channelType", channelType);
		if(titleSearch != null && !titleSearch.equals(""))
			parameters.put("titleSearch", titleSearch);
		if(valueSearch != null && !valueSearch.equals(""))
			parameters.put("valueSearch", valueSearch);
		String query = "SELECT A FROM CrmChannelInfo A "
	    		+ "WHERE 1 = 1 "
	    		+ (titleSearch != null && !titleSearch.equals("") ? "AND A.title LIKE '%" + titleSearch + "%' " : "")
	    		+ (valueSearch != null && !valueSearch.equals("") ? "AND A.note LIKE '%" + valueSearch + "%' " : "")
	    		+ (channelType != null && !channelType.equals("") ? "AND A.channelType = :channelType " : "")
	    		+ (isActive != null && !isActive.equals("") ? "AND A.isActive = 1 " : "");
	    return getByQuery(CrmChannelInfo.class, query, parameters);
	}
	
	@Override
	public void saveSmsInfo(CrmChannelInfo smsInfo, LoggedUser user) throws Exception
	{
		if(smsInfo.getStatus().equals(Tool.ADDED))
		{
			BigDecimal pkId = Tools.newPkId();
			smsInfo.setPkId(pkId);
			smsInfo.setCreatedBy(user.getUser().getPkId());
			smsInfo.setCreatedDate(new Date());
			insert(smsInfo);
		}
		else if(smsInfo.getStatus().equals(Tool.MODIFIED))
		{
			smsInfo.setUpdatedBy(user.getUser().getPkId());
			smsInfo.setUpdatedDate(new Date());
			update(smsInfo);
		}
		else
		{
			delete(smsInfo);
		}
	}
	
	@Override
	public List<CrmTreatmentCondition> getCrmTreatmentList() throws Exception{
		
		String query = "SELECT new crm.entity.CrmTreatmentCondition(A.pkId, A.name, A.conditionType, A.isActive, B.name, A.createdDate, A.description,"
				+ "A.createdBy, A.updatedBy, A.updatedDate, A.firstTreatment, A.insuranceInfo, A.bloodType, A.lifeCategory, A.gender) "
				+ "FROM CrmTreatmentCondition A "
				+ "INNER JOIN Users B ON A.createdBy = B.pkId ";
		return getByQuery(CrmTreatmentCondition.class, query, null);
	}
	
	@Override
	public void saveConditionTreatment(CrmTreatmentCondition condition, List<CrmTreatmentConditionDtl> listDtl) throws Exception{
		
		if(condition.getStatus().equals(Tool.ADDED))
		{
			BigDecimal pkId = Tools.newPkId();
			condition.setPkId(pkId);
			condition.setCreatedDate(new Date());
			if(listDtl != null && listDtl.size() > 0)
			{
				BigDecimal dtlPkId = Tools.newPkId();
				for (CrmTreatmentConditionDtl dtl : listDtl) {
					dtl.setHdrPkId(pkId);
					dtl.setPkId(dtlPkId);
					dtlPkId = dtlPkId.add(BigDecimal.ONE);
				}
			}
			insert(condition);
			insert(listDtl);
		}
		else if(condition.getStatus().equals(Tool.MODIFIED))
		{
			deleteByAnyField(CrmTreatmentConditionDtl.class, "hdrPkId", condition.getPkId());
			condition.setUpdatedDate(new Date());
			if(listDtl != null && listDtl.size() > 0)
			{
				BigDecimal dtlPkId = Tools.newPkId();
				for (CrmTreatmentConditionDtl dtl : listDtl) {
					dtl.setHdrPkId(condition.getPkId());
					dtl.setPkId(dtlPkId);
					dtlPkId = dtlPkId.add(BigDecimal.ONE);
				}
			}
			insert(listDtl);
			update(condition);
		}
		else if(condition.getStatus().equals(Tool.DELETE))
		{
			deleteByAnyField(CrmTreatmentConditionDtl.class, "hdrPkId", condition.getPkId());
			deleteByPkId(CrmTreatmentCondition.class, condition.getPkId());
		}
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public List<CrmTreatmentConditionResult> getListResult(Date searchDate, String conditionType) throws Exception{
		Date beginDate = new Date(searchDate.getYear(), searchDate.getMonth(), searchDate.getDate(), 0, 0);
		Date endDate = new Date(searchDate.getYear(), searchDate.getMonth(), searchDate.getDate(), 23, 59);
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("conditionType", conditionType);
		parameters.put("beginDate", beginDate);
		parameters.put("endDate", endDate);
		
		String jpql = "SELECT A FROM CrmTreatmentConditionResult A "
				+ "WHERE A.createdDate BETWEEN :beginDate AND :endDate "
				+ "AND A.conditionType =:conditionType";
		return getByQuery(CrmTreatmentConditionResult.class, jpql, parameters);
	}
	
	@Override
	public List<CrmTreatmentConditionResultDtl> getResultDtlList(BigDecimal resultPkId, BigDecimal subOrgPkId, BigDecimal searchEmployeePkId, String searchCustomerName) throws Exception{
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("resultPkId", resultPkId);
		if(subOrgPkId != null && subOrgPkId.compareTo(BigDecimal.ZERO) != 0)
			parameters.put("subOrgPkId", subOrgPkId);
		if(searchEmployeePkId != null && searchEmployeePkId.compareTo(BigDecimal.ZERO) != 0)
			parameters.put("searchEmployeePkId", searchEmployeePkId);
		
		String jpql = "SELECT new crm.entity.CrmTreatmentConditionResultDtl(A.pkId, A.resultPkId, A.customerPkId, B.firstName, "
				+ "B.cardNumber, A.employeePkId, C.firstName, D.name, A.sendDate, A.orderId, A.isFirst, A.orderDate) "
				+ "FROM CrmTreatmentConditionResultDtl A "
				+ "INNER JOIN Customer B ON A.customerPkId = B.pkId "
				+ "INNER JOIN Employee C ON A.employeePkId = C.pkId "
				+ "INNER JOIN SubOrganization D ON C.subOrganizationPkId = D.pkId "
				+ "WHERE A.resultPkId = :resultPkId "
				+ (subOrgPkId != null && subOrgPkId.compareTo(BigDecimal.ZERO) != 0 ? "AND D.pkId = :subOrgPkId " : "")
				+ (searchEmployeePkId != null && searchEmployeePkId.compareTo(BigDecimal.ZERO) != 0 ? "AND C.pkId = :searchEmployeePkId " : "")
				+ (searchCustomerName != null && !searchCustomerName.equals("") ? "AND B.firstName LIKE %'" + searchCustomerName + "'% " : "");
		return getByQuery(CrmTreatmentConditionResultDtl.class, jpql, parameters);
	}
	
	@Override
	public List<Employee> getEmployees() throws Exception{
		
		String query = "SELECT NEW crm.entity.Employee(a.pkId, a.firstName, b.name) "
			+ "FROM Employee a "
			+ "INNER JOIN SubOrganization b ON a.subOrganizationPkId = b.pkId ";
		return getByQuery(Employee.class, query, null);
	}
	
}