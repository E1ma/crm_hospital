package crm.businesslogic;

import base.*;
import crm.businessentity.CrmInfo;
import crm.businessentity.LoggedUser;
import crm.businessentity.Tool;
import crm.businesslogic.interfaces.ILogicAllCrmLocal;
import crm.businesslogic.interfaces.ILogicCrmLocal;
import crm.entity.*;

import javax.ejb.Stateless;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;

@Stateless(name = "LogicAllCrm", mappedName = "crm.businesslogic.LogicAllCrm")
public class LogicAllCrm extends base.BaseData
		implements crm.businesslogic.interfaces.ILogicAllCrm, ILogicAllCrmLocal {

	@Resource
	SessionContext sessionContext;

	public LogicAllCrm() {
		super("CRM");
	}

	public void setDataBaseInfo() throws Exception {
		// dataBaseName = "Hospital"
	}
	
	@Override
	public void saveEmployeeInfo(Employee employeeInfo, Users userInfo, LoggedUser user) throws Exception{
		
		if (userInfo.isHasPasswordChanged())
			userInfo.setPassword(Tool.MD5(userInfo.getPassword()));
		
		if(employeeInfo.getStatus().equals(Tool.ADDED))
		{
			BigDecimal pkId = Tools.newPkId();
			userInfo.setPkId(pkId);
			
			BigDecimal pkIdEmployee = Tools.newPkId();
			employeeInfo.setPkId(pkIdEmployee);
			employeeInfo.setUserPkId(pkId);
			employeeInfo.setCreatedBy(user.getUser().getPkId());
			employeeInfo.setCreatedDate(new Date());
			
			insert(employeeInfo);
			insert(userInfo);
		}
		else if(employeeInfo.getStatus().equals(Tool.MODIFIED))
		{
			employeeInfo.setUpdatedBy(user.getUser().getPkId());
			employeeInfo.setUpdatedDate(new Date());
			update(employeeInfo);
			update(userInfo);
		}
		else 
		{
			CustomHashMap parameters = new CustomHashMap();
			parameters.put("pkId", employeeInfo.getPkId());
			executeNonQuery("DELETE FROM Employee WHERE pkId = :pkId", parameters);
			delete(userInfo);
		}
	}
	
	@Override
	public List<SubOrganization> getListSubOrganization() throws Exception {

		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT DISTINCT a FROM SubOrganization a ");
		jpql.append("WHERE a.isCrm = 'Y' ");
		List<SubOrganization> list = new ArrayList<SubOrganization>();

		list = getByQuery(SubOrganization.class, jpql.toString(), null);
		return list;
	}
	
	@Override
	public List<Employee> getListOfEmployees() throws Exception{
		
		String query = "SELECT NEW crm.entity.Employee(a.lastName, a.firstName, a.id, a.email, a.phone, a.subOrganizationPkId, b.name, a.pkId, c.password, a.innerPhone, a.regNumber) "
			+ "FROM Employee a "
			+ "INNER JOIN SubOrganization b ON a.subOrganizationPkId = b.pkId "
			+ "INNER JOIN Users c ON a.userPkId = c.pkId "
			+ "WHERE a.isCrm = 'Y' ";
		return getByQuery(Employee.class, query, null);
	}
	
	@Override
	public boolean checkRegNumber(String regNumber) throws Exception{
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("regNumber", "%" + regNumber + "%");
		jpql.append("SELECT a.regNumber ");
		jpql.append("FROM Customer a ");
		jpql.append("WHERE a.regNumber LIKE :regNumber ");
		if (getByQuery(String.class, jpql.toString(), parameters).size() > 0)
			return true;
		else
		return false;
	}
	
	@Override
	public List<CrmTreatmentCondition> getListCrmTreatmentCondition() throws Exception {

		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT DISTINCT a FROM CrmTreatmentCondition a ");
		jpql.append("WHERE a.isActive = '1' ");
		List<CrmTreatmentCondition> list = new ArrayList<CrmTreatmentCondition>();

		list = getByQuery(CrmTreatmentCondition.class, jpql.toString(), null);
		return list;
	}
	
	@Override
	public List<CrmAutoSendConfig> getListOfCrmAutoSendConfig() throws Exception{
		
		String query = "SELECT NEW crm.entity.CrmAutoSendConfig(a.id, b.name, a.channelType, a.dayCount, a.dayType, a.isActive, a.configDate) "
			+ "FROM CrmAutoSendConfig a "
			+ "INNER JOIN CrmTreatmentCondition b ON a.crmTreatmentConditionPkId = b.pkId ";
		return getByQuery(CrmAutoSendConfig.class, query, null);
	}
	
	@Override
	public void saveSendingConfig(CrmAutoSendConfig crmAutoSendConfig, LoggedUser user) throws Exception{
		
		if(crmAutoSendConfig.getStatus().equals(Tool.ADDED))
		{
			BigDecimal pkId = Tools.newPkId();
			crmAutoSendConfig.setPkId(pkId);
			crmAutoSendConfig.setCreatedBy(user.getUser().getPkId());
			crmAutoSendConfig.setCreatedDate(new Date());
			insert(crmAutoSendConfig);
		}
		else if(crmAutoSendConfig.getStatus().equals(Tool.MODIFIED))
		{
			crmAutoSendConfig.setUpdatedBy(user.getUser().getPkId());
			crmAutoSendConfig.setUpdatedDate(new Date());
			update(crmAutoSendConfig);
		}
		else 
		{
			delete(crmAutoSendConfig);
		}
		
	}

	@Override
	public List<SystemConfig> getSystemConfig() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveSendingConfig(CrmAutoSendConfig crmAutoSendConfig, SystemConfig sysConf, LoggedUser user)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}