package crm.businesslogic;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import base.*;
import crm.businessentity.*;
import crm.businesslogic.interfaces.ILogicTwoLocal;
import crm.entity.*;

@Stateless(name = "LogicTwo", mappedName = "crm.businesslogic.LogicTwo")
public class LogicTwo extends base.BaseData implements crm.businesslogic.interfaces.ILogicTwo,ILogicTwoLocal {
	
	@Resource
	SessionContext sessionContext;
	
	public LogicTwo(){
		super("CRM");
	}
	
	public void setDataBaseInfo() throws Exception{
		//dataBaseName = "Hospital"
	}
	
	public List<SubOrganization> getSubOrganizations(LoggedUser loggedUser) throws Exception{
		List<SubOrganization> list = getAll(SubOrganization.class);
		return list;
	}
	
	public List<Employee> getEmployeeBySubOrganizationPkId(BigDecimal pkId) throws Exception{
		List<Employee> employees = getByAnyField(Employee.class, "subOrganizationPkId", pkId);
		return employees;
	}
	
	public void changePassword(LoggedUser loggedUser, String newPassword) throws Exception{
		
		
		Users users = loggedUser.getUser();
		users.setPassword(Tool.MD5(newPassword));
		update(users);
	}

	public String getSystemConfigsByCustomerNo() throws Exception{
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("customerNo", "CustomerNo");
		jpql.append("SELECT a FROM SystemConfig a WHERE a.name = :customerNo ");
		List<SystemConfig> configs = getByQuery(SystemConfig.class, jpql.toString(), parameters);
		if(configs.size() > 0) return configs.get(0).getValue();
		return "NONE";
	}
	
}