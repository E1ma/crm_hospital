package crm.businesslogic;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import base.*;
import crm.businessentity.LoggedUser;
import crm.businessentity.Tool;
import crm.businesslogic.interfaces.IInfoLogicLocal;
import crm.entity.*;

@Stateless(name = "InfoLogic", mappedName = "crm.businesslogic.InfoLogic")
public class InfoLogic extends base.BaseData
		implements crm.businesslogic.interfaces.IInfoLogic, IInfoLogicLocal {
	
	@Resource
	SessionContext sessionContext;

	public InfoLogic() {
		super("CRM");
	}

	public void setDataBaseInfo() throws Exception {
		// dataBaseName = "Hospital"
	}

	@Override
	public List<Customer> getCustomers(int first, int count, String sortField, String sortType,
			Map<String, String> filters) throws Exception {
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();

		jpql.append("SELECT a ");
		jpql.append("FROM Customer a ");

		if (!filters.isEmpty()) {
			jpql.append("WHERE ");
			for (Map.Entry<String, String> filter : filters.entrySet()) {
				parameters.put(filter.getKey(), "%" + filter.getValue() + "%");
				filter.getValue();
				jpql.append("a." + filter.getKey() + " LIKE :" + filter.getKey() + " AND ");
			}
			jpql.delete(jpql.length() - 4, jpql.length());
		}
		if (sortField != null && !sortField.isEmpty() && sortType != null && !sortType.isEmpty()) {
			jpql.append("ORDER BY a." + sortField + " " + sortType + " ");
		}
		return getByQuery(Customer.class, jpql.toString(), parameters, first, count);
	}
	
	@Override
	public long getCustomerCount(Map<String, String> filters) throws Exception {
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();

		jpql.append("SELECT COUNT(a.pkId) ");
		jpql.append("FROM Customer a ");

		if (!filters.isEmpty()) {
			jpql.append("WHERE ");
			for (Map.Entry<String, String> filter : filters.entrySet()) {
				parameters.put(filter.getKey(), "%" + filter.getValue() + "%");
				filter.getValue();
				jpql.append("a." + filter.getKey() + " LIKE :" + filter.getKey() + " AND ");
			}
			jpql.delete(jpql.length() - 4, jpql.length());
		}

		return (Long) getByQuerySingle(jpql.toString(), parameters);
	}

	@Override
	public List<CrmCustomer> getCrmCustomers(int first, int count, String sortField, String sortType,
			Map<String, String> filters) throws Exception {
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();

		jpql.append("SELECT new crm.entity.CrmCustomer(a, b) ");
		jpql.append("FROM CrmCustomer b INNER JOIN Customer a ON b.customerPkId = a.pkId ");

		if (!filters.isEmpty()) {
			jpql.append("WHERE ");
			for (Map.Entry<String, String> filter : filters.entrySet()) {
				String filterValue = "'%" + filter.getValue() + "%'";
				filter.getValue();
				jpql.append("a." + filter.getKey().replace("customer.", "") + " LIKE " + filterValue + " AND ");
			}
			jpql.delete(jpql.length() - 4, jpql.length());
		}
		if (sortField != null && !sortField.isEmpty() && sortType != null && !sortType.isEmpty()) {
			jpql.append("ORDER BY a." + sortField + " " + sortType + " ");
		}
		return getByQuery(CrmCustomer.class, jpql.toString(), parameters, first, count);
	}

	@Override
	public long getCrmCustomerCount(Map<String, String> filters) throws Exception {
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();

		jpql.append("SELECT COUNT(b.pkId) ");
		jpql.append("FROM CrmCustomer b INNER JOIN Customer a ON b.customerPkId = a.pkId ");

		if (!filters.isEmpty()) {
			jpql.append("WHERE ");
			for (Map.Entry<String, String> filter : filters.entrySet()) {
				String filterValue = "'%" + filter.getValue() + "%'";
				filter.getValue();
				jpql.append("a." + filter.getKey().replace("customer.", "") + " LIKE " + filterValue + " AND ");
			}
			jpql.delete(jpql.length() - 4, jpql.length());
		}

		return (Long) getByQuerySingle(jpql.toString(), parameters);
	}

	@Override
	public List<SubOrganization> getListSubOrganization() throws Exception {

		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT DISTINCT a FROM SubOrganization a ");
		jpql.append("INNER JOIN Employee b on a.pkId = b.subOrganizationPkId ");

		List<SubOrganization> list = new ArrayList<SubOrganization>();

		list = getByQuery(SubOrganization.class, jpql.toString(), null);
		return list;
	}
	
	@Override
	public List<Menu> getMenus() throws Exception {

		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT a FROM Menu a ");

		List<Menu> list = getByQuery(Menu.class, jpql.toString(), null);
		return list;
	}
	

	@Override
	public List<Menu> getMenusFilter(LoggedUser loggedUser) throws Exception {

		StringBuilder jpql = new StringBuilder();
		BigDecimal bigDecimal = loggedUser.getUser().getPkId();
		CustomHashMap parameters = new CustomHashMap();

		parameters.put("bigDecimal", bigDecimal);
		jpql = new StringBuilder();
		jpql.append("SELECT b.menuPkId FROM UserRoleMap a ");
		jpql.append("INNER JOIN RoleMenuMap b ON a.rolePkId = b.rolePkId ");
		jpql.append("WHERE a.userPkId = :bigDecimal ");

		List<BigDecimal> bigDecimals = getByQuery(BigDecimal.class, jpql.toString(), parameters);
		parameters.put("list", bigDecimals);

		jpql = new StringBuilder();
		jpql.append("SELECT a FROM Menu a ");
		jpql.append("WHERE a.pkId NOT IN :list ");

		List<Menu> list = getByQuery(Menu.class, jpql.toString(), parameters);
		return list;
	}

	@Override
	public List<Menu> getMenus(LoggedUser loggedUser) throws Exception {

		StringBuilder jpql = new StringBuilder();
		BigDecimal userPkId = loggedUser.getUser().getPkId();
		CustomHashMap parameters = new CustomHashMap();

		parameters.put("bigDecimal", userPkId);
		jpql = new StringBuilder();
		jpql.append("SELECT new crm.entity.Menu(b.menuPkId, c.sort) FROM UserRoleMap a ");
		jpql.append("INNER JOIN RoleMenuMap b ON a.rolePkId = b.rolePkId ");
		jpql.append("INNER JOIN Menu c ON b.menuPkId = c.pkId ");
		jpql.append("WHERE a.userPkId = :bigDecimal ");

		List<Menu> listPkId = getByQuery(Menu.class, jpql.toString(), parameters);
		List<BigDecimal> listMenuPkId = new ArrayList<BigDecimal>();
		List<Integer> listSort = new ArrayList<Integer>();
		for (Menu menu : listPkId) {
			listMenuPkId.add(menu.getPkId());
			if(!listSort.contains(menu.getSort()))
				listSort.add(menu.getSort());
		}
		parameters.put("list", listMenuPkId);
		parameters.put("sort", listSort);

		jpql = new StringBuilder();
		jpql.append("SELECT a FROM Menu a ");
		jpql.append("WHERE a.pkId IN ( ");
		jpql.append("SELECT MIN(b.pkId) FROM Menu b ");
		jpql.append("GROUP BY b.sort ");
		jpql.append(") AND a.pkId IN :list ");
		jpql.append("ORDER BY a.sort ");

		List<Menu> list = getByQuery(Menu.class, jpql.toString(), parameters);

		jpql = new StringBuilder();
		jpql.append("SELECT a FROM Menu a ");
		jpql.append("WHERE a.sort IN :sort ");
		List<Menu> subList = getByQuery(Menu.class, jpql.toString(), parameters);
		for (Menu menu : list)
			menu.setListSubMenu(new ArrayList<Menu>());
		for (Menu menu : list) {
			for (Menu tmp : subList) {
				if(menu.getSort() == tmp.getSort() && menu.getPkId().compareTo(tmp.getPkId()) != 0)
					menu.getListSubMenu().add(tmp);
			}
		}
		
		return list;
	}
	
	@Override
	public List<Menu> getListMenus(LoggedUser loggedUser) throws Exception {
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();

		parameters.put("userPkId", loggedUser.getUser().getPkId());
		jpql.append("SELECT DISTINCT a FROM Menu a ");
		jpql.append("INNER JOIN RoleMenuMap b ON b.menuPkId = a.pkId ");
		jpql.append("INNER JOIN UserRoleMap c ON c.rolePkId = b.rolePkId ");
		jpql.append("WHERE c.userPkId = :userPkId ");

		return getByQuery(Menu.class, jpql.toString(), parameters);
	}

	@Override
	public SystemConfig getLicense() throws Exception {

		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("name", "License");

		jpql.append("SELECT a FROM SystemConfig a WHERE a.name = :name ");
		List<SystemConfig> list = getByQuery(SystemConfig.class, jpql.toString(), parameters);
		if (list.size() == 0)
			return null;
		else
			return list.get(0);
	}

	@Override
	public void saveLicense(SystemConfig config) throws Exception {

		if (Tool.ADDED.equals(config.getStatus())) {
			config.setPkId(Tools.newPkId());
			insert(config);
		} else if (Tool.MODIFIED.equals(config.getStatus())) {
			update(config);
		} else if (Tool.DELETE.equals(config.getStatus())) {
			delete(config);
		}
	}
	
	@Override
	public List<Employee> getEmployeeInfo() throws Exception
	{
		String jpql = "SELECT new crm.entity.Employee(A.pkId, A.firstName, B.name) FROM Employee A "
				+ "INNER JOIN SubOrganization B ON A.subOrganizationPkId = B.pkId ";
		return getByQuery(Employee.class, jpql, null);
	}
}