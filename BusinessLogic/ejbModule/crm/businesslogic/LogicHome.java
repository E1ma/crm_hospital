package crm.businesslogic;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import base.*;
import crm.businessentity.LoggedUser;
import crm.businessentity.Tool;
import crm.businesslogic.interfaces.ILogicHome;
import crm.entity.*;

@Stateless(name = "LogicHome", mappedName = "crm.businesslogic.LogicHome")
public class LogicHome extends base.BaseData implements crm.businesslogic.interfaces.ILogicHomeLocal, ILogicHome{
	@Resource
	SessionContext sessionContext;

	public LogicHome() {
		super("CRM");
	}

	public void setDataBaseInfo() throws Exception {
		// dataBaseName = "Hospital"
	}

	@Override
	public List<Organization> getOrganizations() throws Exception {
		CustomHashMap  map =  new CustomHashMap();
		StringBuilder  builder  =  new StringBuilder();
		builder.append(" SELECT  count(ors.pkId) FROM Organization ors ");
		builder.append(" GROUP BY  ors.pkId ");
		List<Organization>  list  =  getByQuery(Organization.class, builder.toString(),map);
		return list;
	}

	@Override
	public List<Role> getAccessLaw() throws Exception {
		CustomHashMap  map =  new CustomHashMap();
		StringBuilder  builder  =  new StringBuilder();
		builder.append(" SELECT  count(r.pkId) FROM Role r");
		builder.append(" WHERE  r.parentPkId  IS  NULL ");
		builder.append(" GROUP BY  r.pkId ");
		List<Role>  list  =  getByQuery(Role.class, builder.toString(),map);
		return list;
	}

	@Override
	public List<Users> getUsers() throws Exception {
		CustomHashMap  map =  new CustomHashMap();
		StringBuilder  builder  =  new StringBuilder();
		builder.append(" SELECT  count(u.pkId) FROM Users u");
		builder.append(" GROUP BY  u.pkId ");
		List<Users>  list  =  getByQuery(Users.class, builder.toString(),map);
		return list;
	}

	@Override
	public List<SubOrganization> getRoom() throws Exception {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT  count(a.pkId) FROM SubOrganization a ");
		jpql.append(" GROUP BY a.pkId");
		List<SubOrganization> list = new ArrayList<SubOrganization>();
		list = getByQuery(SubOrganization.class, jpql.toString(), null);
		return list;
	}

	@Override
	public List<Employee> getEmployee() throws Exception {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT  count(a.pkId) FROM Employee a ");
		jpql.append(" GROUP BY a.pkId");
		List<Employee> list = new ArrayList<Employee>();
		list = getByQuery(Employee.class, jpql.toString(), null);
		return list;
	}

	@Override
	public List<Customer> getCustomer() throws Exception {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT  count(a.pkId) FROM Customer a ");
		jpql.append("GROUP BY a.pkId");
		List<Customer> list = new ArrayList<Customer>();
		list = getByQuery(Customer.class, jpql.toString(), null);
		return list;
	}

	@Override
	public List<SubOrganization> getSelectionSubOrganization() throws Exception {
		// TODO Auto-generated method stub
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT  a FROM SubOrganization a ");
		List<SubOrganization> list = new ArrayList<SubOrganization>();
		list = getByQuery(SubOrganization.class, jpql.toString(), null);
		return list;
	}

	@Override
	public List<Employee> getSelectionEmployee(List<BigDecimal> name) throws Exception {
		CustomHashMap  map =  new CustomHashMap();
		StringBuilder jpql = new StringBuilder();
		map.put("names", name);
		jpql.append("SELECT DISTINCT a  FROM Employee a ");
		jpql.append(" WHERE a.subOrganizationPkId IN :names ");
		List<Employee> list = new ArrayList<Employee>();
		list = getByQuery(Employee.class, jpql.toString(), map);
		return list;
	}

	@Override
	public void setPostSave(Post p,String text, BigDecimal e,List<BigDecimal> receiverPkIds,Date time) throws Exception {
		//	public void setPostSave(Post  p,String text, BigDecimal e, List<BigDecimal> receiverPkIds) throws Exception {
		if (Tool.ADDED.equals(p.getStatus())) {
			p.setPkId(Tools.newPkId());
			p.setSenderEmployeePkId(e);
			p.setPost(text);
			p.setCreateDate(time);
			insert(p);	
			for(BigDecimal pkId : receiverPkIds){
				PostMap map = new PostMap();
				map.setPkId(Tools.newPkId());
				map.setPostPkId(p.getPkId());
				map.setReceiverEmployeePkId(pkId);
				insert(map);
			}
		}

	}
	@Override
	public void  setPostDeleteModify(Post  p) throws Exception {
		if (Tool.DELETE.equals(p.getStatus())) {
			deleteByAnyField(PostMap.class, "postPkId", p.getPostpkid());
			deleteByPkId(Post.class, p.getPkId());
		}
		else 
			if (Tool.MODIFIED.equals(p.getStatus())) {			 
				update(p);
			}
	}
	@Override
	public List<Post> getPostView(BigDecimal id) throws Exception {
		// TODO Auto-generated method stub
		CustomHashMap  map  =  new  CustomHashMap();
		map.put("id", id);
		StringBuilder builder   =  new StringBuilder();
		builder.append("SELECT NEW crm.entity.Post(c.firstName,a.createDate, a.post,d.image,a.pkId,b.postPkId,a.senderEmployeePkId ) ");
		builder.append("FROM Post a ");
		builder.append("INNER JOIN PostMap b ON b.postPkId = a.pkId ");
		builder.append("INNER JOIN Employee c ON a.senderEmployeePkId = c.pkId ");
		builder.append("INNER JOIN Users d ON c.userPkId = d.pkId ");
		builder.append("WHERE  b.receiverEmployeePkId =:id  OR  a.senderEmployeePkId=:id ");
		builder.append("GROUP BY  c.firstName,a.createDate,a.post,d.image,a.pkId,b.postPkId,a.senderEmployeePkId  ");
		builder.append("ORDER BY a.createDate DESC ");
		return getByQuery(Post.class,builder.toString(),map);
	}

	@Override
	public List<Post> getPostPostPkId(BigDecimal id) throws Exception {
		CustomHashMap  map  =  new CustomHashMap();
		map.put("postpkid", id);
		StringBuilder builder   =  new StringBuilder();
		builder.append("SELECT NEW crm.entity.Post(a.post,a.createDate,c.firstName,a.senderEmployeePkId,a.pkId, b.postPkId ,b.pkId ) ");
		builder.append("FROM Post a ");
		builder.append("INNER JOIN PostMap b ON b.postPkId = a.pkId ");
		builder.append("INNER JOIN Employee c ON a.senderEmployeePkId = c.pkId ");
		builder.append(" WHERE b.postPkId = :postpkid ");
		return getByQuery(Post.class, builder.toString(),map);
	}

	@Override
	public List<Post> getPostMapReceiverPkId(BigDecimal pkId) throws Exception {
		CustomHashMap  map  =  new  CustomHashMap();
		map.put("pkId", pkId);
		StringBuilder builder   =  new StringBuilder();
		builder.append("SELECT c.firstName FROM Post a ");
		builder.append("INNER JOIN PostMap b ON a.pkId  =b.postPkId ");
		builder.append("INNER JOIN Employee c ON b.receiverEmployeePkId = c.pkId ");
		builder.append("WHERE  b.postPkId = :pkId ");
		return getByQuery(Post.class,builder.toString(),map);
	}

	@Override
	public List<Post> getPostPostPkIdView(BigDecimal id) throws Exception {
		CustomHashMap  map  =  new CustomHashMap();
		map.put("pkid", id);
		StringBuilder builder   =  new StringBuilder();
		builder.append("SELECT NEW crm.entity.Post(a.post,a.createDate,c.firstName,a.senderEmployeePkId,a.pkId,b.postPkId , b.pkId) ");
		builder.append("FROM Post a ");
		builder.append("INNER JOIN PostMap b ON b.postPkId = a.pkId ");
		builder.append("INNER JOIN Employee c ON a.senderEmployeePkId = c.pkId ");
		builder.append(" WHERE c.pkId  = :pkid ");
		return getByQuery(Post.class, builder.toString(),map);
	}

	@Override
	public List<Post> getPostPostUpdate(BigDecimal id) throws Exception {
		CustomHashMap  map  =  new CustomHashMap();
		map.put("postpkid", id);
		StringBuilder builder   =  new StringBuilder();
		builder.append("SELECT NEW crm.entity.Post(a.post,a.createDate,c.firstName,a.senderEmployeePkId,a.pkId, b.postPkId ,b.pkId ) ");
		builder.append("FROM Post a ");
		builder.append("INNER JOIN PostMap b ON b.postPkId = a.pkId ");
		builder.append("INNER JOIN Employee c ON a.senderEmployeePkId = c.pkId ");
		builder.append(" WHERE b.pkId = :postpkid ");
		return getByQuery(Post.class, builder.toString(),map);
	}

	@Override
	public List<UpgradingLog> getUpgrades() throws Exception {
		CustomHashMap parameters =  new CustomHashMap();
		StringBuilder jpql =  new StringBuilder();
		jpql.append("SELECT NEW crm.entity.UpgradingLog(a.createdDate)  FROM UpgradingLog a ");
		
		List<UpgradingLog> logs = getByQuery(UpgradingLog.class, jpql.toString(),null);
		List<UpgradingLog> upgradingLogs =  new ArrayList<UpgradingLog>();
		if(logs.size()<1) return  new ArrayList<UpgradingLog>();
		Date lDate = logs.get(0).getCreatedDate();
		Date endLDate = logs.get(logs.size()-1).getCreatedDate();
		Date date = lDate;
		UpgradingLog log1 =  new UpgradingLog(date);
		upgradingLogs.add(log1);
		
		lDate.setHours(0);
		lDate.setMinutes(0);
		lDate.setSeconds(0);
		endLDate.setHours(0);
		endLDate.setMinutes(0);
		endLDate.setSeconds(0);
		
		if(date.equals(endLDate)){
			return upgradingLogs;
		} else {
			while(date.before(endLDate)) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);
				calendar.add(Calendar.DATE, 1);
				date = calendar.getTime();
				UpgradingLog log =  new UpgradingLog(date);
				upgradingLogs.add(log);
			}
		}
		
		System.out.println(upgradingLogs.size());	
		
		for (UpgradingLog upgradingLog : upgradingLogs) {
			Date beginDate =  (Date) Tool.deepClone(upgradingLog.getCreatedDate());
			beginDate.setHours(0);
			beginDate.setMinutes(0);
			beginDate.setSeconds(0);
			Date endDate =  (Date) Tool.deepClone(upgradingLog.getCreatedDate());
			endDate.setHours(23);
			endDate.setMinutes(59);
			endDate.setSeconds(59);
			
			parameters.put("beginDate", beginDate);
			parameters.put("endDate", endDate);
			jpql =  new StringBuilder();
			jpql.append("SELECT  a FROM UpgradingLog a ");
			jpql.append("WHERE a.createdDate BETWEEN :beginDate AND :endDate ");
			upgradingLog.setUpgradingLogs(getByQuery(UpgradingLog.class,jpql.toString(),parameters));
		}
		return upgradingLogs;
	}
	
	@Override
	public void saveUpgradeLog(UpgradingLog log, LoggedUser user) throws Exception {
		Date date = new Date();
		if(Tool.ADDED.equals(log.getStatus())){
			log.setPkId(Tools.newPkId());
			log.setCreatedDate(date);
			log.setCreatedBy(user.getEmployeePkId());
			insert(log);
		} else if(Tool.MODIFIED.equals(log.getStatus())){
			
		}
		
	}
	
	@Override
	public UserRoleMap getLoggedUserRole(LoggedUser user) throws Exception {
		CustomHashMap parameters =  new CustomHashMap();
		parameters.put("pkId", user.getUser().getPkId());
		parameters.put("name", "Administrator");
		System.out.println( "current User PkId "+ user.getUser().getPkId());
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT  a  FROM UserRoleMap a ");
		jpql.append("INNER JOIN Role b  ON a.rolePkId=b.pkId ");
		jpql.append("WHERE a.userPkId=:pkId  AND b.name=:name  ");
		
		List<UserRoleMap> users =getByQuery(UserRoleMap.class,jpql.toString(),parameters);
		if(users.size()>0)
			return users.get(0);
		return new UserRoleMap();
	}

}
