package crm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "CrmChannelInfo")
public class CrmChannelInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	@Column(name = "ChannelType")
	private String channelType;
	
	@Column(name = "Id")
	private String id;
	
	@Column(name = "Title")
	private String title;
	
	@Column(name = "Note")
	private String note;
	
	@Column(name = "GroupType")
	private String groupType;
	
	@Column(name = "IsActive")
	private int isActive;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;

	@Column(name = "CreatedBy")
	private BigDecimal createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;

	@Column(name = "UpdatedBy")
	private BigDecimal updatedBy;
	
	@Transient
	private String activeStr;
	
	@Transient
	private boolean activeBoolean;
	
	@Transient
	private boolean selected;
	
	@Transient
	private String status;
	
	@Transient
	private String groupTypeStr;
	
	public CrmChannelInfo(){
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public String getActiveStr() {
		if(isActive == 0)
			activeStr = "Үгүй";
		else
			activeStr = "Тийм";
		return activeStr;
	}

	public void setActiveStr(String activeStr) {
		this.activeStr = activeStr;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isActiveBoolean() {
		activeBoolean = isActive == 1;
		return activeBoolean;
	}

	public void setActiveBoolean(boolean activeBoolean) {
		this.activeBoolean = activeBoolean;
		isActive = activeBoolean ? 1 : 0;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getGroupTypeStr() {
		if(this.getGroupType() == null)
			groupTypeStr = "";
		else if(this.getGroupType().equals("Treatment"))
			groupTypeStr = "Эмчилгээний үйлчлүүлэгч";
		else if(this.getGroupType().equals("Examination"))
			groupTypeStr = "Шинжилгээний үйлчлүүлэгч";
		return groupTypeStr;
	}

	public void setGroupTypeStr(String groupTypeStr) {
		this.groupTypeStr = groupTypeStr;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}