package crm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "CustomerDispense")
public class CustomerDispense implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	@Column(name = "CustomerPkId")
	private BigDecimal customerPkId;
	
	@Column(name = "InpatientHdrPkId")
	private BigDecimal inpatientHdrPkId;
	
	/*
	 * 1 - Гарах
	 * 2 - Шилжих
	 */
	
	@Column(name = "Type")
	private byte type;

	@Column(name = "Difficulty")
	private String difficulty;
	
	/*
	 * 0 - Үгүй
	 * 1 - Тийм
	 */
	
	@Column(name = "HasCardNote")
	private byte hasCardNote;
	
	@Column(name = "Advice")
	private String advice;
	
	/*
	 * 0 - Шилжээгүй
	 * 1 - Аймаг, дүүргийн эмнэлэг
	 * 2 - Төрөлжсөн нарийн мэргэжлийн эмнэлэг
	 * 3 - Бусад
	 */
	
	@Column(name = "ChangeHospital")
	private byte changeHospital;
	
	@Column(name = "ChangeHospitalDescription")
	private String changeHospitalDescription;
	
	/*
	 * 0 - Хяналтгүй
	 * 1 - Харьяа өрхийн эмч
	 * 2 - Харьяа сумын эмч
	 * 3 - Харьяа аймгийн эмч
	 * 4 - Холбогдох мэргэжлийн эмч
	 * 5 - Бусад
	 */
	
	@Column(name = "CheckDoctor")
	private byte checkDoctor;
	
	@Column(name = "CheckDoctorDescription")
	private String checkDoctorDescription;
	
	/*
	 * 0 - Анхдагчаар
	 * 1 - Хоёрдогчоор
	 */
	
	@Column(name = "Gash")
	private byte gash;
	
	/*
	 * 0 - Үгүй
	 * 1 - Тийм
	 */
	
	@Column(name = "SurgeryProblem")
	private byte surgeryProblem;
	
	@Column(name = "SurgeryProblemDescription")
	private String surgeryProblemDescription;
	
	/*
	 * 0 - Дунд
	 * 1 - Хүндэвтэр
	 * 2 - Хүнд
	 * 3 - Маш хүнд
	 */
	
	@Column(name = "InPatientHood")
	private byte inPatientHood;
	
	/*
	 * 0 - Хөнгөн
	 * 1 - Дунд
	 * 2 - Хүндэвтэр
	 * 3 - Хүнд
	 * 4 - Маш хүнд
	 */
	
	@Column(name = "OutPatientHood")
	private byte outPatientHood;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;

	@Column(name = "CreatedBy")
	private BigDecimal createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;

	@Column(name = "UpdatedBy")
	private BigDecimal updatedBy;
	
	@Transient
	private String displayStr;
	
	public CustomerDispense(){
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getCustomerPkId() {
		return customerPkId;
	}

	public void setCustomerPkId(BigDecimal customerPkId) {
		this.customerPkId = customerPkId;
	}

	public BigDecimal getInpatientHdrPkId() {
		return inpatientHdrPkId;
	}

	public void setInpatientHdrPkId(BigDecimal inpatientHdrPkId) {
		this.inpatientHdrPkId = inpatientHdrPkId;
	}

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public byte getHasCardNote() {
		return hasCardNote;
	}

	public void setHasCardNote(byte hasCardNote) {
		this.hasCardNote = hasCardNote;
	}

	public String getAdvice() {
		return advice;
	}

	public void setAdvice(String advice) {
		this.advice = advice;
	}

	public byte getChangeHospital() {
		return changeHospital;
	}

	public void setChangeHospital(byte changeHospital) {
		this.changeHospital = changeHospital;
	}

	public String getChangeHospitalDescription() {
		return changeHospitalDescription;
	}

	public void setChangeHospitalDescription(String changeHospitalDescription) {
		this.changeHospitalDescription = changeHospitalDescription;
	}

	public byte getCheckDoctor() {
		return checkDoctor;
	}

	public void setCheckDoctor(byte checkDoctor) {
		this.checkDoctor = checkDoctor;
	}

	public String getCheckDoctorDescription() {
		return checkDoctorDescription;
	}

	public void setCheckDoctorDescription(String checkDoctorDescription) {
		this.checkDoctorDescription = checkDoctorDescription;
	}

	public byte getGash() {
		return gash;
	}

	public void setGash(byte gash) {
		this.gash = gash;
	}

	public byte getSurgeryProblem() {
		return surgeryProblem;
	}

	public void setSurgeryProblem(byte surgeryProblem) {
		this.surgeryProblem = surgeryProblem;
	}

	public byte getInPatientHood() {
		return inPatientHood;
	}

	public void setInPatientHood(byte inPatientHood) {
		this.inPatientHood = inPatientHood;
	}

	public byte getOutPatientHood() {
		return outPatientHood;
	}

	public void setOutPatientHood(byte outPatientHood) {
		this.outPatientHood = outPatientHood;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSurgeryProblemDescription() {
		return surgeryProblemDescription;
	}

	public void setSurgeryProblemDescription(String surgeryProblemDescription) {
		this.surgeryProblemDescription = surgeryProblemDescription;
	}

	public String getDisplayStr() {
		displayStr = "color : #ff0000;";
		if (type != 0)
			displayStr = "color : #333;";
		return displayStr;
	}

	public void setDisplayStr(String displayStr) {
		this.displayStr = displayStr;
	}

}
