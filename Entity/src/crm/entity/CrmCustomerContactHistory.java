package crm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "CrmCustomerContactHistory")
public class CrmCustomerContactHistory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	@Column(name="CrmCustomerPkId")
	private BigDecimal crmCustomerPkId;

	//МЭДЭЭЛЭЛИЙН СУВАГ
	@Column(name = "InfoChannel")
	private String infoChannel;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ContactDate")
	private Date contactDate;
	
	@Column(name = "ContactType")
	private String contactType;

	@Column(name="ContactReason")
	private String contactReason;
	
	@Column(name="ContactBy")
	private String contactBy;
	
	@Column(name="ContactNote")
	private String contactNote;
	
	@Column(name="SubOrganizationPkId")
	private BigDecimal subOrganizationPkId;

	@Transient
	private String status;
	
	@Transient
	private String subOrganizationName;
	
	public CrmCustomerContactHistory() {
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public String getInfoChannel() {
		return infoChannel;
	}

	public void setInfoChannel(String infoChannel) {
		this.infoChannel = infoChannel;
	}

	public Date getContactDate() {
		return contactDate;
	}

	public void setContactDate(Date contactDate) {
		this.contactDate = contactDate;
	}

	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getContactReason() {
		return contactReason;
	}

	public void setContactReason(String contactReason) {
		this.contactReason = contactReason;
	}

	public String getContactBy() {
		return contactBy;
	}

	public void setContactBy(String contactBy) {
		this.contactBy = contactBy;
	}

	public String getContactNote() {
		return contactNote;
	}

	public void setContactNote(String contactNote) {
		this.contactNote = contactNote;
	}

	public BigDecimal getSubOrganizationPkId() {
		return subOrganizationPkId;
	}

	public void setSubOrganizationPkId(BigDecimal subOrganizationPkId) {
		this.subOrganizationPkId = subOrganizationPkId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubOrganizationName() {
		return subOrganizationName;
	}

	public void setSubOrganizationName(String subOrganizationName) {
		this.subOrganizationName = subOrganizationName;
	}

	public BigDecimal getCrmCustomerPkId() {
		return crmCustomerPkId;
	}

	public void setCrmCustomerPkId(BigDecimal crmCustomerPkId) {
		this.crmCustomerPkId = crmCustomerPkId;
	}
}