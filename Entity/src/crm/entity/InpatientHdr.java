package crm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.util.Calendar;
import java.text.SimpleDateFormat;

@Entity
@Table(name = "InpatientHdr")

public class InpatientHdr implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;

	@Column(name = "Id")
	private BigDecimal id;

	@Column(name = "CustomerPkId")
	private BigDecimal customerPkId;

	@Column(name = "EmployeePkId")
	private BigDecimal employeePkId;

	@Column(name = "SubOrganizationPkId")
	private BigDecimal subOrganizationPkId;

	@Column(name = "SentEmployeePkId")
	private BigDecimal sentEmployeePkId;

	@Column(name = "SentSubOrganizationPkId")
	private BigDecimal sentSubOrganizationPkId;

	@Column(name = "InspectionPkId")
	private BigDecimal inspectionPkId;

	@Column(name = "RoomPkId")
	private BigDecimal roomPkId;

	@Column(name = "BedInfoPkId")
	private BigDecimal bedInfoPkId;

	@Column(name = "Duration")
	private int duration;

	/**
	 * 0 - Анхан 1 - Давтан
	 */

	@Column(name = "Multiply")
	private byte multiply;

	/**
	 * 0 - Яаралтай биш 1 - Яаралтай
	 */

	@Column(name = "Express")
	private byte express;

	@Column(name = "BeginDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date beginDate;

	@Column(name = "EndDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;

	/**
	 * 6 - Захиалсан 0 - Даатгалын ажилтан зөвшөөрсөн 1 - Хэвтсэн 2 - Гарсан 3 -
	 * Цуцалсан 4 - Шилжсэн 5 - шилжүүлж байгаа
	 */
	@Column(name = "Status")
	private byte status;

	/**
	 * 0 - Сонгоогүй 1 - Даатгалаар 2 - Өвчтөн хариуцсан
	 */

	@Column(name = "PaymentType")
	private int paymentType;

	/**
	 * 1 - 10% 2 - 15%
	 */

	@Column(name = "InsuranceType")
	private int insuranceType;

	/*
	 * 1 - Хөнгөлөлтгүй 2 - Хөнгөлөлттэй
	 */

	@Column(name = "DiscountType")
	private int discountType;

	@Column(name = "DiscountAmount")
	private int discountAmount;

	@Column(name = "DiscountReason")
	private String discountReason;

	@Column(name = "Advice")
	private String advice;

	/**
	 * 0 - Онош тодруулах 1 - Эмчилгээ хийлгэх 2 - Нөхөн сэргээх эмчилгээ
	 */

	@Column(name = "InpatientCause")
	private int inpatientCause;

	@Column(name = "CreatedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "CreatedBy")
	private BigDecimal createdBy;

	@Column(name = "UpdatedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	@Column(name = "UpdatedBy")
	private BigDecimal updatedBy;

	// Сувилагч
	@Column(name = "NursingPkId")
	private BigDecimal nursingPkId;

	/**
	 * Хагалгааны төрөл 1 - орох 2 - орж байгаа 3 - гарсан
	 */
	@Column(name = "SurgeryStatus")
	private byte surgeryStatus;

	/**
	 * Төлбөрөө төлсөн эсэх 0 - төлөөгүй, 1 - төлсөн
	 */
	@Column(name = "HasPayment")
	private int hasPayment;

	@Column(name = "PaymentPkId")
	private BigDecimal paymentPkId;

	/**
	 * Тасгийн сувилагч эмнэлгээс гарах захиалга хүлээж авахад жагсаалтаас
	 * хасагдах болон тасаг шилжүүлэх үед ашиглагдана 1 - гарах захиалга
	 * баталсан , 2 - тасаг хооронд шилжүүлсэн
	 */
	@Column(name = "MoveOut")
	private byte moveOut;

	@Transient
	private boolean surgerySelectStatus;

	@Transient
	private int countDepartmentAccept;

	@Transient
	private int countBed;

	@Transient
	private String toolStatus;

	@Transient
	private String roomNo;

	@Transient
	private String cardNo;

	@Transient
	private String customerName;

	@Transient
	private String customerLastName;

	@Transient
	private String customerRegNo;

	@Transient
	private String employeeName;

	@Transient
	private String subOrganizationName;

	@Transient
	private String insurance;

	@Transient
	private String gender;

	@Transient
	private int customerGender;

	@Transient
	private int age;

	@Transient
	private String phoneNumber;

	@Transient
	private Date inDate;

	@Transient
	private Date outDate;

	@Transient
	private Date birthDate;

	@Transient
	private String inDateString;

	@Transient
	private String outDateString;

	@Transient
	private Date beginTime;

	@Transient
	private Date endTime;

	@Transient
	private Customer customer;

	@Transient
	private int dateChooser;

	@Transient
	private String insuranceCardNumber;

	@Transient
	private String customerDistrict;

	@Transient
	private String customerBuilding;

	@Transient
	private String paymentTypeStr;

	@Transient
	private String insuranceTypeStr;

	@Transient
	private String inpatientCauseStr;

	@Transient
	private BigDecimal typePkId;

	@Transient
	private BigDecimal inpatientDtlPkId;

	@Transient
	private String fonAwesomeStatus;

	@Transient
	private String beginDateString;

	@Transient
	private InpatientHdr inpatientHdr;

	@Transient
	private String nursingName;

	@Transient
	private String serviceName;

	@Transient
	private String beginHour;

	@Transient
	private int medM;

	@Transient
	private int medD;

	@Transient
	private int medE;

	@Transient
	private int medN;

	@Transient
	private int medStatus;

	@Transient
	private String serviceStatus;

	@Transient
	private String inpatientPayment;

	@Transient
	private String bgColor;

	public InpatientHdr() {
		super();
	}

	public Object clone() throws CloneNotSupportedException {
		return (InpatientHdr) super.clone();
	}

	// LogicInpatientCustomer -> getNursingServiceList
	public InpatientHdr(String customerFirstName, String customerlastName, String roomNo, String serviceName,
			String subOrganizationName, String employeeName, int medM, int medD, int medE, int medN) {
		super();
		this.customerName = customerlastName.length() == 0 ? "" : customerlastName.substring(0, 1) + "." + customerFirstName;
		this.roomNo = roomNo;
		this.serviceName = serviceName;
		this.subOrganizationName = subOrganizationName;
		this.employeeName = employeeName;
		this.medM = medM;
		this.medD = medD;
		this.medE = medE;
		this.medN = medN;
	}

	public InpatientHdr(String name, String subName) {
		this.employeeName = name;
		this.subOrganizationName = subName;
	}

	public InpatientHdr(BigDecimal id, byte status) {
		this.id = id;
	}

	public InpatientHdr(InpatientHdr hdr, Customer customer) {
		this.pkId = hdr.getPkId();
		this.customerPkId = hdr.getCustomerPkId();
		this.employeePkId = hdr.getEmployeePkId();
		this.inspectionPkId = hdr.getInspectionPkId();
		this.beginDate = hdr.getBeginDate();
		this.endDate = hdr.getEndDate();
		this.createdDate = hdr.getCreatedDate();
		this.sentSubOrganizationPkId = hdr.getSentSubOrganizationPkId();
		this.sentEmployeePkId = hdr.getSentEmployeePkId();
		this.roomPkId = hdr.getRoomPkId();
		this.createdBy = hdr.getCreatedBy();
		this.subOrganizationPkId = hdr.getSubOrganizationPkId();
		this.multiply = hdr.getMultiply();
		this.express = hdr.getExpress();
		this.duration = hdr.getDuration();
		this.bedInfoPkId = hdr.getBedInfoPkId();
		this.paymentType = hdr.getPaymentType();
		this.insuranceType = hdr.getInsuranceType();
		this.advice = hdr.getAdvice();
		this.inpatientCause = hdr.getInpatientCause();
		this.id = hdr.getId();

		this.customerLastName = customer.getLastName();
		this.customerName = customer.getFirstName();
		this.customerRegNo = customer.getRegNumber();
		this.gender = customer.getGenderString();
		this.cardNo = customer.getCardNumber();
		this.age = customer.getAge();
		this.phoneNumber = customer.getPhoneNumber();

		if (hdr.getSurgeryStatus() == 1)
			fonAwesomeStatus = "<small class='label label-danger'>Орох</small>";
		else if (hdr.getSurgeryStatus() == 2)
			fonAwesomeStatus = "<small class='label label-warning'>Орж байгаа</small>";
		else if (hdr.getSurgeryStatus() == 3)
			fonAwesomeStatus = "<small class='label label-success'>Орсон</small>";
		System.out.println("constructor" + getPkId());

		this.customer = customer;
	}

	// logicEnr ахлах сувилагчид ашигдаж байгаа
	public InpatientHdr(InpatientHdr hdr, Customer c, String employeeName, String subName, String nurseName) {
		this.pkId = hdr.getPkId();
		this.customerPkId = hdr.getCustomerPkId();
		this.employeePkId = hdr.getEmployeePkId();
		this.inspectionPkId = hdr.getInspectionPkId();
		this.beginDate = hdr.getBeginDate();
		this.createdDate = hdr.getCreatedDate();
		this.createdBy = hdr.getCreatedBy();
		this.subOrganizationPkId = hdr.getSubOrganizationPkId();
		this.multiply = hdr.getMultiply();
		this.express = hdr.getExpress();
		this.duration = hdr.getDuration();
		this.paymentType = hdr.getPaymentType();
		this.insuranceType = hdr.getInsuranceType();
		this.advice = hdr.getAdvice();
		this.inpatientCause = hdr.getInpatientCause();
		this.id = hdr.getId();
		this.status = hdr.getStatus();
		this.surgeryStatus = hdr.getSurgeryStatus();
		this.updatedDate = hdr.getUpdatedDate();
		this.roomPkId = hdr.getRoomPkId();
		this.bedInfoPkId = hdr.getBedInfoPkId();
		this.nursingPkId = hdr.getNursingPkId();
		this.hasPayment = hdr.getHasPayment();
		this.sentEmployeePkId = hdr.getSentEmployeePkId();
		this.paymentPkId = hdr.getPaymentPkId();
		this.sentSubOrganizationPkId = hdr.getSentSubOrganizationPkId();
		if (status == 5)
			fonAwesomeStatus = "fa fa-paper-plane-o color-blue";
		else if (status == 4)
			fonAwesomeStatus = "fa fa-check-circle-o color-green";
		else if (hdr.getStatus() == 2)
			fonAwesomeStatus = "fa fa-sign-out color-red";
		this.customerLastName = c.getLastName();
		this.customerName = c.getFirstName();
		this.gender = c.getGenderString();
		this.age = c.getAge();
		this.customerRegNo = c.getRegNumber();
		this.phoneNumber = c.getPhoneNumber();
		this.cardNo = c.getCardNumber();
		this.customerDistrict = c.getDistrict();
		this.customerBuilding = c.getBuilding();
		this.employeeName = employeeName;
		this.subOrganizationName = subName;
		this.nursingName = nurseName;
	}
	
	//LogicEnr -> getListInpateintEnrList()
	public InpatientHdr(InpatientHdr hdr, Customer c, String roomNo, String employeeName, String subName, String nurseName) {
		this.pkId = hdr.getPkId();
		this.customerPkId = hdr.getCustomerPkId();
		this.employeePkId = hdr.getEmployeePkId();
		this.inspectionPkId = hdr.getInspectionPkId();
		this.beginDate = hdr.getBeginDate();
		this.createdDate = hdr.getCreatedDate();
		this.createdBy = hdr.getCreatedBy();
		this.subOrganizationPkId = hdr.getSubOrganizationPkId();
		this.multiply = hdr.getMultiply();
		this.express = hdr.getExpress();
		this.duration = hdr.getDuration();
		this.paymentType = hdr.getPaymentType();
		this.insuranceType = hdr.getInsuranceType();
		this.advice = hdr.getAdvice();
		this.inpatientCause = hdr.getInpatientCause();
		this.id = hdr.getId();
		this.status = hdr.getStatus();
		this.surgeryStatus = hdr.getSurgeryStatus();
		this.updatedDate = hdr.getUpdatedDate();
		this.roomPkId = hdr.getRoomPkId();
		this.bedInfoPkId = hdr.getBedInfoPkId();
		this.nursingPkId = hdr.getNursingPkId();
		this.hasPayment = hdr.getHasPayment();
		this.sentEmployeePkId = hdr.getSentEmployeePkId();
		this.paymentPkId = hdr.getPaymentPkId();
		this.sentSubOrganizationPkId = hdr.getSentSubOrganizationPkId();
		if (status == 5)
			fonAwesomeStatus = "fa fa-paper-plane-o color-blue";
		else if (status == 4)
			fonAwesomeStatus = "fa fa-check-circle-o color-green";
		else if (hdr.getStatus() == 2)
			fonAwesomeStatus = "fa fa-sign-out color-red";
		this.customerLastName = c.getLastName();
		this.customerName = c.getFirstName();
		this.gender = c.getGenderString();
		this.age = c.getAge();
		this.customerRegNo = c.getRegNumber();
		this.phoneNumber = c.getPhoneNumber();
		this.cardNo = c.getCardNumber();
		this.customerDistrict = c.getDistrict();
		this.customerBuilding = c.getBuilding();
		this.employeeName = employeeName;
		this.subOrganizationName = subName;
		this.roomNo = roomNo;
		this.nursingName = nurseName;
	}

	public InpatientHdr(InpatientHdr hdr, Customer c, String roomNu, String employeeName) {
		this.pkId = hdr.getPkId();
		this.customerPkId = hdr.getCustomerPkId();
		this.employeePkId = hdr.getEmployeePkId();
		this.inspectionPkId = hdr.getInspectionPkId();
		this.beginDate = hdr.getBeginDate();
		this.createdDate = hdr.getCreatedDate();
		this.createdBy = hdr.getCreatedBy();
		this.subOrganizationPkId = hdr.getSubOrganizationPkId();
		this.sentEmployeePkId = hdr.getSentEmployeePkId();
		this.sentSubOrganizationPkId = hdr.getSentSubOrganizationPkId();
		this.multiply = hdr.getMultiply();
		this.express = hdr.getExpress();
		this.duration = hdr.getDuration();
		this.paymentType = hdr.getPaymentType();
		this.insuranceType = hdr.getInsuranceType();
		this.advice = hdr.getAdvice();
		this.inpatientCause = hdr.getInpatientCause();
		this.id = hdr.getId();
		this.surgeryStatus = hdr.getSurgeryStatus();
		this.status = hdr.getStatus();
		this.roomPkId = hdr.getRoomPkId();
		this.bedInfoPkId = hdr.getBedInfoPkId();
		this.nursingPkId = hdr.getNursingPkId();
		this.surgeryStatus = hdr.getSurgeryStatus();
		this.hasPayment = hdr.getHasPayment();
		if (hdr.getStatus() == 5)
			fonAwesomeStatus = "fa fa-paper-plane-o color-blue";
		else if (hdr.getStatus() == 4)
			fonAwesomeStatus = "fa fa-check-circle-o color-green";
		else if (hdr.getStatus() == 2)
			fonAwesomeStatus = "fa fa-sign-out color-red";

		this.customerLastName = c.getLastName();
		this.customerName = c.getFirstName();
		this.gender = c.getGenderString();
		this.age = c.getAge();
		this.customerRegNo = c.getRegNumber();
		this.phoneNumber = c.getPhoneNumber();
		this.cardNo = c.getCardNumber();
		this.customerDistrict = c.getDistrict();
		this.customerBuilding = c.getBuilding();
		this.employeeName = employeeName;
		this.roomNo = roomNu;
	}

	public InpatientHdr(InpatientHdr hdr, Customer c, String roomNo, String employeeName, String subName,
			BigDecimal pkId) {
		this.pkId = hdr.getPkId();
		this.customerPkId = hdr.getCustomerPkId();
		this.employeePkId = hdr.getEmployeePkId();
		this.inspectionPkId = hdr.getInspectionPkId();
		this.inspectionPkId = hdr.getInspectionPkId();
		this.beginDate = hdr.getBeginDate();
		this.endDate = hdr.getEndDate();
		this.createdDate = hdr.getCreatedDate();
		this.sentSubOrganizationPkId = hdr.getSentSubOrganizationPkId();
		this.sentEmployeePkId = hdr.getSentEmployeePkId();
		this.roomPkId = hdr.getRoomPkId();
		this.createdBy = hdr.getCreatedBy();
		this.subOrganizationPkId = hdr.getSubOrganizationPkId();
		this.multiply = hdr.getMultiply();
		this.express = hdr.getExpress();
		this.duration = hdr.getDuration();
		this.bedInfoPkId = hdr.getBedInfoPkId();
		this.paymentType = hdr.getPaymentType();
		this.insuranceType = hdr.getInsuranceType();
		this.advice = hdr.getAdvice();
		this.inpatientCause = hdr.getInpatientCause();
		this.id = hdr.getId();

		this.customerLastName = c.getLastName();
		this.customerName = c.getFirstName();
		this.customerRegNo = c.getRegNumber();
		this.customerGender = c.getGender();
		this.cardNo = c.getCardNumber();
		this.age = c.getAge();

		this.roomNo = roomNo;
		this.subOrganizationName = subName;
		this.employeeName = employeeName;
		this.typePkId = pkId;
		if (hdr.getStatus() == 5)
			fonAwesomeStatus = "fa fa-paper-plane-o color-blue";
		else if (hdr.getStatus() == 4)
			fonAwesomeStatus = "fa fa-check-circle-o color-green";
	}

	public InpatientHdr(Date beginDate, String name, Customer c) {
		this.beginDate = beginDate;
		this.subOrganizationName = name;
		this.customerLastName = c.getLastName();
		this.customerName = c.getFirstName();
		this.customerGender = c.getGender();
		this.age = c.getAge();
		this.cardNo = c.getCardNumber();
	}

	public InpatientHdr(BigDecimal pkId, Date beginDate, Date endDate, Customer customer, String firstName,
			String subName, String room, int duration) {
		this.pkId = pkId;
		this.beginDate = beginDate;
		this.endDate = endDate;
		this.customerPkId = customer.getPkId();
		this.customerName = customer.getFirstName();
		this.customerLastName = customer.getLastName();
		this.customerRegNo = customer.getRegNumber();
		this.age = customer.getAge();
		this.cardNo = customer.getCardNumber();
		this.customerGender = customer.getGender();
		this.employeeName = firstName;
		this.subOrganizationName = subName;
		this.roomNo = room;
		this.duration = duration;
	}

	public InpatientHdr(BigDecimal subOrganizationPkId, BigDecimal employeePkId) {
		super();
		this.subOrganizationPkId = subOrganizationPkId;
		this.employeePkId = employeePkId;
	}

	public InpatientHdr(Customer customer, BigDecimal inspectionPkId, BigDecimal subOrganizationPkId,
			String employeeName, String subOrganizationName, Date inDate, Date outDate, int duration, BigDecimal pkId,
			int paymentType, int insuranceType, int inpatientCause, String advice, BigDecimal employeePkId) {
		super();
		this.pkId = pkId;
		this.inspectionPkId = inspectionPkId;
		this.subOrganizationPkId = subOrganizationPkId;
		this.employeePkId = employeePkId;
		this.customerPkId = customer.getPkId();
		this.cardNo = customer.getCardNumber();
		this.customerName = customer.getFirstName();
		this.customerLastName = customer.getLastName();
		this.customerRegNo = customer.getRegNumber();
		this.insuranceCardNumber = customer.getInsuranceCardNumber();
		this.customerDistrict = customer.getDistrict();
		this.customerBuilding = customer.getBuilding();
		this.employeeName = employeeName;
		this.subOrganizationName = subOrganizationName;
		this.insurance = "11";
		if (customer.getGender() == 1)
			this.gender = "Эрэгтэй";
		else if (customer.getGender() == 0)
			this.gender = "Эмэгтэй";
		this.phoneNumber = customer.getPhoneNumber();
		this.age = customer.getAge();
		this.inDate = inDate;
		this.outDate = outDate;
		this.duration = duration;
		this.advice = advice;
		this.paymentType = paymentType;
		if (inpatientCause == 0)
			this.inpatientCauseStr = "Онош тодруулах";
		else if (inpatientCause == 1)
			this.inpatientCauseStr = "Эмчилгээ хийлгэх";
		else if (inpatientCause == 2)
			this.inpatientCauseStr = "Нөхөн сэргээх эмчилгээ";
		if (paymentType == 0)
			this.paymentTypeStr = "Төр хариуцсан";
		else if (paymentType == 1) {
			this.paymentTypeStr = "Даатгалаар";
			if (insuranceType == 0)
				this.insuranceTypeStr = "10%";
			else if (insuranceType == 1)
				this.insuranceTypeStr = "15%";
		} else if (paymentType == 2)
			this.paymentTypeStr = "Өвчтөн хариуцсан";
		this.roomNo = roomNo;

	}

	public InpatientHdr(Customer customer, BigDecimal inspectionPkId, BigDecimal subOrganizationPkId,
			String employeeName, String subOrganizationName, Date inDate, Date outDate, int duration, BigDecimal pkId,
			int paymentType, int insuranceType, int inpatientCause, String advice, BigDecimal employeePkId,
			String roomNo) {
		super();
		this.pkId = pkId;
		this.inspectionPkId = inspectionPkId;
		this.subOrganizationPkId = subOrganizationPkId;
		this.employeePkId = employeePkId;
		this.customerPkId = customer.getPkId();
		this.cardNo = customer.getCardNumber();
		this.customerName = customer.getFirstName();
		this.customerLastName = customer.getLastName();
		this.customerRegNo = customer.getRegNumber();
		this.insuranceCardNumber = customer.getInsuranceCardNumber();
		this.customerDistrict = customer.getDistrict();
		this.customerBuilding = customer.getBuilding();
		this.employeeName = employeeName;
		this.subOrganizationName = subOrganizationName;
		this.insurance = "11";
		if (customer.getGender() == 1)
			this.gender = "Эрэгтэй";
		else if (customer.getGender() == 0)
			this.gender = "Эмэгтэй";
		this.phoneNumber = customer.getPhoneNumber();
		this.age = customer.getAge();
		this.inDate = inDate;
		this.outDate = outDate;
		this.duration = duration;
		this.advice = advice;
		this.paymentType = paymentType;
		if (inpatientCause == 0)
			this.inpatientCauseStr = "Онош тодруулах";
		else if (inpatientCause == 1)
			this.inpatientCauseStr = "Эмчилгээ хийлгэх";
		else if (inpatientCause == 2)
			this.inpatientCauseStr = "Нөхөн сэргээх эмчилгээ";
		if (paymentType == 0)
			this.paymentTypeStr = "Төр хариуцсан";
		else if (paymentType == 1) {
			this.paymentTypeStr = "Даатгалаар";
			if (insuranceType == 0)
				this.insuranceTypeStr = "10%";
			else if (insuranceType == 1)
				this.insuranceTypeStr = "15%";
		} else if (paymentType == 2)
			this.paymentTypeStr = "Өвчтөн хариуцсан";
		this.roomNo = roomNo;

	}

	// Тасгийн сувилагчийн меню хэсгийн стор нээгдэх өвчтөн бас өөр хэсэгт
	// ашигласан
	public InpatientHdr(InpatientHdr ih, Customer c, String roomNo) {
		this.pkId = ih.getPkId();
		this.id = ih.getId();
		this.customerPkId = ih.getCustomerPkId();
		this.subOrganizationPkId = ih.getSubOrganizationPkId();
		this.employeePkId = ih.getEmployeePkId();
		this.inspectionPkId = ih.getInspectionPkId();
		this.roomPkId = ih.getRoomPkId();
		this.express = ih.getExpress();
		this.multiply = ih.getMultiply();
		this.beginDate = ih.getBeginDate();
		this.endDate = ih.getEndDate();
		this.duration = ih.getDuration();
		this.status = ih.getStatus();
		this.createdDate = ih.getCreatedDate();
		this.createdBy = ih.getCreatedBy();
		this.updatedDate = ih.getUpdatedDate();
		this.updatedBy = ih.getUpdatedBy();
		this.customer = c;
		this.roomNo = roomNo;
		
		//Тасгийн  сувилагчийн  меню  хэсэгээс дарж ороход ашигласан 
		this.cardNo = c.getCardNumber();
		this.customerName = c.getFirstName();
		this.customerLastName = c.getLastName();
		this.customerRegNo = c.getRegNumber();
		this.insuranceCardNumber = c.getInsuranceCardNumber();
		this.customerDistrict = c.getDistrict();
		this.customerBuilding = c.getBuilding();
		this.age = c.getAge();

	}

	public void dateChanger() {
		endDate = getBeginDate();
		Calendar c = Calendar.getInstance();
		c.setTime(endDate);
		c.add(Calendar.DATE, duration);
		setEndDate(c.getTime());
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public BigDecimal getCustomerPkId() {
		return customerPkId;
	}

	public void setCustomerPkId(BigDecimal customerPkId) {
		this.customerPkId = customerPkId;
	}

	public BigDecimal getEmployeePkId() {
		return employeePkId;
	}

	public void setEmployeePkId(BigDecimal employeePkId) {
		this.employeePkId = employeePkId;
	}

	public BigDecimal getSubOrganizationPkId() {
		if (subOrganizationPkId == null)
			subOrganizationPkId = BigDecimal.ZERO;
		return subOrganizationPkId;
	}

	public void setSubOrganizationPkId(BigDecimal subOrganizationPkId) {
		this.subOrganizationPkId = subOrganizationPkId;
	}

	public BigDecimal getSentEmployeePkId() {
		return sentEmployeePkId;
	}

	public void setSentEmployeePkId(BigDecimal sentEmployeePkId) {
		this.sentEmployeePkId = sentEmployeePkId;
	}

	public BigDecimal getSentSubOrganizationPkId() {
		return sentSubOrganizationPkId;
	}

	public void setSentSubOrganizationPkId(BigDecimal sentSubOrganizationPkId) {
		this.sentSubOrganizationPkId = sentSubOrganizationPkId;
	}

	public BigDecimal getInspectionPkId() {
		return inspectionPkId;
	}

	public void setInspectionPkId(BigDecimal inspectionPkId) {
		this.inspectionPkId = inspectionPkId;
	}

	public BigDecimal getRoomPkId() {
		return roomPkId;
	}

	public void setRoomPkId(BigDecimal roomPkId) {
		this.roomPkId = roomPkId;
	}

	public BigDecimal getBedInfoPkId() {
		return bedInfoPkId;
	}

	public void setBedInfoPkId(BigDecimal bedInfoPkId) {
		this.bedInfoPkId = bedInfoPkId;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public byte getMultiply() {
		return multiply;
	}

	public void setMultiply(byte multiply) {
		this.multiply = multiply;
	}

	public byte getExpress() {
		return express;
	}

	public void setExpress(byte express) {
		this.express = express;
	}

	public Date getBeginDate() {
		if (beginDate == null)
			beginDate = new Date();
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		if (endDate == null)
			endDate = new Date();
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public int getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}

	public int getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(int insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getAdvice() {
		return advice;
	}

	public void setAdvice(String advice) {
		this.advice = advice;
	}

	public int getInpatientCause() {
		return inpatientCause;
	}

	public void setInpatientCause(int inpatientCause) {
		this.inpatientCause = inpatientCause;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getCountBed() {
		return countBed;
	}

	public void setCountBed(int countBed) {
		this.countBed = countBed;
	}

	public String getToolStatus() {
		return toolStatus;
	}

	public void setToolStatus(String toolStatus) {
		this.toolStatus = toolStatus;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getSubOrganizationName() {
		return subOrganizationName;
	}

	public void setSubOrganizationName(String subOrganizationName) {
		this.subOrganizationName = subOrganizationName;
	}

	public String getInsurance() {
		return insurance;
	}

	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Date getInDate() {
		return inDate;
	}

	public void setInDate(Date inDate) {
		this.inDate = inDate;
	}

	public Date getOutDate() {
		return outDate;
	}

	public void setOutDate(Date outDate) {
		this.outDate = outDate;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getInDateString() {
		inDateString = new SimpleDateFormat("yyyy-MM-dd").format(inDate);
		return inDateString;
	}

	public void setInDateString(String inDateString) {
		this.inDateString = inDateString;
	}

	public String getOutDateString() {
		outDateString = new SimpleDateFormat("yyyy-MM-dd").format(outDate);
		return outDateString;
	}

	public void setOutDateString(String outDateString) {
		this.outDateString = outDateString;
	}

	public Date getBeginTime() {
		if (beginTime == null)
			beginTime = new Date();
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		if (endTime == null) {
			endTime = new Date();
		}
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getDateStringWithTime(Date d) {
		return new SimpleDateFormat("yyyy-MM-dd hh:mm").format(d);
	}

	public String getDateString(Date d) {
		return new SimpleDateFormat("yyyy-MM-dd").format(d);
	}

	public int getDateChooser() {
		return dateChooser;
	}

	public void setDateChooser(int dateChooser) {
		this.dateChooser = dateChooser;
	}

	public String getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

	public String getCustomerRegNo() {
		return customerRegNo;
	}

	public void setCustomerRegNo(String customerRegNo) {
		this.customerRegNo = customerRegNo;
	}

	public String getInsuranceCardNumber() {
		return insuranceCardNumber;
	}

	public void setInsuranceCardNumber(String insuranceCardNumber) {
		this.insuranceCardNumber = insuranceCardNumber;
	}

	public String getCustomerDistrict() {
		return customerDistrict;
	}

	public void setCustomerDistrict(String customerDistrict) {
		this.customerDistrict = customerDistrict;
	}

	public String getCustomerBuilding() {
		return customerBuilding;
	}

	public void setCustomerBuilding(String customerBuilding) {
		this.customerBuilding = customerBuilding;
	}

	public String getPaymentTypeStr() {
		if (paymentType == 0)
			paymentTypeStr = "Сонгох";
		else if (paymentType == 1)
			paymentTypeStr = "Даатгал";
		else if (paymentType == 2)
			paymentTypeStr = "Төлбөрт";
		return paymentTypeStr;
	}

	public void setPaymentTypeStr(String paymentTypeStr) {
		this.paymentTypeStr = paymentTypeStr;
	}

	public String getInsuranceTypeStr() {
		return insuranceTypeStr;
	}

	public void setInsuranceTypeStr(String insuranceTypeStr) {
		this.insuranceTypeStr = insuranceTypeStr;
	}

	public String getInpatientCauseStr() {
		if (inpatientCause == 0)
			inpatientCauseStr = "Онош тодруулах";
		else if (inpatientCause == 1)
			inpatientCauseStr = "Эмчилгээ хийлгэх";
		else if (inpatientCause == 2)
			inpatientCauseStr = "Нөхөн сэргээх эмчилгээ";
		return inpatientCauseStr;
	}

	public void setInpatientCauseStr(String inpatientCauseStr) {
		this.inpatientCauseStr = inpatientCauseStr;
	}

	public int getCustomerGender() {
		return customerGender;
	}

	public void setCustomerGender(int customerGender) {
		this.customerGender = customerGender;
	}

	public String getDateFormat(Date d) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(d);
	}

	public BigDecimal getTypePkId() {
		return typePkId;
	}

	public void setTypePkId(BigDecimal typePkId) {
		this.typePkId = typePkId;
	}

	public String getDateConvert(Date d) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return format.format(d);
	}

	public BigDecimal getInpatientDtlPkId() {
		return inpatientDtlPkId;
	}

	public void setInpatientDtlPkId(BigDecimal inpatientDtlPkId) {
		this.inpatientDtlPkId = inpatientDtlPkId;
	}

	public String getFonAwesomeStatus() {
		return fonAwesomeStatus;
	}

	public void setFonAwesomeStatus(String fonAwesomeStatus) {
		this.fonAwesomeStatus = fonAwesomeStatus;
	}

	public int getCountDepartmentAccept() {
		return countDepartmentAccept;
	}

	public void setCountDepartmentAccept(int countDepartmentAccept) {
		this.countDepartmentAccept = countDepartmentAccept;
	}

	public BigDecimal getNursingPkId() {
		return nursingPkId;
	}

	public void setNursingPkId(BigDecimal nursingPkId) {
		this.nursingPkId = nursingPkId;
	}

	public boolean isSurgerySelectStatus() {
		return this.surgeryStatus == 1 ? true : false;
	}

	public void setSurgerySelectStatus(boolean surgerySelectStatus) {
		this.surgeryStatus = (byte) (surgerySelectStatus ? 1 : 0);
	}

	public byte getSurgeryStatus() {
		return surgeryStatus;
	}

	public void setSurgeryStatus(byte surgeryStatus) {
		this.surgeryStatus = surgeryStatus;
	}

	public InpatientHdr getInpatientHdr() {
		return inpatientHdr;
	}

	public void setInpatientHdr(InpatientHdr inpatientHdr) {
		this.inpatientHdr = inpatientHdr;
	}

	public String getBeginDateString() {
		if (beginDate != null)
			beginDateString = new SimpleDateFormat("yyyy-MM-dd").format(getBeginDate());
		return beginDateString;
	}

	public void setBeginDateString(String beginDateString) {
		this.beginDateString = beginDateString;
	}

	public int getDiscountType() {
		return discountType;
	}

	public void setDiscountType(int discountType) {
		this.discountType = discountType;
	}

	public int getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(int discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getDiscountReason() {
		return discountReason;
	}

	public void setDiscountReason(String discountReason) {
		this.discountReason = discountReason;
	}

	public int getHasPayment() {
		return hasPayment;
	}

	public void setHasPayment(int hasPayment) {
		this.hasPayment = hasPayment;
	}

	public String getNursingName() {
		return nursingName;
	}

	public void setNursingName(String nursingName) {
		this.nursingName = nursingName;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getBeginHour() {
		return beginHour;
	}

	public void setBeginHour(String beginHour) {
		this.beginHour = beginHour;
	}

	public int getMedM() {
		return medM;
	}

	public void setMedM(int medM) {
		this.medM = medM;
	}

	public int getMedD() {
		return medD;
	}

	public void setMedD(int medD) {
		this.medD = medD;
	}

	public int getMedE() {
		return medE;
	}

	public void setMedE(int medE) {
		this.medE = medE;
	}

	public int getMedN() {
		return medN;
	}

	public void setMedN(int medN) {
		this.medN = medN;
	}

	public String getServiceStatus() {
		if (medStatus == 1)
			serviceStatus = "Захиалсан";
		else if (medStatus == 2)
			serviceStatus = "Хэрэгжүүлсэн";
		else if (medStatus == 3)
			serviceStatus = "Зогсоосон";
		else if (medStatus == 4)
			serviceStatus = "Цуцалсан";
		else if (medStatus == 5)
			serviceStatus = "Татгалзсан";
		return serviceStatus;
	}

	public void setServiceStatus(String serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	public int getMedStatus() {
		return medStatus;
	}

	public void setMedStatus(int medStatus) {
		this.medStatus = medStatus;
	}

	public String getBgColor() {
		if (medStatus == 1)
			bgColor = "gray";
		else if (medStatus == 2)
			bgColor = "green";
		else if (medStatus == 3)
			bgColor = "red";
		else if (medStatus == 4)
			bgColor = "yellow";
		else if (medStatus == 5)
			bgColor = "red";
		return bgColor;
	}

	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}

	public BigDecimal getPaymentPkId() {
		return paymentPkId;
	}

	public void setPaymentPkId(BigDecimal paymentPkId) {
		this.paymentPkId = paymentPkId;
	}

	public byte getMoveOut() {
		return moveOut;
	}

	public void setMoveOut(byte moveOut) {
		this.moveOut = moveOut;
	}

	public String getInpatientPayment() {
		return inpatientPayment;
	}

	public void setInpatientPayment(String inpatientPayment) {
		this.inpatientPayment = inpatientPayment;
	}
}