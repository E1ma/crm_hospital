package crm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Diagnose")
public class Diagnose implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "PkId", nullable = false, length = 18)
	private BigDecimal pkId;
	
	@Column(name = "Id")
	private String id;
	
	@Column(name = "NameMn")
	private String nameMn;
	
	@Column(name = "NameEn")
	private String nameEn;
	
	@Column(name = "NameRu")
	private String nameRu;
	
	@Column(name = "DiagnoseGroupPkId")
	private BigDecimal diagnoseGroupPkId;
	
	@Transient
	private String status;
	
	@Transient
	private String diagnoseGroupName;
	
	@Transient
	private BigDecimal inspectionPkId;
	
	public Diagnose() {
		super();
	}
	
	public Diagnose(String nameEn, String id){
		super();
		this.nameEn=nameEn;
		this.id =id;
	}
	
	public Diagnose(Diagnose a, BigDecimal inspectionPkId){
		super();
		this.pkId = a.getPkId();
		this.id = a.getId();
		this.nameMn = a.getNameMn();
		this.nameEn = a.getNameEn();
		this.nameRu = a.getNameRu();
		this.inspectionPkId = inspectionPkId;
	}
	
	public Diagnose(BigDecimal pkId, String id, String nameEn, String nameMn){
		super();
		this.pkId = pkId;
		this.id = id;
		this.nameEn = nameEn;
		this.nameMn = nameMn;
	}
	
	public Diagnose(Diagnose diagnose, long count){
		this.pkId = diagnose.getPkId();
		this.id = diagnose.getId();
		this.nameMn = diagnose.getNameMn();
		this.nameEn = diagnose.getNameEn();
		this.nameRu = diagnose.getNameRu();
	}
	
	//InfoLogic -> getListDiagnose
	public Diagnose(Diagnose diagnose, String diagnoseGroupName){
		this.pkId = diagnose.getPkId();
		this.id = diagnose.getId();
		this.nameMn = diagnose.getNameMn();
		this.nameEn = diagnose.getNameEn();
		this.nameRu = diagnose.getNameRu();
		this.diagnoseGroupName = diagnoseGroupName;
	}
	
	public BigDecimal getPkId() {
		return pkId;
	}
	
	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getNameEn() {
		return nameEn;
	}
	
	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}
	
	public String getNameMn() {
		return nameMn;
	}
	
	public void setNameMn(String nameMn) {
		this.nameMn = nameMn;
	}
	
	public String getNameRu() {
		return nameRu;
	}
	
	public void setNameRu(String nameRu) {
		this.nameRu = nameRu;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public BigDecimal getInspectionPkId() {
		return inspectionPkId;
	}
	
	public void setInspectionPkId(BigDecimal inspectionPkId) {
		this.inspectionPkId = inspectionPkId;
	}

	public BigDecimal getDiagnoseGroupPkId() {
		return diagnoseGroupPkId;
	}

	public void setDiagnoseGroupPkId(BigDecimal diagnoseGroupPkId) {
		this.diagnoseGroupPkId = diagnoseGroupPkId;
	}

	public String getDiagnoseGroupName() {
		return diagnoseGroupName;
	}

	public void setDiagnoseGroupName(String diagnoseGroupName) {
		this.diagnoseGroupName = diagnoseGroupName;
	}	
}