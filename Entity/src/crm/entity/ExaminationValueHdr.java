package crm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "ExaminationValueHdr")
public class ExaminationValueHdr implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;

	@Column(name = "RequestPkId")
	private BigDecimal requestPkId;

	@Column(name = "QuestionPkId")
	private BigDecimal questionPkId;

	@Column(name = "AnswerPkId")
	private BigDecimal answerPkId;

	@Column(name = "Value")
	private String value;

	@Column(name = "CreatedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "CreatedBy")
	private BigDecimal createdBy;

	@Column(name = "UpdatedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	@Column(name = "UpdatedBy")
	private BigDecimal updatedBy;

	@Column(name = "LaborantPkId")
	private BigDecimal laborantPkId;

	@Column(name = "DoctorPkId")
	private BigDecimal doctorPkId;

	@Transient
	private String status;

	@Transient
	private String minValue;

	@Transient
	private String maxValue;
	
	@Transient
	private String name;

	public ExaminationValueHdr() {
		super();
	}

	public ExaminationValueHdr(ExaminationValueHdr evh, String minValue, String maxValue) {
		super();
		this.pkId = evh.getPkId();
		this.requestPkId = evh.getRequestPkId();
		this.answerPkId = evh.getAnswerPkId();
		this.questionPkId = evh.getQuestionPkId();
		this.value = evh.getValue();
		this.createdBy = evh.getCreatedBy();
		this.createdDate = evh.getCreatedDate();
		this.updatedBy = evh.getUpdatedBy();
		this.updatedDate = evh.getUpdatedDate();
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	
	public ExaminationValueHdr(String name, String value, BigDecimal questionPkId){
		super();
		this.value = value;
		this.name = name;
		this.questionPkId = questionPkId;
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getRequestPkId() {
		return requestPkId;
	}

	public void setRequestPkId(BigDecimal requestPkId) {
		this.requestPkId = requestPkId;
	}

	public BigDecimal getQuestionPkId() {
		return questionPkId;
	}

	public void setQuestionPkId(BigDecimal questionPkId) {
		this.questionPkId = questionPkId;
	}

	public BigDecimal getAnswerPkId() {
		return answerPkId;
	}

	public void setAnswerPkId(BigDecimal answerPkId) {
		this.answerPkId = answerPkId;
	}

	public String getValue() {
		String retValue = value;
		if (minValue != null)
			if (value != null)
				if (Float.parseFloat(minValue) > Float.parseFloat(value))
					retValue = value + " <span style = 'color:red !important'> L </span>";
		if (maxValue != null)
			if (value != null)
				if (Float.parseFloat(maxValue) < Float.parseFloat(value))
					retValue = value + " <span style = 'color:red !important'> H </span>";

		return retValue;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public BigDecimal getDoctorPkId() {
		return doctorPkId;
	}

	public void setDoctorPkId(BigDecimal doctorPkId) {
		this.doctorPkId = doctorPkId;
	}

	public BigDecimal getLaborantPkId() {
		return laborantPkId;
	}

	public void setLaborantPkId(BigDecimal laborantPkId) {
		this.laborantPkId = laborantPkId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
