package crm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "CrmCustomer")
public class CrmCustomer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;

	@Column(name = "CustomerPkId")
	private BigDecimal customerPkId;
	
	//ГЭРЛЭЛТИЙН БАЙДАЛ
	@Column(name = "WeddingStatus")
	private String weddingStatus;
	
	//ЦУСНЫ БҮЛЭГ
	//А - 2
	//В - 3
	//АВ - 4
	//0 - 1
	@Column(name = "BloodType")
	private String bloodType;

	//МЭДЭЭЛЭЛ АВАХ СУВАГ
	@Column(name = "FeedBackType")
	private String feedBackType;

	//ЗЭРЭГЛЭЛ
	//VIP - VIP
	//General - ЭНГИЙН
	//Vulnerable - ЭМЗЭГ БҮЛЭГ
	@Column(name = "LifeCategory")
	private String lifeCategory;
	
	//ХЯНАЛТТАЙ ЭСЭХ
	@Column(name = "ControlType")
	private String controlType;
	
	@Column(name = "InsuranceInfo")
	private String insuranceInfo;

	@Transient
	private String status;

	@Transient
	private String bloodTypeStr;

	@Transient
	private String weddingStatusStr;
	
	@Transient
	private String lifeCategoryStr;
	
	@Transient
	private String insuranceInfoStr;

	@Transient
	private String cardNumber;
	
	@Transient
	private Customer customer;
	
	@Transient
	private boolean infoCheck;
	
	@Transient
	private boolean mailCheck;
	
	@Transient
	private boolean smsCheck;
	
	@Transient
	private boolean dmCheck;
	
	@Transient
	private boolean snsCheck;
	
	@Transient
	private boolean selected;
	
	@Transient
	private String bgColor;
	
	@Transient
	private BigDecimal typePkId;
	
	@Transient
	private String typeName;
	
	@Transient
	private String orderId;
	
	@Transient
	private Date orderDate;

	public CrmCustomer() {
		super();
	}
	
	//InfoLogic -> getCrmCustomers
	public CrmCustomer(Customer customer, CrmCustomer cus) {
		super();
		this.pkId = cus.getPkId();
		this.customer = customer;
		this.customerPkId = cus.getCustomerPkId();
		this.weddingStatus = cus.getWeddingStatus();
		this.bloodType = cus.getBloodType();
		this.feedBackType = cus.getFeedBackType();
		this.lifeCategory = cus.getLifeCategory();
		this.controlType = cus.getControlType();
		this.insuranceInfo = cus.getInsuranceInfo();
	}
	
	//CrmLogic -> autoSendMailOrSms
	public CrmCustomer(Customer customer, CrmCustomer cus, BigDecimal typePkId, String typeName, String orderId, Date orderDate) {
		super();
		this.pkId = cus.getPkId();
		this.customer = customer;
		this.customerPkId = cus.getCustomerPkId();
		this.weddingStatus = cus.getWeddingStatus();
		this.bloodType = cus.getBloodType();
		this.feedBackType = cus.getFeedBackType();
		this.lifeCategory = cus.getLifeCategory();
		this.controlType = cus.getControlType();
		this.insuranceInfo = cus.getInsuranceInfo();
		this.typePkId = typePkId;
		this.typeName = typeName;
		this.orderId = orderId;
		this.orderDate = orderDate;
	}

	//LogicCrm -> getCustomerSearch ХАРИЛЦАГЧИЙН ХАЙЛТ
	public CrmCustomer(Customer customer, String lifeCategory, String feedBackType) {
		super();
		this.customer = customer;
		this.lifeCategory = lifeCategory;
		this.feedBackType = feedBackType;
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public String getWeddingStatus() {
		return weddingStatus;
	}

	public void setWeddingStatus(String weddingStatus) {
		this.weddingStatus = weddingStatus;
	}

	public String getBloodType() {
		return bloodType;
	}

	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}

	public String getFeedBackType() {
		return feedBackType;
	}

	public void setFeedBackType(String feedBackType) {
		this.feedBackType = feedBackType;
	}

	public BigDecimal getCustomerPkId() {
		return customerPkId;
	}

	public void setCustomerPkId(BigDecimal customerPkId) {
		this.customerPkId = customerPkId;
	}

	public String getLifeCategory() {
		return lifeCategory;
	}

	public void setLifeCategory(String lifeCategory) {
		this.lifeCategory = lifeCategory;
	}

	public String getControlType() {
		return controlType;
	}

	public void setControlType(String controlType) {
		this.controlType = controlType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public boolean isInfoCheck() {
		if(getStatus() == null)
		{
			infoCheck = false;
			if(getFeedBackType() != null && !getFeedBackType().equals(""))
				infoCheck = true;
		}
		return infoCheck;
	}

	public void setInfoCheck(boolean infoCheck) {
		this.infoCheck = infoCheck;
	}

	public boolean isMailCheck() {
		if(getStatus() == null)
		{
			mailCheck = false;
			if(getFeedBackType() != null && getFeedBackType().contains("Mail"))
				mailCheck = true;
		}
		if(!isInfoCheck())
			mailCheck = false;
		return mailCheck;
	}

	public void setMailCheck(boolean mailCheck) {
		this.mailCheck = mailCheck;
	}

	public boolean isSmsCheck() {
		if(getStatus() == null)
		{
			smsCheck = false;
			if(getFeedBackType() != null && getFeedBackType().contains("SMS"))
				smsCheck = true;
		}
		if(!isInfoCheck())
			smsCheck = false;
		return smsCheck;
	}

	public void setSmsCheck(boolean smsCheck) {
		this.smsCheck = smsCheck;
	}

	public boolean isDmCheck() {
		if(getStatus() == null)
		{
			dmCheck = false;
			if(getFeedBackType() != null && getFeedBackType().contains("DM"))
				dmCheck = true;
		}
		if(!isInfoCheck())
			dmCheck = false;
		return dmCheck;
	}

	public void setDmCheck(boolean dmCheck) {
		this.dmCheck = dmCheck;
	}

	public boolean isSnsCheck() {
		if(getStatus() == null)
		{
			snsCheck = false;
			if(getFeedBackType() != null && getFeedBackType().contains("SNS"))
				snsCheck = true;
		}
		if(!isInfoCheck())
			snsCheck = false;
		return snsCheck;
	}

	public void setSnsCheck(boolean snsCheck) {
		this.snsCheck = snsCheck;
	}
	
	public String getBloodTypeStr() {
		if(bloodType == null)
			bloodTypeStr = "";
		else if(bloodType.equals("2"))
			bloodTypeStr = "A";
		else if(bloodType.equals("3"))
			bloodTypeStr = "B";
		else if(bloodType.equals("4"))
			bloodTypeStr = "AB";
		else if(bloodType.equals("1"))
			bloodTypeStr = "0";
		return bloodTypeStr;
	}

	public void setBloodTypeStr(String bloodTypeStr) {
		this.bloodTypeStr = bloodTypeStr;
	}

	public String getWeddingStatusStr() {
		if(weddingStatus == null)
			weddingStatusStr = "";
		else if(weddingStatus.equals("Wedding"))
			weddingStatusStr = "Гэрлэсэн";
		else if(weddingStatus.equals("NotWedding"))
			weddingStatusStr = "Гэрлээгүй";
		return weddingStatusStr;
	}

	public void setWeddingStatusStr(String weddingStatusStr) {
		this.weddingStatusStr = weddingStatusStr;
	}
	
	public String getLifeCategoryStr() {
		if(lifeCategory == null)
			lifeCategoryStr = "";
		else if(lifeCategory.equals("Vip"))
			lifeCategoryStr = "VIP";
		else if(lifeCategory.equals("General"))
			lifeCategoryStr = "Энгийн";
		else if(lifeCategory.equals("Vulnerable"))
			lifeCategoryStr = "Эмзэг бүлэг";
		return lifeCategoryStr;
	}

	public void setLifeCategoryStr(String lifeCategoryStr) {
		this.lifeCategoryStr = lifeCategoryStr;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getBgColor() {
		if(feedBackType == null || feedBackType.equals(""))
			bgColor = "yellow";
		return bgColor;
	}

	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getInsuranceInfo() {
		return insuranceInfo;
	}

	public void setInsuranceInfo(String insuranceInfo) {
		this.insuranceInfo = insuranceInfo;
	}

	public String getInsuranceInfoStr() {
		if(insuranceInfo == null)
			insuranceInfoStr = "";
		else if(insuranceInfo.equals("insurance"))
			insuranceInfoStr = "Даатгалтай";
		else if(insuranceInfo.equals("notInsurance"))
			insuranceInfoStr = "Даатгалгүй";
		return insuranceInfoStr;
	}

	public void setInsuranceInfoStr(String insuranceInfoStr) {
		this.insuranceInfoStr = insuranceInfoStr;
	}

	public BigDecimal getTypePkId() {
		return typePkId;
	}

	public void setTypePkId(BigDecimal typePkId) {
		this.typePkId = typePkId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
}