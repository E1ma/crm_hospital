package crm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "CrmCustomerFamily")
public class CrmCustomerFamily implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;

	//ХОЛБОО
	@Column(name = "FamilyName")
	private String familyName;
	
	@Column(name = "Name")
	private String name;
	
	@Column(name = "BirthDate")
	private String birthDate;

	@Column(name = "Gender")
	private String gender;

	@Column(name = "PhoneNumber")
	private String phoneNumber;
	
	@Column(name = "TelePhone")
	private String telePhone;
	
	@Column(name="CrmCustomerPkId")
	private BigDecimal crmCustomerPkId;

	@Transient
	private String status;
	
	public CrmCustomerFamily() {
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getTelePhone() {
		return telePhone;
	}

	public void setTelePhone(String telePhone) {
		this.telePhone = telePhone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getCrmCustomerPkId() {
		return crmCustomerPkId;
	}

	public void setCrmCustomerPkId(BigDecimal crmCustomerPkId) {
		this.crmCustomerPkId = crmCustomerPkId;
	}
}