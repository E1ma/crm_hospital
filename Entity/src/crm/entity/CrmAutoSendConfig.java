package crm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "CrmAutoSendConfig")
public class CrmAutoSendConfig implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	@Column(name = "Id")
	private String id;
	
	@Column(name = "CrmTreatmentConditionPkId")
	private BigDecimal crmTreatmentConditionPkId;
	
	@Column(name = "IsActive")
	private String isActive;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ConfigDate")
	private Date configDate;
	
	@Column(name = "ChannelType")
	private String channelType;
	
	@Column(name = "DayType")
	private String dayType;
	
	@Column(name = "DayCount")
	private int dayCount;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "CreatedBy")
	private BigDecimal createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date UpdatedDate;
	
	@Column(name = "UpdatedBy")
	private BigDecimal UpdatedBy;
	
	@Transient
	private String status;
	
	@Transient
	private String name;
	
	@Transient
	private String activeStr;
	
	@Transient
	private boolean activeBool;
	
	public CrmAutoSendConfig(){
		super();
	}
	
	public CrmAutoSendConfig(String id, String crmTreatmentConditionName, String channelType, int dayCount, 
			String dayType, String isActive, Date configDate){
		this.id = id;
		this.name = crmTreatmentConditionName;
		this.channelType = channelType;
		this.dayCount = dayCount;
		this.dayType = dayType;
		this.isActive = isActive;
		this.configDate = configDate;
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getCrmTreatmentConditionPkId() {
		return crmTreatmentConditionPkId;
	}

	public void setCrmTreatmentConditionPkId(BigDecimal crmTreatmentConditionPkId) {
		this.crmTreatmentConditionPkId = crmTreatmentConditionPkId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public Date getConfigDate() {
		return configDate;
	}

	public void setConfigDate(Date configDate) {
		this.configDate = configDate;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	public String getDayType() {
		return dayType;
	}

	public void setDayType(String dayType) {
		this.dayType = dayType;
	}

	public int getDayCount() {
		return dayCount;
	}

	public void setDayCount(int dayCount) {
		this.dayCount = dayCount;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return UpdatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		UpdatedDate = updatedDate;
	}

	public BigDecimal getUpdatedBy() {
		return UpdatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		UpdatedBy = updatedBy;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getActiveStr() {
		activeStr = isActive.equals("Y") ? "Тийм" : "Үгүй";
		return activeStr;
	}

	public void setActiveStr(String activeStr) {
		this.activeStr = activeStr;
	}

	public boolean isActiveBool() {
		activeBool = isActive == "Y";
		return activeBool;
	}

	public void setActiveBool(boolean activeBool) {
		
		if(activeBool == true)
			isActive = "Y";
		else
			isActive = " ";
			
		this.activeBool = activeBool;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}