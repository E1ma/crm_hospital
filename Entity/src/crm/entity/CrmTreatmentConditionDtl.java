package crm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "CrmTreatmentConditionDtl")
public class CrmTreatmentConditionDtl implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	@Column(name="Type")
	private String type;
	
	@Column(name="TypePkId")
	private BigDecimal typePkId;
	
	@Column(name="HdrPkId")
	private BigDecimal hdrPkId;
	
	@Column(name="BeginAge")
	private int beginAge;
	
	@Column(name="EndAge")
	private int endAge;
	
	@Column(name="AimagPkId")
	private BigDecimal aimagPkId;
	
	@Column(name="SoumPkId")
	private BigDecimal soumPkId;
	
	@Transient
	private String typeName;
	
	@Transient
	private String depName;
	
	@Transient
	private String examId;
	
	@Transient
	private String diagnoseId;
	
	@Transient
	private String aimagName;
	
	@Transient
	private String soumName;
	
	public Object clone() throws CloneNotSupportedException {
		return (CrmTreatmentConditionDtl) super.clone();
	}
	
	public CrmTreatmentConditionDtl()
	{
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getTypePkId() {
		return typePkId;
	}

	public void setTypePkId(BigDecimal typePkId) {
		this.typePkId = typePkId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getDepName() {
		return depName;
	}

	public void setDepName(String depName) {
		this.depName = depName;
	}

	public String getExamId() {
		return examId;
	}

	public void setExamId(String examId) {
		this.examId = examId;
	}

	public String getDiagnoseId() {
		return diagnoseId;
	}

	public void setDiagnoseId(String diagnoseId) {
		this.diagnoseId = diagnoseId;
	}

	public int getBeginAge() {
		return beginAge;
	}

	public void setBeginAge(int beginAge) {
		this.beginAge = beginAge;
	}

	public int getEndAge() {
		return endAge;
	}

	public void setEndAge(int endAge) {
		this.endAge = endAge;
	}

	public BigDecimal getAimagPkId() {
		return aimagPkId;
	}

	public void setAimagPkId(BigDecimal aimagPkId) {
		this.aimagPkId = aimagPkId;
	}

	public BigDecimal getSoumPkId() {
		return soumPkId;
	}

	public void setSoumPkId(BigDecimal soumPkId) {
		this.soumPkId = soumPkId;
	}

	public String getAimagName() {
		return aimagName;
	}

	public void setAimagName(String aimagName) {
		this.aimagName = aimagName;
	}

	public String getSoumName() {
		return soumName;
	}

	public void setSoumName(String soumName) {
		this.soumName = soumName;
	}

	public BigDecimal getHdrPkId() {
		return hdrPkId;
	}

	public void setHdrPkId(BigDecimal hdrPkId) {
		this.hdrPkId = hdrPkId;
	}
}