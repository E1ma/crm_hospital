package crm.businessentity;

import java.math.BigDecimal;
import java.util.Date;

public class CrmInfo {
	
	private Date beginDate;
	private Date endDate;
	private Date examDate;
	private Date surgeryDate;
	private String doctorName;
	private String surgeryName;
	private String subOrgName;
	private String diagnoseName;
	private String result;
	private String type;
	private String examinationName;
	private String status;
	private String statusMn;
	private String unit;
	private String questionName;
	private String minValue;
	private String maxValue;
	private BigDecimal examPkId;

	public CrmInfo() {
		super();
	}
	
	//LogicCrm -> getTreatmentInfo() ХЭВТЭН ЭМЧИЛГЭЭНИЙ ТАБ
	public CrmInfo(Date beginDate, Date endDate, String doctorName, String subOrgName, String diagnoseName, byte outPatientHood) {
		super();
		this.beginDate = beginDate;
		this.endDate = endDate;
		this.doctorName = doctorName;
		this.subOrgName = subOrgName;
		this.diagnoseName = diagnoseName;
		this.type = Tool.INPATIENTDTLSTATUS;
		if((int)outPatientHood == 0)
			this.result = "Хөнгөн";
		else if((int)outPatientHood == 1)
			this.result = "Дунд";
		else if((int)outPatientHood == 2)
			this.result = "Хүндэвтэр";
		else if((int)outPatientHood == 3)
			this.result = "Хүнд";
		else if((int)outPatientHood == 4)
			this.result = "Маш хүнд";
	}
	
	//LogicCrm -> getTreatmentInfo() ШИНЖИЛГЭЭНИЙ
	public CrmInfo(String examinationName, Date examDate, BigDecimal pkId, String status) {
		super();
		this.examDate = examDate;
		this.examinationName = examinationName;
		this.status = status;
		this.examPkId = pkId;
		this.type = Tool.INSPECTIONTYPE_EXAMINATION;
	}
	
	//LogicCrm -> getExaminationResult
	public CrmInfo(String questionName, String minValue, String maxValue, String unit, String result) {
		super();
		this.questionName = questionName;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.unit = unit;
		this.result = result;
	}
	
	//LogicCrm -> getTreatmentInfo ХАГАЛГАА
	public CrmInfo(String surgeryName, Date surgeryDate, String doctorName, String subOrgName) {
		super();
		this.surgeryName = surgeryName;
		this.surgeryDate = surgeryDate;
		this.doctorName = doctorName;
		this.subOrgName = subOrgName;
		this.diagnoseName = "";
		this.type = Tool.INSPECTIONTYPE_SURGERY;
	}
	
	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getSubOrgName() {
		return subOrgName;
	}

	public void setSubOrgName(String subOrgName) {
		this.subOrgName = subOrgName;
	}

	public String getDiagnoseName() {
		return diagnoseName;
	}

	public void setDiagnoseName(String diagnoseName) {
		this.diagnoseName = diagnoseName;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}

	public String getExaminationName() {
		return examinationName;
	}

	public void setExaminationName(String examinationName) {
		this.examinationName = examinationName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getExamPkId() {
		return examPkId;
	}

	public void setExamPkId(BigDecimal examPkId) {
		this.examPkId = examPkId;
	}

	public String getStatusMn() {
		statusMn = "";
		if(Tool.EXAMINATIONREQUESTTYPE_COMPLETED.equals(status))
			statusMn = "Шинжилгээний хариу гарсан";
		else if(Tool.EXAMINATIONREQUESTTYPE_ACTIVE.equals(status))
			statusMn = "Сорьц авсан";
		else if(Tool.EXAMINATIONREQUESTTYPE_REQUEST.equals(status))
			statusMn = "Шинжилгээ захиалсан";
		else if(Tool.EXAMINATIONREQUESTTYPE_TEMPSAVE.equals(status))
			statusMn = "Түр хадгалсан";
		return statusMn;
	}

	public void setStatusMn(String statusMn) {
		this.statusMn = statusMn;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getQuestionName() {
		return questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	public String getSurgeryName() {
		return surgeryName;
	}

	public void setSurgeryName(String surgeryName) {
		this.surgeryName = surgeryName;
	}

	public Date getSurgeryDate() {
		return surgeryDate;
	}

	public void setSurgeryDate(Date surgeryDate) {
		this.surgeryDate = surgeryDate;
	}
}