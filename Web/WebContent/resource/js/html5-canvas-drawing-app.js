var  outlineImage =  new Image();
var  earNoseImage =  new Image();
function prepareCanvas()
{
	var canvasDiv = document.getElementById('canvasDiv');
	canvas = document.createElement('canvas');
	canvas.setAttribute('width', 800);
	canvas.setAttribute('height', 300);
	canvas.setAttribute('id', 'canvas');
	canvas.setAttribute('style', "border: 1px solid black");
	canvasDiv.appendChild(canvas);
	if(typeof G_vmlCanvasManager != 'undefined') {
		canvas = G_vmlCanvasManager.initElement(canvas);
	}
	context = canvas.getContext("2d");
	outlineImage.src = "../../resource/images/surgeryEdit.png";  
	earNoseImage.src = "../../resource/images/earNose.png";  
	$('#canvas').mousedown(function(e){
		
	  var mouseX = e.pageX - 185- 114;
	  var mouseY = e.pageY - 355 + 100;	
	  paint = true;
	  console.log("off " +this.offsetLeft + "top " + this.offsetTop);
	  addClick(mouseX,mouseY);
	  redraw();
	});
	$('#canvas').mousemove(function(e){
	  if(paint){
		addClick(e.pageX - 185 - 114, e.pageY - 355+ 100, true);
		redraw();
	  }
	});
	$('#canvas').mouseup(function(e){
	  paint = false;
	});
	$('#canvas').mouseleave(function(e){
	  paint = false;
	});
}
var clickX = new Array();
var clickY = new Array();
var clickDrag = new Array();
var paint;

function addClick(x, y, dragging)
{
  clickX.push(x);
  clickY.push(y);
  clickDrag.push(dragging);
}

function redraw(){
  context.clearRect(0, 0, context.canvas.width, context.canvas.height);
  context.strokeStyle = "#df4b26";
  context.lineJoin = "round";
  context.lineWidth = 5;
  
  for(var i=0; i < clickX.length; i++) {
    context.beginPath();
    if(clickDrag[i] && i){
      context.moveTo(clickX[i-1], clickY[i-1]);
     }else{
       context.moveTo(clickX[i]-1, clickY[i]);
     }
     context.lineTo(clickX[i], clickY[i]);
     context.closePath();
     context.stroke();
  }
  context.drawImage(outlineImage, 0,0);
  context.drawImage(earNoseImage, 200,0);
  
  var  dat  =  context.getImageData(50,50,50,50);
  var  url  =  dat.data;
}