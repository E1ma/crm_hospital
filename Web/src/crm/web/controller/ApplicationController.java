package crm.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.TimerService;
import javax.faces.application.ProjectStage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import base.*;
import crm.businesslogic.interfaces.IInfoLogicLocal;
import crm.entity.*;

@ApplicationScoped
@ManagedBean(name = "applicationController")
public class ApplicationController implements Serializable {

	@EJB(beanName = "InfoLogic")
	IInfoLogicLocal infoLogic;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String cssPath = "/resource/css/source/";
	private String jsPath = "/resource/js/source/";
	private String path = "/resource/js/source/";
	private String excelPath = "/resource/";
	private String imagePath = "C:/MedITm/Rentgen/";
	private String inspectionImagePath = "C:/MedITm/";
	private String customerImagePath = "C:/MedITm/Customer/";
	private String employeeImagePath = "C:/MedITm/Employee/";
	private String endoPath = "D:/IBI/jboss1/welcome-content/";
	private boolean listCash;
	private List<String> diagnoseTypeList;
	private LazyDataModel<CrmCustomer> lazyCrmCustomerList;
	private LazyDataModel<Customer> lazyCustomerList;
	private LazyDataModel<Customer> lazyCustomerList1;
	private LazyDataModel<Customer> lazyCustomerList2;

	private boolean diagnoseRefresh = false;
	private boolean subOrgaRefresh = false;

	private volatile int count;

	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public ApplicationController() {

	}

	public void test() {
		System.out.println("OUT : ");
	}

	public void test(String text) {
		System.out.println("OUT : " + text);
	}

	@PostConstruct
	public void postConstruct() {
		System.out.println("ApplicationController : initData");
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
		if (fc.isProjectStage(ProjectStage.Production)) {
			setCssPath(request.getContextPath() + "/resource/css/source/");
			setJsPath(request.getContextPath() + "/resource/js/source/");
			setPath(request.getContextPath() + "/resource/");
		} else {
			setCssPath(request.getContextPath() + "/resource/css/source/");
			setJsPath(request.getContextPath() + "/resource/js/source/");
			setPath(request.getContextPath() + "/resource/");
		}
		// sendData();
		System.out.println("ApplicationController : initData");
		System.out.println("INIT-CALLED");
        //ScheduleExpression exp = new ScheduleExpression();
        //exp.hour("*").minute("*").second("*/4");
        //timerService.createCalendarTimer(exp);
	}
	
	//@Timeout
    //public void timeOut() {
      //System.out.println("Method Invoked");
    //}

	public String url(String url) {
		return url;
	}

	public String getCssPath() {
		return cssPath;
	}

	public void setCssPath(String cssPath) {
		this.cssPath = cssPath;
	}

	public String getJsPath() {
		return jsPath;
	}

	public void setJsPath(String jsPath) {
		this.jsPath = jsPath;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public boolean isListCash() {
		if (listCash) {
			listCash = false;
			return true;
		}
		return listCash;
	}

	public void setListCash(boolean listCash) {
		this.listCash = listCash;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public List<String> getDiagnoseTypeList() {
		if (diagnoseTypeList == null) {
			try {
				diagnoseTypeList = new ArrayList<String>();
				diagnoseTypeList.add("A");
				diagnoseTypeList.add("B");
				diagnoseTypeList.add("C");
				diagnoseTypeList.add("D");
				diagnoseTypeList.add("E");
				diagnoseTypeList.add("F");
				diagnoseTypeList.add("G");
				diagnoseTypeList.add("H");
				diagnoseTypeList.add("I");
				diagnoseTypeList.add("J");
				diagnoseTypeList.add("K");
				diagnoseTypeList.add("L");
				diagnoseTypeList.add("M");
				diagnoseTypeList.add("N");
				diagnoseTypeList.add("O");
				diagnoseTypeList.add("P");
				diagnoseTypeList.add("Q");
				diagnoseTypeList.add("R");
				diagnoseTypeList.add("S");
				diagnoseTypeList.add("T");
				diagnoseTypeList.add("U");
				diagnoseTypeList.add("V");
				diagnoseTypeList.add("W");
				diagnoseTypeList.add("X");
				diagnoseTypeList.add("Y");
				diagnoseTypeList.add("Z");
				// diagnoseTypeList = infoLogic.getDiagnoseTypeList();
			} catch (Exception ex) {

			}
		}
		return diagnoseTypeList;
	}

	public void setDiagnoseTypeList(List<String> diagnoseTypeList) {
		this.diagnoseTypeList = diagnoseTypeList;
	}

	public void newDecimal10() {
		System.out.println(Tools.newDecimal10());
	}

	public boolean isDiagnoseRefresh() {
		return diagnoseRefresh;
	}

	public void setDiagnoseRefresh(boolean diagnoseRefresh) {
		this.diagnoseRefresh = diagnoseRefresh;
	}

	public String getInspectionImagePath() {
		return inspectionImagePath;
	}

	public void setInspectionImagePath(String inspectionImagePath) {
		this.inspectionImagePath = inspectionImagePath;
	}

	public boolean isSubOrgaRefresh() {
		return subOrgaRefresh;
	}

	public void setSubOrgaRefresh(boolean subOrgaRefresh) {
		this.subOrgaRefresh = subOrgaRefresh;
	}
	
	public void setLazyCrmCustomerList(LazyDataModel<CrmCustomer> lazyCrmCustomerList) {
		this.lazyCrmCustomerList = lazyCrmCustomerList;
	}

	public LazyDataModel<Customer> getLazyCustomerList() {
		if (lazyCustomerList == null) {
			lazyCustomerList = new LazyDataModel<Customer>() {

				public List<Customer> load(int first, int pageSize, String sortField, SortOrder sortOrder,
						Map<String, String> filters) {
					List<Customer> data = new ArrayList<Customer>();
					String sortType = "";
					if (sortOrder.equals(SortOrder.ASCENDING)) {
						sortType = "ASC";
					} else if (sortOrder.equals(SortOrder.DESCENDING)) {
						sortType = "DESC";
					} else if (sortOrder.equals(SortOrder.UNSORTED)) {
						sortType = "";
					}
					List<Customer> result = new ArrayList<Customer>();
					try {
						Map<String, String> map = new HashMap<>();
						for (Map.Entry<String, String> entry : filters.entrySet()) {
							map.put(entry.getKey(), entry.getValue().toString());
						}
						result = infoLogic.getCustomers(first, pageSize, sortField, sortType, map);
						lazyCustomerList.setRowCount((int) infoLogic.getCustomerCount(map));
					} catch (Exception e) {
						e.printStackTrace();
					}
					return result;
				}
			};
		}
		return lazyCustomerList;
	}

	public void setLazyCustomerList(LazyDataModel<Customer> lazyCustomerList) {
		this.lazyCustomerList = lazyCustomerList;
	}

	public LazyDataModel<Customer> getLazyCustomerList1() {
		if (lazyCustomerList1 == null) {
			lazyCustomerList1 = new LazyDataModel<Customer>() {

				public List<Customer> load(int first, int pageSize, String sortField, SortOrder sortOrder,
						Map<String, String> filters) {
					List<Customer> data = new ArrayList<Customer>();
					String sortType = "";
					if (sortOrder.equals(SortOrder.ASCENDING)) {
						sortType = "ASC";
					} else if (sortOrder.equals(SortOrder.DESCENDING)) {
						sortType = "DESC";
					} else if (sortOrder.equals(SortOrder.UNSORTED)) {
						sortType = "";
					}
					List<Customer> result = new ArrayList<Customer>();
					try {
						Map<String, String> map = new HashMap<>();
						for (Map.Entry<String, String> entry : filters.entrySet()) {
							map.put(entry.getKey(), entry.getValue().toString());
						}
						result = infoLogic.getCustomers(first, pageSize, sortField, sortType, map);
						lazyCustomerList1.setRowCount((int) infoLogic.getCustomerCount(map));
					} catch (Exception e) {
						e.printStackTrace();
					}
					return result;
				}
			};
		}
		return lazyCustomerList1;
	}

	public void setLazyCustomerList1(LazyDataModel<Customer> lazyCustomerList1) {
		this.lazyCustomerList1 = lazyCustomerList1;
	}
	
	public LazyDataModel<Customer> getLazyCustomerList2() {
		if (lazyCustomerList2 == null) {
			lazyCustomerList2 = new LazyDataModel<Customer>() {

				public List<Customer> load(int first, int pageSize, String sortField, SortOrder sortOrder,
						Map<String, String> filters) {
					List<Customer> data = new ArrayList<Customer>();
					String sortType = "";
					if (sortOrder.equals(SortOrder.ASCENDING)) {
						sortType = "ASC";
					} else if (sortOrder.equals(SortOrder.DESCENDING)) {
						sortType = "DESC";
					} else if (sortOrder.equals(SortOrder.UNSORTED)) {
						sortType = "";
					}
					List<Customer> result = new ArrayList<Customer>();
					try {
						Map<String, String> map = new HashMap<>();
						for (Map.Entry<String, String> entry : filters.entrySet()) {
							map.put(entry.getKey(), entry.getValue().toString());
						}
						result = infoLogic.getCustomers(first, pageSize, sortField, sortType, map);
						lazyCustomerList2.setRowCount((int) infoLogic.getCustomerCount(map));
					} catch (Exception e) {
						e.printStackTrace();
					}
					return result;
				}
			};
		}
		return lazyCustomerList2;
	}

	public void setLazyCustomerList2(LazyDataModel<Customer> lazyCustomerList2) {
		this.lazyCustomerList2 = lazyCustomerList2;
	}

	public LazyDataModel<CrmCustomer> getLazyCrmCustomerList() {
		if (lazyCrmCustomerList == null) {
			lazyCrmCustomerList = new LazyDataModel<CrmCustomer>() {

				public List<CrmCustomer> load(int first, int pageSize, String sortField, SortOrder sortOrder,
						Map<String, String> filters) {
					List<Customer> data = new ArrayList<Customer>();
					String sortType = "";
					if (sortOrder.equals(SortOrder.ASCENDING)) {
						sortType = "ASC";
					} else if (sortOrder.equals(SortOrder.DESCENDING)) {
						sortType = "DESC";
					} else if (sortOrder.equals(SortOrder.UNSORTED)) {
						sortType = "";
					}
					List<CrmCustomer> result = new ArrayList<CrmCustomer>();
					try {
						Map<String, String> map = new HashMap<>();
						for (Map.Entry<String, String> entry : filters.entrySet()) {
							map.put(entry.getKey(), entry.getValue().toString());
						}
						result = infoLogic.getCrmCustomers(first, pageSize, sortField, sortType, map);
						lazyCrmCustomerList.setRowCount((int) infoLogic.getCrmCustomerCount(map));
					} catch (Exception e) {
						e.printStackTrace();
					}
					return result;
				}
			};
		}
		return lazyCrmCustomerList;
	}
	
	public String getEndoPath() {
		return endoPath;
	}

	public void setEndoPath(String endoPath) {
		this.endoPath = endoPath;
	}

	public String getCustomerImagePath() {
		return customerImagePath;
	}

	public void setCustomerImagePath(String customerImagePath) {
		this.customerImagePath = customerImagePath;
	}

	public String getEmployeeImagePath() {
		return employeeImagePath;
	}

	public void setEmployeeImagePath(String employeeImagePath) {
		this.employeeImagePath = employeeImagePath;
	}

	public String getExcelPath() {
		return excelPath;
	}

	public void setExcelPath(String excelPath) {
		this.excelPath = excelPath;
	}	
}