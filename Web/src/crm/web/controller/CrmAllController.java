package crm.web.controller;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import base.*;
import crm.businessentity.CrmInfo;
import crm.businessentity.Tool;
import crm.businesslogic.interfaces.IInfoLogicLocal;
import crm.businesslogic.interfaces.ILogicAllCrmLocal;
import crm.entity.*;

@SessionScoped
@ManagedBean(name = "crmAllController")
public class CrmAllController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String ACCOUNT_SID = "ACb9292959d8d1da16eb1042e33bb983af";
    public static final String AUTH_TOKEN = "61dfcf4e3ff3b69313c67584930b5716";

    
    
	@EJB(beanName = "InfoLogic")
	IInfoLogicLocal infoLogic;

	@EJB(beanName = "LogicAllCrm")
	ILogicAllCrmLocal logicAllCrm;

	@ManagedProperty(value = "#{userController}")
	private UserSessionController userSessionController;

	@ManagedProperty(value = "#{applicationController}")
	private ApplicationController applicationController;
	
	private Date currentDate = new Date();
	private Users currentUsers;
	private Employee currentEmployee;
	private Employee currentUpdatedEmployee;
	private CrmAutoSendConfig currentSendConfig;
	private SystemConfig currSysConfig;
	
	private List<CrmTreatmentCondition> listCrmTreatmentCondition;
	private List<Employee> listOfEmployeeControl;
	private List<SubOrganization> crmSubOrganizationList;
	private List<CrmAutoSendConfig> crmAutoSendConfig;
	private List<SystemConfig> sysConfig;
	
	/**
	 * CRM ХАРИУЦСАН ХЭЛТЭС
	 * */
	public void getEmployeeInfoList()
	{
		try {
			listOfEmployeeControl = logicAllCrm.getListOfEmployees();
		} catch (Exception e) {
			userSessionController.showErrorMessage(e.toString());
		}
	}
	
	public void saveEmployee(){
		try{
			
			if(currentEmployee.getFirstName() == null || currentEmployee.getFirstName().equals(""))
			{
				userSessionController.showWarningMessage("Нэр заавал оруулна уу");
				return;
			}
			if(currentEmployee.getLastName() == null || currentEmployee.getLastName().equals(""))
			{
				userSessionController.showWarningMessage("Овог заавал оруулна уу");
				return;
			}
			if(currentEmployee.getRegNumber() == null || currentEmployee.getRegNumber().equals(""))
			{
				userSessionController.showWarningMessage("Регистерийн дугаар заавал оруулна уу");
				return;
			}
			if(currentEmployee.getId() == null || currentEmployee.getId().equals(""))
			{
				userSessionController.showWarningMessage("Ажилтны № заавал оруулна уу");
				return;
			}
			if(currentUsers.getPassword() == null || currentUsers.getPassword().equals(""))
			{
				userSessionController.showWarningMessage("Нууч үгээ оруулна уу");
				return;
			}
			if(currentEmployee.getSubOrganizationPkId() == null || currentEmployee.getSubOrganizationPkId().equals(""))
			{
				userSessionController.showWarningMessage("Тасагаа оруулна уу");
				return;
			}
	
			currentEmployee.setStatus(Tool.ADDED);
			currentEmployee.setIsCrm("Y");
			currentEmployee.setDegreePkId(new BigDecimal(1));
			
			currentUsers.setStatus(Tool.ADDED);
			currentUsers.setId(currentEmployee.getId());
			currentUsers.setOrganizationPkId(userSessionController.getLoggedInfo().getOrganization().getPkId());
			currentUsers.setName(currentEmployee.getFirstName());
			
			logicAllCrm.saveEmployeeInfo(currentEmployee, currentUsers, userSessionController.getLoggedInfo());
			userSessionController.showSuccessMessage("Амжилттай хадгаллаа");
			RequestContext context = RequestContext.getCurrentInstance();
			getEmployeeInfoList();
			currentEmployee = new Employee();
			context.update(":form:myTable");
		}
		catch(Exception e)
		{
			userSessionController.showErrorMessage(e.toString());
		}	
	}
	
	public void updateCurrentEmployee(Employee employee)
	{
		currentUpdatedEmployee = employee;
		currentUpdatedEmployee.setStatus(Tool.MODIFIED);
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('updateEmployee').show();");
		context.update("form:updateEmployee");
	}
	
	public void deleteEmployee(Employee employee){
		currentEmployee = employee;
		currentEmployee.setStatus(Tool.DELETE);
	}
	
	public void deleteEmployee(){
		
		RequestContext context = RequestContext.getCurrentInstance();
		
		try {
			logicAllCrm.saveEmployeeInfo(currentEmployee, currentUsers, userSessionController.getLoggedInfo());
			userSessionController.showSuccessMessage("Амжилттай устгалаа");
			getEmployeeInfoList();
			context.update(":form:myTable");
		} catch (Exception e) {
			e.printStackTrace();
			userSessionController.showErrorMessage(e.toString());
		}
	}
	public void saveUpdatedEmployee(){
		try{

			if(currentUpdatedEmployee.getFirstName() == null || currentUpdatedEmployee.getFirstName().equals(""))
			{
				userSessionController.showWarningMessage("Нэр заавал оруулна уу");
				return;
			}
			if(currentUpdatedEmployee.getLastName() == null || currentUpdatedEmployee.getLastName().equals(""))
			{
				userSessionController.showWarningMessage("Овог заавал оруулна уу");
				return;
			}
			if(currentUpdatedEmployee.getRegNumber() == null || currentUpdatedEmployee.getRegNumber().equals(""))
			{
				userSessionController.showWarningMessage("Регистерийн дугаар заавал оруулна уу");
				return;
			}
			if(currentUpdatedEmployee.getId() == null || currentUpdatedEmployee.getId().equals(""))
			{
				userSessionController.showWarningMessage("Ажилтны № заавал оруулна уу");
				return;
			}
			if(currentUpdatedEmployee.getSubOrganizationName() == null || currentUpdatedEmployee.getSubOrganizationName().equals(""))
			{
				userSessionController.showWarningMessage("Тасагаа оруулна уу");
				return;
			}
			
			currentUpdatedEmployee.setIsCrm("Y");
			currentUpdatedEmployee.setDegreePkId(new BigDecimal(1));
			
			currentUsers.setId(currentUpdatedEmployee.getId());
			currentUsers.setName(currentUpdatedEmployee.getFirstName());
			
			logicAllCrm.saveEmployeeInfo(currentUpdatedEmployee, currentUsers, userSessionController.getLoggedInfo());
			userSessionController.showSuccessMessage("Амжилттай хадгаллаа");
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('updateEmployee').hide();");
			getEmployeeInfoList();
			context.update(":form:myTable");
		}
		catch(Exception e)
		{
			userSessionController.showErrorMessage(e.toString());
		}
	}
	
	public void checkRegNumber() {
		try {
			if (logicAllCrm.checkRegNumber(getCurrentEmployee().getRegNumber())) {
					userSessionController.showErrorMessage("Регистерээ зөв оруулна уу!!");
			}
		} catch (Exception ex) {
			userSessionController.showErrorMessage(ex.getMessage());
		}
	}
	
	/**
	 * CRM АВТОМАТААР ИЛГЭЭХ ХЭСЭГ
	 * */
	public void getSendingConfigList()
	{
		try {
			crmAutoSendConfig = logicAllCrm.getListOfCrmAutoSendConfig();
		} catch (Exception e) {
			userSessionController.showErrorMessage(e.toString());
		}
	}
	/**
	 * CRM АВТОМАТААР ИЛГЭЭХ ТОХИРУУЛГА ХАДГАЛАХ
	 * */
	public void saveSendingConfig(){
		
		getSysConfig();
		
		try {
			currentSendConfig.setStatus(Tool.ADDED);
			currentSendConfig.setId(sysConfig.get(0).getValue());
			currSysConfig = sysConfig.get(0);
			
			logicAllCrm.saveSendingConfig(currentSendConfig, currSysConfig, userSessionController.getLoggedInfo());
			userSessionController.showSuccessMessage("Амжилттай хадгаллаа");
			RequestContext context = RequestContext.getCurrentInstance();
			getSendingConfigList();
			currentSendConfig = new CrmAutoSendConfig();
			context.update(":form:tab:configTable");
		} catch (Exception e) {
			userSessionController.showErrorMessage(e.toString());
		}
	}
	
	
	public UserSessionController getUserSessionController() {
		return userSessionController;
	}

	public void setUserSessionController(UserSessionController userSessionController) {
		this.userSessionController = userSessionController;
	}

	public ApplicationController getApplicationController() {
		return applicationController;
	}

	public void setApplicationController(ApplicationController applicationController) {
		this.applicationController = applicationController;
	}

	public Users getCurrentUsers() {
		if(currentUsers == null)
			currentUsers = new Users();
		return currentUsers;
	}

	public void setCurrentUsers(Users currentUsers) {
		this.currentUsers = currentUsers;
	}

	public Employee getCurrentEmployee() {
		if(currentEmployee == null)
			currentEmployee = new Employee();
		return currentEmployee;
	}

	public void setCurrentEmployee(Employee currentEmployee) {
		this.currentEmployee = currentEmployee;
	}

	public Employee getCurrentUpdatedEmployee() {
		if(currentUpdatedEmployee == null)
			currentUpdatedEmployee = new Employee();
		return currentUpdatedEmployee;
	}

	public void setCurrentUpdatedEmployee(Employee currentUpdatedEmployee) {
		this.currentUpdatedEmployee = currentUpdatedEmployee;
	}

	public List<Employee> getListOfEmployeeControl() {
		return listOfEmployeeControl;
	}

	public void setListOfEmployeeControl(List<Employee> listOfEmployeeControl) {
		this.listOfEmployeeControl = listOfEmployeeControl;
	}
	
	public List<SubOrganization> getCrmSubOrganizationList() {
		
		try {
			crmSubOrganizationList = logicAllCrm.getListSubOrganization();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return crmSubOrganizationList;
	}

	public void setCrmSubOrganizationList(
			List<SubOrganization> crmSubOrganizationList) {
		this.crmSubOrganizationList = crmSubOrganizationList;
	}

	public List<CrmAutoSendConfig> getCrmAutoSendConfig() {
		try {
			crmAutoSendConfig = logicAllCrm.getListOfCrmAutoSendConfig();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return crmAutoSendConfig;
	}

	public void setCrmAutoSendConfig(List<CrmAutoSendConfig> crmAutoSendConfig) {
		this.crmAutoSendConfig = crmAutoSendConfig;
	}

	public CrmAutoSendConfig getCurrentSendConfig() {
		if(currentSendConfig == null)
			currentSendConfig = new CrmAutoSendConfig();
		return currentSendConfig;
	}

	public void setCurrentSendConfig(CrmAutoSendConfig currentSendConfig) {
		this.currentSendConfig = currentSendConfig;
	}

	public List<CrmTreatmentCondition> getListCrmTreatmentCondition() {
		try {
			listCrmTreatmentCondition = logicAllCrm.getListCrmTreatmentCondition();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listCrmTreatmentCondition;
	}

	public void setListCrmTreatmentCondition(
			List<CrmTreatmentCondition> listCrmTreatmentCondition) {
		this.listCrmTreatmentCondition = listCrmTreatmentCondition;
	}

	public List<SystemConfig> getSysConfig() {
		try {
			sysConfig = logicAllCrm.getSystemConfig();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sysConfig;
	}

	public void setSysConfig(List<SystemConfig> sysConfig) {
		this.sysConfig = sysConfig;
	}

	public SystemConfig getCurrSysConfig() {
		if(currSysConfig == null)
			currSysConfig = new SystemConfig();
		return currSysConfig;
	}

	public void setCurrSysConfig(SystemConfig currSysConfig) {
		this.currSysConfig = currSysConfig;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	
}