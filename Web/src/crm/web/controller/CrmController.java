package crm.web.controller;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import base.*;
import crm.businessentity.CrmInfo;
import crm.businessentity.Tool;
import crm.businesslogic.interfaces.IInfoLogicLocal;
import crm.businesslogic.interfaces.ILogicCrmLocal;
import crm.entity.*;

@SessionScoped
@ManagedBean(name = "crmController")
public class CrmController implements  Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String ACCOUNT_SID = "ACb9292959d8d1da16eb1042e33bb983af";
    public static final String AUTH_TOKEN = "61dfcf4e3ff3b69313c67584930b5716";

	@EJB(beanName = "InfoLogic")
	IInfoLogicLocal infoLogic;

	@EJB(beanName = "LogicCrm")
	ILogicCrmLocal logicCrm;

	@ManagedProperty(value = "#{userController}")
	private UserSessionController userSessionController;

	@ManagedProperty(value = "#{applicationController}")
	private ApplicationController applicationController;

	private CrmCustomer currentCustomer;
	private CrmCustomer crmPatientInfo;
	private Customer selectedCustomer;
	private CrmTreatmentCondition currentTreat;
	private CrmTreatmentConditionResult currentResult;
	
	private List<CrmCustomerFamily> listFamily;
	private List<CrmCustomerInfo> listInfo;
	private List<CrmCustomerContactHistory> listContact;
	private List<SubOrganization> listSubOrganization;
	private List<CrmInfo> listInpatient;
	private List<CrmInfo> listExam;
	private List<CrmInfo> listExamResult;
	private List<CrmInfo> listSurgery;
	private List<CrmCustomer> listPatient;
	private StreamedContent file;
	private List<CrmCustomer> listCrmCustomer;
	private List<CrmChannelInfo> listSmsInfo;
	private List<CrmChannelInfo> listMailInfo;
	private List<CrmChannelInfo> listSelectInfo;
	private CrmChannelInfo currentSms;
	private CrmChannelInfo currentMail;
	private CrmChannelInfo selectedMail;
	private List<CrmCustomer> listCustomer;
	private List<CrmCustomer> selectedMailCustomer;
	private List<Customer> listCusMailSms;
	
	private List<CrmTreatmentCondition> listTreatment;
	private List<CrmTreatmentConditionDtl> listTreatDtl;
	private List<CrmTreatmentConditionDtl> listExamDtl;
	private List<CrmTreatmentConditionDtl> listDiagnoseDtl;
	private List<CrmTreatmentConditionDtl> listAgeDtl;
	private List<CrmTreatmentConditionDtl> listAddressDtl;
	
	private List<CrmTreatmentConditionResult> listResult;
	private List<CrmTreatmentConditionResultDtl> listResultDtl;

	private List<Aimag> listAimag;
	private List<Soum> listSoum;
	private List<Diagnose> listDiagnose;
	private List<Diagnose> selectedDiagnoses;
	private List<Examination> listExamination;
	private List<Examination> selectedExaminations;
	private List<Employee> listEmployee;
	private List<Employee> selectedEmployees;
	private Employee currentEmployee;
	private List<SubOrganization> listSubOrg;
	
	//PatientSearch
	private String cardNumber;
	private String customerName;
	private String phoneNumber;
	private String customerAddress;
	private String lifeCategory;
	private String totalCount;
	//infoChannelSms
	private String titleSearch;
	private String valueSearch;
	
	private String titleSend;
	private String noteSend;
	private String selectedConditionType;

	private Date beginDate;
	private Date endDate;
	private Date searchDate;
	private int minAge;
	private int maxAge;
	private int beginAge;
	private int endAge;
	private boolean notInfo;
	private boolean infoSms;
	private boolean infoMail;
	
	private BigDecimal aimagPkId;
	private BigDecimal soumPkId;
	private BigDecimal subOrgPkId;
	private BigDecimal searchEmployeePkId;
	private String searchCustomerName;
	
	public void setSubOrgaList() throws Exception
	{
		listSubOrganization = infoLogic.getListSubOrganization();
	}
	
	public StreamedContent getFile() {
		InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resource/CRMExcelTemplate.xls");
        file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "CRMExcelTemplate.xls");
        return file;
    }
		
	public void setFile(StreamedContent file) {
		this.file = file;
	}
	
	public void autoSendMailOrSms()
	{
		try {
			logicCrm.autoSendMailOrSms();
		} catch (Exception e) {
			userSessionController.showErrorMessage(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * ЭМЧИЛГЭЭНИЙ ҮЙЛЧЛҮҮЛЭГЧ ХАЙХ
	 * */
	public void searchResult()
	{
		try
		{
			listResult = logicCrm.getListResult(searchDate, selectedConditionType);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void getSearchData()
	{
		try {
			if(listEmployee == null)
				listEmployee = infoLogic.getEmployeeInfo();
			if(listSubOrg == null)
				listSubOrg = infoLogic.getAll(SubOrganization.class);
			} catch (Exception e) {
				e.printStackTrace();
		}
	}
	
	public void getResultDtl()
	{
		try {
			if(currentResult == null) return;
			listResultDtl = logicCrm.getResultDtlList(currentResult.getPkId(), subOrgPkId, searchEmployeePkId, searchCustomerName);
		} catch (Exception e) {
			userSessionController.showErrorMessage(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * ЭМЧИЛГЭЭНИЙ ҮЙЛЧЛҮҮЛЭГЧ НЭМЭХ
	 * */
	public String saveConditionTreatment()
	{
		try
		{
			if(currentTreat.getName() == null || currentTreat.getName().equals(""))
			{
				userSessionController.showWarningMessage("Нэр заавал оруулна уу");
				return "";
			}
			if(currentTreat.getDescription() == null || currentTreat.getDescription().equals(""))
			{
				userSessionController.showWarningMessage("Хураангуй мэдээлэл заавал оруулна уу");
				return "";
			}
			List<CrmTreatmentConditionDtl> listDtl = new ArrayList<>();
			getListTreatDtl().addAll(getListAddressDtl());
			getListTreatDtl().addAll(getListAgeDtl());
			getListTreatDtl().addAll(getListDiagnoseDtl());
			getListTreatDtl().addAll(getListExamDtl());
			for (CrmTreatmentConditionDtl tmp : getListTreatDtl()) {
				if(tmp.getType() != null && !tmp.getType().equals(""))
					listDtl.add((CrmTreatmentConditionDtl)tmp.clone());
			}
			for (CrmChannelInfo tmp : listSelectInfo) {
				if(tmp.isSelected())
				{
					CrmTreatmentConditionDtl newDtl = new CrmTreatmentConditionDtl();
					newDtl.setType(Tool.CRMCHANNEL);
					newDtl.setTypePkId(tmp.getPkId());
					listDtl.add(newDtl);
				}
			}
			logicCrm.saveConditionTreatment(currentTreat, listDtl);
			userSessionController.showSuccessMessage("Амжилттай хадгаллаа");
			return "treatmentList";
		}
		catch(Exception ex)
		{
			userSessionController.showErrorMessage(ex.getMessage());
			return "";
		}
	}
	
	public void getCrmTreatmentList()
	{
		try {
			listTreatment = logicCrm.getCrmTreatmentList();
		} catch (Exception e) {
			userSessionController.showErrorMessage(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public String newTreatmentCondition() throws CloneNotSupportedException
	{
		try {
			currentTreat = new CrmTreatmentCondition();
			currentTreat.setStatus(Tool.ADDED);
			currentTreat.setIsActive(1);
			currentTreat.setCreatedUser(userSessionController.getLoggedInfo().getUser().getName());
			currentTreat.setCreatedBy(userSessionController.getLoggedInfo().getUser().getPkId());
			
			getListTreatDtl().clear();
			getListExamDtl().clear();
			getListDiagnoseDtl().clear();
			getListAgeDtl().clear();
			getListAddressDtl().clear();
			
			if(listSelectInfo == null)
				listSelectInfo = logicCrm.getAll(CrmChannelInfo.class);
			else
				for (CrmChannelInfo tmp : listSelectInfo) {
					tmp.setSelected(false);
				}
			
			for(int i = 0; i < 3; i++)
			{
				CrmTreatmentConditionDtl dtl = new CrmTreatmentConditionDtl();
				dtl.setTypeName(" ");
				getListTreatDtl().add((CrmTreatmentConditionDtl)dtl.clone());
				getListExamDtl().add((CrmTreatmentConditionDtl)dtl.clone());
				getListDiagnoseDtl().add((CrmTreatmentConditionDtl)dtl.clone());
				getListAgeDtl().add((CrmTreatmentConditionDtl)dtl.clone());
				getListAddressDtl().add((CrmTreatmentConditionDtl)dtl.clone());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "medCusRegister";
	}
	
	public String modifyTreatmentCondition(CrmTreatmentCondition condition) throws CloneNotSupportedException
	{
		try {
			currentTreat = condition;
			currentTreat.setStatus(Tool.MODIFIED);
			currentTreat.setCreatedUser(userSessionController.getLoggedInfo().getUser().getName());
			currentTreat.setUpdatedBy(userSessionController.getLoggedInfo().getUser().getPkId());
			getListTreatDtl().clear();
			getListExamDtl().clear();
			getListDiagnoseDtl().clear();
			getListAgeDtl().clear();
			getListAddressDtl().clear();
			if(listEmployee == null)
				listEmployee = infoLogic.getEmployeeInfo();
			if(listAimag == null)
				listAimag = infoLogic.getAll(Aimag.class);
			if(listSoum == null)
				listSoum = infoLogic.getAll(Soum.class);
			if(listDiagnose == null)
				listDiagnose = infoLogic.getAll(Diagnose.class);
			if(listExamination == null)
				listExamination = infoLogic.getAll(Examination.class);
			if(listSelectInfo == null)
				listSelectInfo = logicCrm.getAll(CrmChannelInfo.class);
			for (CrmChannelInfo channel : listSelectInfo)
				channel.setSelected(false);
			List<CrmTreatmentConditionDtl> listDtl = logicCrm.getByAnyField(CrmTreatmentConditionDtl.class, "hdrPkId", condition.getPkId());
			for (CrmTreatmentConditionDtl tmp : listDtl) {
				if(Tool.CRMADDRESS.equals(tmp.getType()))
				{
					tmp.setAimagName(getAimagName(tmp.getAimagPkId()));
					tmp.setSoumName(getSoumName(tmp.getSoumPkId()));
					getListAddressDtl().add((CrmTreatmentConditionDtl)tmp.clone());
				}
				else if(Tool.CRMAGE.equals(tmp.getType()))
					getListAgeDtl().add((CrmTreatmentConditionDtl)tmp.clone());
				else if(Tool.CRMDIAGNOSE.equals(tmp.getType()))
				{
					Diagnose diag = getDiagnoseInfo(tmp.getTypePkId());
					tmp.setDiagnoseId(diag.getId());
					tmp.setTypeName(diag.getNameMn());
					getListDiagnoseDtl().add((CrmTreatmentConditionDtl)tmp.clone());
				}
				else if(Tool.CRMEMPLOYEE.equals(tmp.getType()))
				{
					Employee emp = getEmpInfo(tmp.getTypePkId());
					tmp.setDepName(emp.getSubOrganizationName());
					tmp.setTypeName(emp.getFirstName());
					getListTreatDtl().add((CrmTreatmentConditionDtl)tmp.clone());
				}
				else if(Tool.CRMEXAM.equals(tmp.getType()))
				{
					Examination ex = getExamInfo(tmp.getTypePkId());
					tmp.setExamId(ex.getId());
					tmp.setTypeName(ex.getName());
					getListExamDtl().add((CrmTreatmentConditionDtl)tmp.clone());
				}
				else if(Tool.CRMCHANNEL.equals(tmp.getType()))
				{
					for (CrmChannelInfo channel : listSelectInfo)
						if(channel.getPkId().compareTo(tmp.getTypePkId()) == 0)
							channel.setSelected(true);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "medCusRegister";
	}
	
	public void deleteTreatRow(CrmTreatmentCondition condition)
	{
		try {
			condition.setStatus(Tool.DELETE);
			logicCrm.saveConditionTreatment(condition, null);
			getCrmTreatmentList();
			userSessionController.showSuccessMessage("Амжилттай устгалаа");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void showDialog(String type)
	{
		try {
			RequestContext context = RequestContext.getCurrentInstance();
			switch (type) {
			case "Exam":
				if(listExamination == null)
					listExamination = infoLogic.getAll(Examination.class);
				context.execute("PF('addExam').show()");
				break;
			case "Diagnose":
				if(listDiagnose == null)
					listDiagnose = infoLogic.getAll(Diagnose.class);
				context.execute("PF('addDiagnose').show()");
				break;
			case "Address":
				if(listAimag == null)
					listAimag = infoLogic.getAll(Aimag.class);
				if(listSoum == null)
					listSoum = infoLogic.getAll(Soum.class);
				context.execute("PF('addAimag').show()");
				break;
			case "Age":
				context.execute("PF('addAge').show()");
				break;
			case "Employee":
				if(listEmployee == null)
					listEmployee = infoLogic.getEmployeeInfo();
				context.execute("PF('addEmployee').show()");
				break;
			default:
				break;
			}
			
		} catch (Exception e) {
			userSessionController.showErrorMessage(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void saveDialog(String type)
	{
		RequestContext context = RequestContext.getCurrentInstance();
		int index = -1;
		switch (type) {
		case "Age":
			if(endAge == 0)
			{
				userSessionController.showWarningMessage("Насаа оруулна уу");
				return;
			}
			if(beginAge >= endAge)
			{
				userSessionController.showWarningMessage("Эхлэх нас их байж болохгүй");
				return;
			}
			for (CrmTreatmentConditionDtl tmp : getListAgeDtl()) {
				if(tmp.getType() == null || tmp.getType().equals(""))
				{
					index = getListAgeDtl().indexOf(tmp);
					break;
				}
			}
			if(index != -1)
				getListAgeDtl().remove(index);
			CrmTreatmentConditionDtl age = new CrmTreatmentConditionDtl();
			age.setType(Tool.CRMAGE);
			age.setBeginAge(beginAge);
			age.setEndAge(endAge);
			getListAgeDtl().add(index == -1 ? getListAgeDtl().size() : index, age);
			context.execute("PF('addAge').hide()");
			break;
		case "Address":
			for (CrmTreatmentConditionDtl tmp : getListAddressDtl()) {
				if(tmp.getType() == null || tmp.getType().equals(""))
				{
					index = getListAddressDtl().indexOf(tmp);
					break;
				}
			}
			if(index != -1)
				getListAddressDtl().remove(index);
			CrmTreatmentConditionDtl address = new CrmTreatmentConditionDtl();
			address.setType(Tool.CRMADDRESS);
			address.setAimagPkId(aimagPkId);
			address.setSoumPkId(soumPkId);
			address.setAimagName(getAimagName(aimagPkId));
			address.setSoumName(getSoumName(soumPkId));
			getListAddressDtl().add(index == -1 ? getListAddressDtl().size() : index, address);
			context.execute("PF('addAimag').hide()");
			break;
		case "Dignose":
			if(selectedDiagnoses == null || selectedDiagnoses.size() == 0) return;
			List<CrmTreatmentConditionDtl> deleteList = new ArrayList<>();
			for (CrmTreatmentConditionDtl tmp : getListDiagnoseDtl()) {
				if(tmp.getType() == null || tmp.getType().equals(""))
					deleteList.add(tmp);
			}
			if(deleteList.size() > 0)
				getListDiagnoseDtl().removeAll(deleteList);
			for (Diagnose tmp : selectedDiagnoses) {
				CrmTreatmentConditionDtl dtl = new CrmTreatmentConditionDtl();
				dtl.setType(Tool.CRMDIAGNOSE);
				dtl.setDiagnoseId(tmp.getId());
				dtl.setTypeName(tmp.getNameMn());
				dtl.setTypePkId(tmp.getPkId());
				getListDiagnoseDtl().add(dtl);
			}
			context.execute("PF('addDiagnose').hide()");
			break;
		case "Exam":
			if(selectedExaminations == null || selectedExaminations.size() == 0) return;
			List<CrmTreatmentConditionDtl> deleteListExam = new ArrayList<>();
			for (CrmTreatmentConditionDtl tmp : getListExamDtl()) {
				if(tmp.getType() == null || tmp.getType().equals(""))
					deleteListExam.add(tmp);
			}
			if(deleteListExam.size() > 0)
				getListExamDtl().removeAll(deleteListExam);
			for (Examination tmp : selectedExaminations) {
				CrmTreatmentConditionDtl dtl = new CrmTreatmentConditionDtl();
				dtl.setType(Tool.CRMEXAM);
				dtl.setExamId(tmp.getId());
				dtl.setTypeName(tmp.getName());
				dtl.setTypePkId(tmp.getPkId());
				getListExamDtl().add(dtl);
			}
			context.execute("PF('addExam').hide()");
			break;
		case "Employee":
			if(selectedEmployees == null || selectedEmployees.size() == 0) return;
			List<CrmTreatmentConditionDtl> deleteListEmp = new ArrayList<>();
			for (CrmTreatmentConditionDtl tmp : getListTreatDtl()) {
				if(tmp.getType() == null || tmp.getType().equals(""))
					deleteListEmp.add(tmp);
			}
			if(deleteListEmp.size() > 0)
				getListTreatDtl().removeAll(deleteListEmp);
			for (Employee tmp : selectedEmployees) {
				CrmTreatmentConditionDtl dtl = new CrmTreatmentConditionDtl();
				dtl.setType(Tool.CRMEMPLOYEE);
				dtl.setDepName(tmp.getSubOrganizationName());
				dtl.setTypeName(tmp.getFirstName());
				dtl.setTypePkId(tmp.getPkId());
				getListTreatDtl().add(dtl);
			}
			context.execute("PF('addEmployee').hide()");
			break;
		default:
			break;
		}
	}
	
	private String getAimagName(BigDecimal pkId)
	{
		for (Aimag tmp : listAimag) {
			if(pkId != null && tmp.getPkId().compareTo(pkId) == 0)
				return tmp.getName();
		}
		return "";
	}
	
	private String getSoumName(BigDecimal pkId)
	{
		for (Soum tmp : listSoum) {
			if(pkId != null && tmp.getPkId().compareTo(pkId) == 0)
				return tmp.getName();
		}
		return "";
	}
	
	private Diagnose getDiagnoseInfo(BigDecimal pkId)
	{
		for (Diagnose tmp : listDiagnose) {
			if(pkId != null && tmp.getPkId().compareTo(pkId) == 0)
				return tmp;
		}
		return null;
	}
	
	private Employee getEmpInfo(BigDecimal pkId)
	{
		for (Employee tmp : listEmployee) {
			if(pkId != null && tmp.getPkId().compareTo(pkId) == 0)
				return tmp;
		}
		return null;
	}
	
	private Examination getExamInfo(BigDecimal pkId)
	{
		for (Examination tmp : listExamination) {
			if(pkId != null && tmp.getPkId().compareTo(pkId) == 0)
				return tmp;
		}
		return null;
	}
	
	public void deleteConditionDtl(CrmTreatmentConditionDtl dtl, String type)
	{
		if(dtl.getType() == null || dtl.getType().equals("")) return;
		RequestContext context = RequestContext.getCurrentInstance();
		switch (type) {
		case "Age":
			getListAgeDtl().remove(dtl);
			context.update("@(.tblAge)");
			break;
		case "Address":
			getListAddressDtl().remove(dtl);
			context.update("@(.tblAddress)");
			break;
		case "Diagnose":
			getListDiagnoseDtl().remove(dtl);
			context.update("@(.tblDiagnose)");
			break;
		case "Exam":
			getListExamDtl().remove(dtl);
			context.update("@(.tblExam)");
			break;
		case "Employee":
			getListTreatDtl().remove(dtl);
			context.update("@(.tblTreat)");
			break;
		default:
			break;
		}
	}
	
	/**
	 * SMS ИЛГЭЭХ
	 * */
	public void smsSendFromList()
	{
		if(listCusMailSms == null || listCusMailSms.size() == 0)
		{
			userSessionController.showWarningMessage("И-мэйл илгээх үйлчлүүлэгчдийг сонгоно уу");
			return;
		}
		
		userSessionController.showSuccessMessage("Амжилттай илгээлээ");
	}
	
	/**
	 * E-mail ИЛГЭЭХ
	 * */
	public void deleteMailCustomer(Customer cus)
	{
		listCusMailSms.remove(cus);
	}
	
	public void addSelectCustomer()
	{
		try {
			listCustomer = logicCrm.getAllCrmCustomer();
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('selectCustomer').show();");
			context.update("form:selectCustomer");
		} catch (Exception e) {
			e.printStackTrace();
			userSessionController.showErrorMessage(e.toString());
		}
	}
	
	public void selectAllCustomer()
	{
		if(selectedMailCustomer == null || selectedMailCustomer.size() == 0)
		{
			userSessionController.showWarningMessage("Харилцагч заавал сонгоно уу");
			return;
		}
		for (CrmCustomer tmp : selectedMailCustomer) {
			if(!listCusMailSms.contains(tmp.getCustomer()))
				listCusMailSms.add(tmp.getCustomer());
		}
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("form:customerInfo");
		context.execute("PF('selectCustomer').hide();");
	}
	
	public void selectRowMail(CrmChannelInfo mail)
	{
		RequestContext context = RequestContext.getCurrentInstance();
		titleSend = mail.getTitle();
		noteSend = mail.getNote();
		context.update("@(.title)");
		context.update("@(.note)");
	}
	
	public void mailSendFromList()
	{
		if(listCusMailSms == null || listCusMailSms.size() == 0)
		{
			userSessionController.showWarningMessage("И-мэйл илгээх үйлчлүүлэгчдийг сонгоно уу");
			return;
		}
		for (Customer tmp : listCusMailSms) {
			emailSend(tmp, titleSend, noteSend);
		}
		userSessionController.showSuccessMessage("Амжилттай илгээлээ");
	}
	
	/**
	 * SMS -ИЙН БҮРТГЭЛ
	 * */
	public void getSmsInfoList()
	{
		try {
			listSmsInfo = logicCrm.getChannelInfo("SMS", titleSearch, valueSearch, "");
		} catch (Exception e) {
			userSessionController.showErrorMessage(e.toString());
		}
	}
	
	public void setNewSms()
	{
		currentSms = new CrmChannelInfo();
		currentSms.setChannelType("SMS");
		currentSms.setStatus(Tool.ADDED);
		currentSms.setActiveBoolean(true);
		currentSms.setIsActive(1);
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addSms').show();");
		context.update("form:addSms");
	}
	
	public void saveSms()
	{
		try
		{
			if(currentSms.getId() == null || currentSms.getId().equals(""))
			{
				userSessionController.showWarningMessage("Код заавал оруулна уу");
				return;
			}
			if(currentSms.getTitle() == null || currentSms.getTitle().equals(""))
			{
				userSessionController.showWarningMessage("Гарчиг заавал оруулна уу");
				return;
			}
			if(currentSms.getNote() == null || currentSms.getNote().equals(""))
			{
				userSessionController.showWarningMessage("Агуулга заавал оруулна уу");
				return;
			}
			logicCrm.saveSmsInfo(currentSms, userSessionController.getLoggedInfo());
			userSessionController.showSuccessMessage("Амжилттай хадгаллаа");
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('addSms').hide();");
			getSmsInfoList();
			context.update(":form:smsInfo");
		}
		catch(Exception e)
		{
			userSessionController.showErrorMessage(e.toString());
		}
	}
	
	public void updateCurrentSms(CrmChannelInfo sms)
	{
		currentSms = sms;
		currentSms.setStatus(Tool.MODIFIED);
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addSms').show();");
		context.update("form:addSms");
	}
	
	public void deleteSms(CrmChannelInfo sms)
	{
		try {
			sms.setStatus(Tool.DELETE);
			logicCrm.saveSmsInfo(sms, userSessionController.getLoggedInfo());
			userSessionController.showSuccessMessage("Амжилттай устгалаа");
			getSmsInfoList();
		} catch (Exception e) {
			e.printStackTrace();
			userSessionController.showErrorMessage(e.toString());
		}
	}
	
	/**
	 * Mail -ИЙН БҮРТГЭЛ
	 * */
	public void getMailInfoList()
	{
		try {
			listMailInfo = logicCrm.getChannelInfo("Mail", titleSearch, valueSearch, "");
		} catch (Exception e) {
			userSessionController.showErrorMessage(e.toString());
		}
	}
	
	public void setNewMail()
	{
		currentMail = new CrmChannelInfo();
		currentMail.setChannelType("Mail");
		currentMail.setStatus(Tool.ADDED);
		currentMail.setActiveBoolean(true);
		currentMail.setIsActive(1);
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addMail').show();");
		context.update("form:addMail");
	}
	
	public void saveMail()
	{
		try
		{
			if(currentMail.getId() == null || currentMail.getId().equals(""))
			{
				userSessionController.showWarningMessage("Код заавал оруулна уу");
				return;
			}
			if(currentMail.getTitle() == null || currentMail.getTitle().equals(""))
			{
				userSessionController.showWarningMessage("Гарчиг заавал оруулна уу");
				return;
			}
			if(currentMail.getNote() == null || currentMail.getNote().equals(""))
			{
				userSessionController.showWarningMessage("Агуулга заавал оруулна уу");
				return;
			}
			logicCrm.saveSmsInfo(currentMail, userSessionController.getLoggedInfo());
			userSessionController.showSuccessMessage("Амжилттай хадгаллаа");
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('addMail').hide();");
			getMailInfoList();
			context.update(":form:mailInfo");
		}
		catch(Exception e)
		{
			userSessionController.showErrorMessage(e.toString());
		}
	}
	
	public void updateCurrentMail(CrmChannelInfo mail)
	{
		currentMail = mail;
		currentMail.setStatus(Tool.MODIFIED);
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addMail').show();");
		context.update("form:addMail");
	}
	
	public void deleteMail(CrmChannelInfo mail)
	{
		try {
			mail.setStatus(Tool.DELETE);
			logicCrm.saveSmsInfo(mail, userSessionController.getLoggedInfo());
			userSessionController.showSuccessMessage("Амжилттай устгалаа");
			getMailInfoList();
		} catch (Exception e) {
			e.printStackTrace();
			userSessionController.showErrorMessage(e.toString());
		}
	}
	
	/**
	 * ХАРИЛЦАГЧИЙН ДЭЛГЭРЭНГҮЙ ХАЙЛТ
	 * */
	public void searchCrmCustomer()
	{
		try
		{
			listCrmCustomer = new ArrayList<CrmCustomer>();
			List<CrmCustomer> tmpLisCustomer = logicCrm.getCustomerSearch(cardNumber, customerName, phoneNumber, customerAddress, lifeCategory, notInfo, infoSms, infoMail); 
			for (CrmCustomer tmp : tmpLisCustomer) {
				if(maxAge > 0)
				{
					if(tmp.getCustomer().getAge() <= maxAge && tmp.getCustomer().getAge() >= minAge)
						listCrmCustomer.add(tmp);
				}
				else if(customerAddress != null && !customerAddress.equals(""))
				{
					if(tmp.getCustomer().getAddress().contains(customerAddress))
						listCrmCustomer.add(tmp);
				}
				else
					listCrmCustomer.add(tmp);
			}
		}
		catch(Exception ex)
		{
			userSessionController.showErrorMessage(ex.getMessage());
		}
	}
	
	public void sendMailSms(String type)
	{
		if(type.equals("Mail"))
		{
			for (CrmCustomer tmp : listCrmCustomer) {
				if(tmp.isSelected())
					emailSend(tmp.getCustomer(), "", "");
			}
		}
		else
		{
			for (CrmCustomer tmp : listCrmCustomer) {
				if(tmp.isSelected())
					smsSend(tmp);
			}
		}
	}
	
	public void smsSend(CrmCustomer customer)
	{
		try
		{
			
		}
		catch(Exception ex)
		{
			
		}
	}
	
	public void emailSend(Customer customer, String title, String note) {
		final String username = "chomunkhbayar@gmail.com";
		final String pass = "htjtultu;th";
		
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
					return new javax.mail.PasswordAuthentication(username, pass);
				}
			});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("chomunkhbayar@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(customer.getEmail()));
			
			message.setSubject(title);
			message.setText(note);
			
			Transport.send(message);
		} catch (MessagingException e) {
	    	e.printStackTrace();
		}
	}
	
	/**
	 * ХАРИЛЦАГЧИЙН ЖАГСААЛТ 
	 * */
	public void getCrmPatientList() throws Exception
	{
		listPatient = logicCrm.getAllCrmCustomer();
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("form:patList");
	}
	
	public void deletePatient(CrmCustomer customer) throws Exception
	{
		if(customer == null) return;
		listPatient.remove(customer);
		logicCrm.deleteCrmCustomer(customer);
		userSessionController.showSuccessMessage("Амжилттай устгалаа");
	}
	
	public String modifyCustomerList(CrmCustomer customer)
	{
		try {
			crmPatientInfo = customer;
			setCustomerFamilyList();
			setCustomerInfoList();
			setCustomerContactList();
			setOutTreamentList();
		} catch (Exception ex) {
			userSessionController.showErrorMessage(ex.getMessage());
		}
		return "patientInfo";
	}
	
	/**
	 * ШИНЭЭР CRM ИЙН ХАРИЛЦАГЧ НЭМЭХ, ЗАСАХ ХЭСЭГ
	 * */
	public String newCustomer() {
		try {
			currentCustomer = new CrmCustomer();
			currentCustomer.setStatus(Tool.ADDED);
			currentCustomer.setCustomer(new Customer());
		} catch (Exception ex) {
			userSessionController.showErrorMessage(ex.getMessage());
		}
		return "crm_register";
	}
	
	public String modifyCustomer() {
		try {
			if(crmPatientInfo == null)
			{
				userSessionController.showWarningMessage("Харилцагч заавал сонгоно уу");
				return "";
			}
			currentCustomer = crmPatientInfo;
			currentCustomer.setStatus(Tool.MODIFIED);
			selectedCustomer = crmPatientInfo.getCustomer();
		} catch (Exception ex) {
			userSessionController.showErrorMessage(ex.getMessage());
		}
		return "crm_register";
	}
	
	public String saveCustomer() throws Exception {
		String ret = "";
		if(currentCustomer.getCustomer() == null || currentCustomer.getCustomer().getPkId() == null || currentCustomer.getCustomer().getPkId().compareTo(BigDecimal.ZERO) == 0)
		{
			userSessionController.showWarningMessage("Харилцагч заавал сонгоно уу");
			return ret;
		}
		if(currentCustomer.getStatus().equals(Tool.ADDED) && logicCrm.getCustomerByPkId(currentCustomer.getCustomerPkId()))
		{
			userSessionController.showWarningMessage("Тухайн харилцагчийг CRM системд бүртгэсэн байна.");
			return ret;
		}
		String feedBackType = "";
		if(!currentCustomer.isInfoCheck())
		{
			feedBackType = "";
		}
		else
		{
			if(currentCustomer.isMailCheck())
				feedBackType += "Mail";
			if(currentCustomer.isSmsCheck())
				feedBackType += feedBackType == "" ? "SMS" : ",SMS";
			if(currentCustomer.isSnsCheck())
				feedBackType += feedBackType == "" ? "SNS" : ",SNS";
			if(currentCustomer.isDmCheck())
				feedBackType += feedBackType == "" ? "DM" : ",DM";
		}
		currentCustomer.setFeedBackType(feedBackType);
		logicCrm.saveCustomer(currentCustomer);
		ret = "patientInfo";
		return ret;
	}
	
	/**
	 * ГЭР БҮЛИЙН ГИШҮҮДИЙН МЭДЭЭЛЭЛ НЭМЖ, ХАСАХ, УСТГАХ ХЭСЭГ
	 * */
	private void setCustomerFamilyList() throws Exception{
		if(crmPatientInfo == null)
			return;
		listFamily = logicCrm.getCustomerFamily(crmPatientInfo.getPkId());
		if(listFamily == null)
			listFamily = new ArrayList<>();
		else
			for (CrmCustomerFamily tmp : listFamily) {
				tmp.setStatus(Tool.MODIFIED);
			}
		listFamily.add(setNewCustomerFamily());
	}
	
	private CrmCustomerFamily setNewCustomerFamily()
	{
		CrmCustomerFamily dtl = new CrmCustomerFamily();
		dtl.setCrmCustomerPkId(crmPatientInfo.getPkId());
		dtl.setStatus(Tool.LAST);
		return dtl;
	}
	
	public void addNewRowFamily(CrmCustomerFamily info) throws Exception
	{
		info.setStatus(Tool.ADDED);
		info.setPkId(logicCrm.setCustomerFamily(info));
		listFamily.add(setNewCustomerFamily());
	}
	
	public void modifyRowFamily(CrmCustomerFamily info) throws Exception
	{
		if(info.getStatus().equals(Tool.LAST)) return;
		info.setStatus(Tool.MODIFIED);
		logicCrm.setCustomerFamily(info);
	}
	
	public void deleteRowFamily(CrmCustomerFamily info) throws Exception
	{
		info.setStatus(Tool.DELETE);
		logicCrm.setCustomerFamily(info);
		listFamily.remove(info);
	}
	
	/**
	 * ҮЙЛЧЛҮҮЛЭГЧИЙН ТУХАЙ МЭДЭЭЛЭЛ НЭМЖ, ЗАСАХ, УСТГАХ ХЭСЭГ
	 * */
	private void setCustomerInfoList() throws Exception{
		if(crmPatientInfo == null) return;
		listInfo = logicCrm.getCustomerInfo(crmPatientInfo.getPkId());
		if(listInfo == null)
			listInfo = new ArrayList<>();
		else
			for (CrmCustomerInfo tmp : listInfo) {
				tmp.setStatus(Tool.MODIFIED);
			}
		listInfo.add(setNewCustomerInfo());
	}
	
	private CrmCustomerInfo setNewCustomerInfo()
	{
		CrmCustomerInfo dtl = new CrmCustomerInfo();
		dtl.setCrmCustomerPkId(crmPatientInfo.getPkId());
		dtl.setCreatedDate(new Date());
		dtl.setCreatedBy(userSessionController.getLoggedInfo().getUser().getPkId());
		dtl.setCreatedByName(userSessionController.getLoggedInfo().getUser().getName());
		dtl.setStatus(Tool.LAST);
		return dtl;
	}
	
	public void addNewRowInfo(CrmCustomerInfo info) throws Exception
	{
		info.setStatus(Tool.ADDED);
		info.setCreatedDate(new Date());
		info.setPkId(logicCrm.setCustomerInfo(info));
		listInfo.add(setNewCustomerInfo());
	}
	
	public void modifyRowInfo(CrmCustomerInfo info) throws Exception
	{
		if(info.getStatus().equals(Tool.LAST)) return;
		info.setStatus(Tool.MODIFIED);
		logicCrm.setCustomerInfo(info);
	}
	
	public void deleteRowInfo(CrmCustomerInfo info) throws Exception
	{
		info.setStatus(Tool.DELETE);
		logicCrm.setCustomerInfo(info);
		listInfo.remove(info);
	}
	
	/**
	 * ХОЛБОО БАРЬСАН ТҮҮХ НЭМЖ, ЗАСАХ, УСТГАХ ХЭСЭГ
	 * */
	private void setCustomerContactList() throws Exception{
		if(crmPatientInfo == null) return;
		listContact = logicCrm.getCustomerContactHistory(crmPatientInfo.getPkId());
		if(listContact == null)
			listContact = new ArrayList<>();
		else
			for (CrmCustomerContactHistory tmp : listContact) {
				tmp.setStatus(Tool.MODIFIED);
			}
		listContact.add(setNewCustomerContact());
	}
	
	private CrmCustomerContactHistory setNewCustomerContact()
	{
		CrmCustomerContactHistory dtl = new CrmCustomerContactHistory();
		dtl.setCrmCustomerPkId(crmPatientInfo.getPkId());
		dtl.setContactDate(new Date());
		dtl.setStatus(Tool.LAST);
		return dtl;
	}
	
	public void addNewRowContact(CrmCustomerContactHistory info) throws Exception
	{
		info.setStatus(Tool.ADDED);
		info.setPkId(logicCrm.setCustomerContactHistory(info));
		listContact.add(setNewCustomerContact());
	}
	
	public void modifyRowContact(CrmCustomerContactHistory info) throws Exception
	{
		if(info.getStatus().equals(Tool.LAST)) return;
		info.setStatus(Tool.MODIFIED);
		logicCrm.setCustomerContactHistory(info);
	}
	
	public void deleteRowContact(CrmCustomerContactHistory info) throws Exception
	{
		info.setStatus(Tool.DELETE);
		logicCrm.setCustomerContactHistory(info);
		listContact.remove(info);
	}
	
	/**
	 * ГАДУУР ЭМЧИЛГЭЭНИЙ ХЭСЭГ
	 * */
	public void setOutTreamentList() throws Exception
	{
		if(crmPatientInfo == null) return;
		List<CrmInfo> tmpList = logicCrm.getTreatmentInfo(crmPatientInfo.getCustomer().getPkId());
		listInpatient = new ArrayList<>();
		listExam = new ArrayList<>();
		listSurgery = new ArrayList<>();
		if(tmpList != null && tmpList.size() > 0)
		{
			for (CrmInfo crmInfo : tmpList) {
				if(crmInfo.getType().equals(Tool.INPATIENTDTLSTATUS))
					listInpatient.add(crmInfo);
				else if(crmInfo.getType().equals(Tool.INSPECTIONTYPE_EXAMINATION))
					listExam.add(crmInfo);
				else if(crmInfo.getType().equals(Tool.INSPECTIONTYPE_SURGERY))
					listSurgery.add(crmInfo);
			}
		}
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("outTab");
	}
	
	public void showExamresult(CrmInfo tmp) throws Exception{
		listExamResult = logicCrm.getExaminationResult(tmp.getExamPkId());
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("form:exaRequests");
	}
	/**
	 * Searching patient information дуудах
	 */
	public String searchPatientInfo(){
		
		return("/crm/patientInfoSearch.xhtml");
	}
	
	/**
	 * ХАРИЛЦАГЧИЙН МЭДЭЭЛЛИЙГ ЭКСЭЛЭЭС ТАТАХ
	 * */
	public void fileUploadListener(FileUploadEvent event) throws IOException{
		try
		{
	        FileInputStream fis= (FileInputStream) event.getFile().getInputstream();
		    HSSFWorkbook wb = new HSSFWorkbook(fis);
		    HSSFSheet sheet = wb.getSheetAt(0);
		    FormulaEvaluator formulaEvaluator = wb.getCreationHelper().createFormulaEvaluator();
		    int i = 0;
		    List<CrmCustomer> lstCustomer = new ArrayList<CrmCustomer>();
		    List<String> listCardNumber = new ArrayList<String>();
		    List<BigDecimal> listCusPkId = new ArrayList<BigDecimal>();
		    List<String> listOldCusCard = new ArrayList<String>();
		    for(Row row: sheet){
		    	if(i == 0)
		    	{
			    	i++;
		    		continue;
		    	}
		    	CrmCustomer customer = new CrmCustomer();
		    	for(Cell cell: row){
		            switch(formulaEvaluator.evaluateInCell(cell).getCellType()){
		            case Cell.CELL_TYPE_NUMERIC:
		            	customer.setBloodType(((int)cell.getNumericCellValue()) + "");
		                break;
		            case Cell.CELL_TYPE_STRING:
		            	if(cell.getColumnIndex() == 0)
		            	{
		                	customer.setCardNumber(cell.getStringCellValue());
		                	listCardNumber.add(cell.getStringCellValue());
		            	}
		            	else if(cell.getColumnIndex() == 1)
		                	customer.setWeddingStatus(cell.getStringCellValue());
		                else if(cell.getColumnIndex() == 3)
		                	customer.setFeedBackType(cell.getStringCellValue());
		                else if(cell.getColumnIndex() == 4)
		                	customer.setLifeCategory(cell.getStringCellValue());
		                else if(cell.getColumnIndex() == 5)
		                	customer.setControlType(cell.getStringCellValue());
		                break;
		            }
		    	}
	            if(customer.getCardNumber() != null)
	            	lstCustomer.add(customer);
		    }
		    BigDecimal newPkId = Tools.newPkId();
		    List<Customer> listCus = logicCrm.getListCustomer(listCardNumber);
		    for (CrmCustomer tmp : lstCustomer) {
		    	BigDecimal customerPkId = BigDecimal.ZERO;
				for (Customer customer : listCus) {
					if(tmp.getCardNumber().equals(customer.getCardNumber()))
						customerPkId = customer.getPkId();
				}
				if(customerPkId.compareTo(BigDecimal.ZERO) == 0)
				{
					userSessionController.showErrorMessage(String.format("%s картын дугаартай үйлчлүүлэгч системд бүртгэлгүй байна.", tmp.getCardNumber()));
					return;
				}
				else
				{
					tmp.setCustomerPkId(customerPkId);
					listCusPkId.add(customerPkId);
				}
				newPkId = newPkId.add(BigDecimal.ONE);
				tmp.setPkId(newPkId);
			}
		    listOldCusCard = logicCrm.getListCrmCustomer(listCusPkId);
		    if(listOldCusCard.size() > 0)
		    {
		    	String message = "";
		    	for (String tmp : listOldCusCard) {
					message += tmp + " \n";
				}
		    	userSessionController.showErrorMessage(String.format("%s картын дугаартай үйлчлүүлэгч системд бүртгэлтэй байна. Эксел файлаас устгаад дахин татна уу.", message));
				return;
		    }
		    logicCrm.insertCrmCustomerList(lstCustomer);
		    getCrmPatientList();
		}catch (Exception ex) {
			userSessionController.showErrorMessage(ex.getMessage());
		}
    }
	
	public UserSessionController getUserSessionController() {
		return userSessionController;
	}
	public void setUserSessionController(UserSessionController userSessionController) {
		this.userSessionController = userSessionController;
	}
	public ApplicationController getApplicationController() {
		return applicationController;
	}

	public void setApplicationController(ApplicationController applicationController) {
		this.applicationController = applicationController;
	}

	public Customer getSelectedCustomer() {
		return selectedCustomer;
	}

	public void setSelectedCustomer(Customer selectedCustomer) throws Exception {
		this.selectedCustomer = selectedCustomer;
		currentCustomer.setCustomer(selectedCustomer);
		currentCustomer.setCustomerPkId(selectedCustomer.getPkId());
	}

	public CrmCustomer getCrmPatientInfo() {
		return crmPatientInfo;
	}

	public void setCrmPatientInfo(CrmCustomer crmPatientInfo) throws Exception {
		this.crmPatientInfo = crmPatientInfo;
		setCustomerFamilyList();
		setCustomerInfoList();
		setCustomerContactList();
		setOutTreamentList();
	}

	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public CrmCustomer getCurrentCustomer() {
		return currentCustomer;
	}

	public void setCurrentCustomer(CrmCustomer currentCustomer) {
		this.currentCustomer = currentCustomer;
	}
	
	public List<CrmCustomerFamily> getListFamily() {
		return listFamily;
	}

	public void setListFamily(List<CrmCustomerFamily> listFamily) {
		this.listFamily = listFamily;
	}

	public List<CrmCustomerInfo> getListInfo() {
		return listInfo;
	}

	public void setListInfo(List<CrmCustomerInfo> listInfo) {
		this.listInfo = listInfo;
	}

	public List<CrmCustomerContactHistory> getListContact() {
		return listContact;
	}

	public void setListContact(List<CrmCustomerContactHistory> listContact) {
		this.listContact = listContact;
	}

	public List<SubOrganization> getListSubOrganization() {
		return listSubOrganization;
	}

	public void setListSubOrganization(List<SubOrganization> listSubOrganization) {
		this.listSubOrganization = listSubOrganization;
	}

	public List<CrmInfo> getListInpatient() {
		return listInpatient;
	}

	public void setListInpatient(List<CrmInfo> listInpatient) {
		this.listInpatient = listInpatient;
	}

	public List<CrmInfo> getListExam() {
		return listExam;
	}

	public void setListExam(List<CrmInfo> listExam) {
		this.listExam = listExam;
	}

	public List<CrmInfo> getListExamResult() {
		return listExamResult;
	}

	public void setListExamResult(List<CrmInfo> listExamResult) {
		this.listExamResult = listExamResult;
	}

	public List<CrmInfo> getListSurgery() {
		return listSurgery;
	}

	public void setListSurgery(List<CrmInfo> listSurgery) {
		this.listSurgery = listSurgery;
	}

	public List<CrmCustomer> getListPatient() {
		return listPatient;
	}

	public void setListPatient(List<CrmCustomer> listPatient) {
		this.listPatient = listPatient;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getLifeCategory() {
		return lifeCategory;
	}

	public void setLifeCategory(String lifeCategory) {
		this.lifeCategory = lifeCategory;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public boolean isNotInfo() {
		return notInfo;
	}

	public void setNotInfo(boolean notInfo) {
		this.notInfo = notInfo;
	}

	public boolean isInfoSms() {
		return infoSms;
	}

	public void setInfoSms(boolean infoSms) {
		this.infoSms = infoSms;
	}

	public boolean isInfoMail() {
		return infoMail;
	}

	public void setInfoMail(boolean infoMail) {
		this.infoMail = infoMail;
	}

	public List<CrmCustomer> getListCrmCustomer() {
		return listCrmCustomer;
	}

	public void setListCrmCustomer(List<CrmCustomer> listCrmCustomer) {
		this.listCrmCustomer = listCrmCustomer;
	}

	public String getTotalCount() {
		totalCount = "Нийт тоо : " + (listCrmCustomer == null ? 0 : listCrmCustomer.size());
		return totalCount;
	}

	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}

	public String getTitleSearch() {
		return titleSearch;
	}

	public void setTitleSearch(String titleSearch) {
		this.titleSearch = titleSearch;
	}

	public String getValueSearch() {
		return valueSearch;
	}

	public void setValueSearch(String valueSearch) {
		this.valueSearch = valueSearch;
	}

	public List<CrmChannelInfo> getListSmsInfo() {
		return listSmsInfo;
	}

	public void setListSmsInfo(List<CrmChannelInfo> listSmsInfo) {
		this.listSmsInfo = listSmsInfo;
	}

	public CrmChannelInfo getCurrentSms() {
		if(currentSms == null)
			currentSms = new CrmChannelInfo();
		return currentSms;
	}

	public void setCurrentSms(CrmChannelInfo currentSms) {
		this.currentSms = currentSms;
	}

	public List<CrmChannelInfo> getListMailInfo() {
		return listMailInfo;
	}

	public void setListMailInfo(List<CrmChannelInfo> listMailInfo) {
		this.listMailInfo = listMailInfo;
	}

	public CrmChannelInfo getCurrentMail() {
		if(currentMail == null)
			currentMail = new CrmChannelInfo();
		return currentMail;
	}

	public void setCurrentMail(CrmChannelInfo currentMail) {
		this.currentMail = currentMail;
	}

	public List<CrmCustomer> getListCustomer() {
		return listCustomer;
	}

	public void setListCustomer(List<CrmCustomer> listCustomer) {
		this.listCustomer = listCustomer;
	}

	public List<Customer> getListCusMailSms() {
		if(listCusMailSms == null)
			listCusMailSms = new ArrayList<Customer>();
		return listCusMailSms;
	}

	public void setListCusMailSms(List<Customer> listCusMailSms) {
		this.listCusMailSms = listCusMailSms;
	}

	public List<CrmCustomer> getSelectedMailCustomer() {
		return selectedMailCustomer;
	}

	public void setSelectedMailCustomer(List<CrmCustomer> selectedMailCustomer) {
		this.selectedMailCustomer = selectedMailCustomer;
	}

	public CrmChannelInfo getSelectedMail() {
		if(selectedMail == null)
			selectedMail = new CrmChannelInfo();
		return selectedMail;
	}

	public void setSelectedMail(CrmChannelInfo selectedMail) {
		this.selectedMail = selectedMail;
	}

	public String getTitleSend() {
		return titleSend;
	}

	public void setTitleSend(String titleSend) {
		this.titleSend = titleSend;
	}

	public String getNoteSend() {
		return noteSend;
	}

	public void setNoteSend(String noteSend) {
		this.noteSend = noteSend;
	}

	public List<CrmTreatmentCondition> getListTreatment() {
		return listTreatment;
	}

	public void setListTreatment(List<CrmTreatmentCondition> listTreatment) {
		this.listTreatment = listTreatment;
	}

	public CrmTreatmentCondition getCurrentTreat() {
		return currentTreat;
	}

	public void setCurrentTreat(CrmTreatmentCondition currentTreat) {
		this.currentTreat = currentTreat;
	}

	public List<CrmTreatmentConditionDtl> getListTreatDtl() {
		if(listTreatDtl == null)
			listTreatDtl = new ArrayList<>();
		return listTreatDtl;
	}

	public void setListTreatDtl(List<CrmTreatmentConditionDtl> listTreatDtl) {
		this.listTreatDtl = listTreatDtl;
	}

	public List<CrmTreatmentConditionDtl> getListExamDtl() {
		if(listExamDtl == null)
			listExamDtl = new ArrayList<>();
		return listExamDtl;
	}

	public void setListExamDtl(List<CrmTreatmentConditionDtl> listExamDtl) {
		this.listExamDtl = listExamDtl;
	}

	public List<CrmTreatmentConditionDtl> getListDiagnoseDtl() {
		if(listDiagnoseDtl == null)
			listDiagnoseDtl = new ArrayList<>();
		return listDiagnoseDtl;
	}

	public void setListDiagnoseDtl(List<CrmTreatmentConditionDtl> listDiagnoseDtl) {
		this.listDiagnoseDtl = listDiagnoseDtl;
	}

	public List<CrmTreatmentConditionDtl> getListAgeDtl() {
		if(listAgeDtl == null)
			listAgeDtl = new ArrayList<>();
		return listAgeDtl;
	}

	public void setListAgeDtl(List<CrmTreatmentConditionDtl> listAgeDtl) {
		this.listAgeDtl = listAgeDtl;
	}

	public List<CrmTreatmentConditionDtl> getListAddressDtl() {
		if(listAddressDtl == null)
			listAddressDtl = new ArrayList<>();
		return listAddressDtl;
	}

	public void setListAddressDtl(List<CrmTreatmentConditionDtl> listAddressDtl) {
		this.listAddressDtl = listAddressDtl;
	}

	public int getBeginAge() {
		return beginAge;
	}

	public void setBeginAge(int beginAge) {
		this.beginAge = beginAge;
	}

	public int getEndAge() {
		return endAge;
	}

	public void setEndAge(int endAge) {
		this.endAge = endAge;
	}

	public List<Aimag> getListAimag() {
		return listAimag;
	}

	public void setListAimag(List<Aimag> listAimag) {
		this.listAimag = listAimag;
	}

	public List<Soum> getListSoum() {
		return listSoum;
	}

	public void setListSoum(List<Soum> listSoum) {
		this.listSoum = listSoum;
	}

	public BigDecimal getAimagPkId() {
		return aimagPkId;
	}

	public void setAimagPkId(BigDecimal aimagPkId) {
		this.aimagPkId = aimagPkId;
	}

	public BigDecimal getSoumPkId() {
		return soumPkId;
	}

	public void setSoumPkId(BigDecimal soumPkId) {
		this.soumPkId = soumPkId;
	}

	public List<Diagnose> getListDiagnose() {
		return listDiagnose;
	}

	public void setListDiagnose(List<Diagnose> listDiagnose) {
		this.listDiagnose = listDiagnose;
	}

	public List<Diagnose> getSelectedDiagnoses() {
		return selectedDiagnoses;
	}

	public void setSelectedDiagnoses(List<Diagnose> selectedDiagnoses) {
		this.selectedDiagnoses = selectedDiagnoses;
	}

	public List<Examination> getListExamination() {
		return listExamination;
	}

	public void setListExamination(List<Examination> listExamination) {
		this.listExamination = listExamination;
	}

	public List<Examination> getSelectedExaminations() {
		return selectedExaminations;
	}

	public void setSelectedExaminations(List<Examination> selectedExaminations) {
		this.selectedExaminations = selectedExaminations;
	}

	public List<Employee> getListEmployee() {
		return listEmployee;
	}

	public void setListEmployee(List<Employee> listEmployee) {
		this.listEmployee = listEmployee;
	}

	public List<Employee> getSelectedEmployees() {
		return selectedEmployees;
	}

	public void setSelectedEmployees(List<Employee> selectedEmployees) {
		this.selectedEmployees = selectedEmployees;
	}

	public List<SubOrganization> getListSubOrg() {
		return listSubOrg;
	}

	public void setListSubOrg(List<SubOrganization> listSubOrg) {
		this.listSubOrg = listSubOrg;
	}

	public Date getSearchDate() {
		if(searchDate == null)
			searchDate = new Date();
		return searchDate;
	}

	public void setSearchDate(Date searchDate) {
		this.searchDate = searchDate;
	}

	public List<CrmTreatmentConditionResult> getListResult() {
		return listResult;
	}

	public void setListResult(List<CrmTreatmentConditionResult> listResult) {
		this.listResult = listResult;
	}

	public String getSelectedConditionType() {
		return selectedConditionType;
	}

	public void setSelectedConditionType(String selectedConditionType) {
		this.selectedConditionType = selectedConditionType;
	}

	public CrmTreatmentConditionResult getCurrentResult() {
		return currentResult;
	}

	public void setCurrentResult(CrmTreatmentConditionResult currentResult) {
		this.currentResult = currentResult;
	}

	public Employee getCurrentEmployee() {
		if(currentEmployee == null)
			currentEmployee = new Employee();
		return currentEmployee;
	}

	public void setCurrentEmployee(Employee currentEmployee) {
		this.currentEmployee = currentEmployee;
	}
	
	public List<CrmTreatmentConditionResultDtl> getListResultDtl() {
		return listResultDtl;
	}

	public void setListResultDtl(List<CrmTreatmentConditionResultDtl> listResultDtl) {
		this.listResultDtl = listResultDtl;
	}

	public BigDecimal getSubOrgPkId() {
		return subOrgPkId;
	}

	public void setSubOrgPkId(BigDecimal subOrgPkId) {
		this.subOrgPkId = subOrgPkId;
	}

	public BigDecimal getSearchEmployeePkId() {
		return searchEmployeePkId;
	}

	public void setSearchEmployeePkId(BigDecimal searchEmployeePkId) {
		this.searchEmployeePkId = searchEmployeePkId;
	}

	public String getSearchCustomerName() {
		return searchCustomerName;
	}

	public void setSearchCustomerName(String searchCustomerName) {
		this.searchCustomerName = searchCustomerName;
	}

	public List<CrmChannelInfo> getListSelectInfo() {
		return listSelectInfo;
	}

	public void setListSelectInfo(List<CrmChannelInfo> listSelectInfo) {
		this.listSelectInfo = listSelectInfo;
	}
}