package crm.web.controller;

import java.awt.Color;
import java.awt.Label;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import crm.businessentity.Tool;
import crm.businesslogic.interfaces.ILogicHomeLocal;
import crm.entity.*;

@SessionScoped
@ManagedBean(name = "homeController")
public class HomeController {
	@ManagedProperty(value = "#{userController}")
	private UserSessionController userSessionController;

	@ManagedProperty(value = "#{applicationController}")
	private ApplicationController applicationController;
	
	@EJB(beanName = "LogicHome")
	
	ILogicHomeLocal  loHomeLocal;
	List<Organization> organizations;
	List<Role>  rolelist;
	List<SubOrganization> subOrganizations;
	List<SubOrganization> listsubOrganizations;
	List<Users>  users;
	List<Employee>  employees;
	List<Customer>  customers;
	List<String>  liststring;
	List<String>  selectedSubOrgaValue;
	List<LoginDialog> loginDialogs;
	
	private List<Employee> employeelist;
	private List<String> selectedEmployee;
	private List<Post>  listpost;
	private List<Post>  listreceiverPkId;
	private List<Post>  listpostgroup;
	private List<Post>  listemployee;
	private List<Post> listpostEmployeePkId;
	
	private List<Post>  lispostPkId;
	private List<Post> listPostMapPkId;
	private List<String>  listbgColor;
	private List<UpgradingLog> upgradeLogs;
	
	private boolean loginUser=false;
	private Post  posts;
	private Date selectDate;
	private Post  post;
	private Employee  employee;
	private String selecttext;
	private UpgradingLog selectUpgradeLog;
	
	@PostConstruct
	public void init(){
		postView();
		setSelectName();
	}
	//Шинэчлэлт  хийхэд харуулах  функц
	public void upgrade(){
		try {
			setUpgradeLogs(loHomeLocal.getUpgrades());
			UserRoleMap user = loHomeLocal.getLoggedUserRole(getUserSessionController().getLoggedInfo());
			System.out.println( "name shuu " + user.getUserPkId());
			if(user.getUserPkId()!=null){
				if(user.getUserPkId().equals(getUserSessionController().getLoggedInfo().getUser().getPkId()))
					setLoginUser(true);
			}
			System.out.println( "name shuu boolean " + isLoginUser());
		} catch (Exception e) {
			e.printStackTrace();
			userSessionController.showErrorMessage(e.getMessage());
		}
	}
	//Шинэчлэлт  хадгалах функц
	public void saveUpgrade(){
		try {
			RequestContext context  =  RequestContext.getCurrentInstance();
			selectUpgradeLog.setStatus(Tool.ADDED);
			loHomeLocal.saveUpgradeLog(selectUpgradeLog,getUserSessionController().getLoggedInfo());
			setSelectUpgradeLog(null);
			selectUpgradeLog = new UpgradingLog();
			context.update("form:logTabId:logSavePanelId");
		} catch (Exception e) {
			userSessionController.showErrorMessage(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public  void  setSelectName(){
		try {
			listsubOrganizations  =  loHomeLocal.getSelectionSubOrganization();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public  void  action(){
		List<BigDecimal> bigDecimal  =  new ArrayList<BigDecimal>();
		for (int i = 0; i < getSelectedSubOrgaValue().size(); i++) {
			bigDecimal.add(new BigDecimal(getSelectedSubOrgaValue().get(i)));
		}
		System.out.println("DD" + bigDecimal.size());
		try {
			if(bigDecimal.size() < 1){
				employeelist  =  new ArrayList<>();
			}else {
				employeelist  =  loHomeLocal.getSelectionEmployee(bigDecimal);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public  String getName(String name ){
		String ret = "";
		try {

			switch (name) {
			case "Байгууллага":
				organizations  =  loHomeLocal.getOrganizations();
				ret= " " + organizations.size() +"";
				break;
			case "Хандах эрх":
				rolelist   = loHomeLocal.getAccessLaw();
				ret= " " + rolelist.size()+ "";
				break;
			case "Хэрэглэгч":
				users   =  loHomeLocal.getUsers();
				ret  = " " + users.size() + " ";
				break;
			case "Эдийн засгийн хуанли":
				break;
			case "Кабинет":
				subOrganizations  =  loHomeLocal.getRoom();
				ret = " " + subOrganizations.size() +" ";
				break;
			case "Ажилтан":
				employees   =  loHomeLocal.getEmployee();
				ret = " " + employees.size() +" ";
				break;
			case "Эмчилгээ":
				break;
			case "Үйлчлүүлэгч ":
				customers   =  loHomeLocal.getCustomer();
				ret = " " + customers.size() +" ";
				break;
			case "Захиалга":
				break;
			case "Тайлан":
				break;
			case "Хийгдэх хагалгааны жагсаалт":
				break;
			case "Хийгдэх эмчилгээний жагсаалт":
				break;
			case "Төлөвлөлтийн цагалбар":
				break;
			case "Удирдлага хяналт":
				break;
			default:
				break;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return ret;
	}
	public  String  getActive(String st){
		String  str="";
		try {
			switch (st) {
			
			default:
				break;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return str;
	}
	public  String savePost(){
		try {
			List<BigDecimal>  bigDecimals  =  new ArrayList<>();
			for (int i = 0; i < getSelectedEmployee().size(); i++) {
				bigDecimals.add( new BigDecimal(getSelectedEmployee().get(i)));
			}		
			if (getSelectedEmployee()==null || getSelectedEmployee().isEmpty()) {
				userSessionController.showErrorMessage("Кабинет сонгоогүй байна");
				return "";
			}
			else if(getSelectedSubOrgaValue()==null  || getSelectedSubOrgaValue().isEmpty()){
				userSessionController.showErrorMessage("Ажилтан  сонгоогүй байна");
				return "";
			}
			else if (getSelecttext()==null || getSelecttext().isEmpty()) {
				userSessionController.showErrorMessage("Пост оруулаагүй байна");
				return "";
			}
			RequestContext  context  =  RequestContext.getCurrentInstance();
			Date  d   =  new Date();
			loHomeLocal.setPostSave(getPost(), getSelecttext(),userSessionController.getLoggedInfo().getEmployeePkId(),bigDecimals,d);
			setPost(null);
			listpost  =  loHomeLocal.getPostView(userSessionController.getLoggedInfo().getEmployee().getPkId());
			getUserSessionController().setShowMessage("Амжилттай хадгаллаа");
			context.update("form:panels");
			return "home.html";

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return "home.html";
	}
	public void showMessage(int index) {
		getUserSessionController().showMessage(index);
	}
	public  void  postView(){
		try {
			loHomeLocal.getPostPostPkIdView(userSessionController.getLoggedInfo().getEmployee().getPkId());
			listpost  =  loHomeLocal.getPostView(userSessionController.getLoggedInfo().getEmployee().getPkId());
			System.out.println( "reciever " + listreceiverPkId.size());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public  List<Post> getpostViewList(BigDecimal pkId){
		List<Post>  p  = new ArrayList<>();
		try {
			p   =  loHomeLocal.getPostMapReceiverPkId(pkId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return p;
	}
	public  void  deletePost(Post  p ){
		try {
			p.setStatus(Tool.DELETE);
			loHomeLocal.setPostDeleteModify(p);
			listpost  =  loHomeLocal.getPostView(userSessionController.getLoggedInfo().getEmployee().getPkId());

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void  editPost(Post  p){
		if (userSessionController.getLoggedInfo().getEmployeePkId().equals(p.getSenderEmployeePkId())) {
			try {
				BigDecimal  postMapPkid = null;
				lispostPkId  = loHomeLocal.getPostPostPkId(p.getPostpkid());
				for (int i = 0; i < lispostPkId.size(); i++) {
					postMapPkid= lispostPkId.get(i).getPostMaPkId();
				}
				listpost  =  loHomeLocal.getPostView(userSessionController.getLoggedInfo().getEmployee().getPkId());
				listPostMapPkId  =  loHomeLocal.getPostPostUpdate(postMapPkid);
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e.getMessage());
			}
		}
		else 
		{
			userSessionController.showErrorMessage("Засварлах  боломжгүй");
		}
	}
	public void  update(Post pos){
		pos.setStatus(Tool.MODIFIED);
		try {
			loHomeLocal.setPostDeleteModify(pos);
			listpost  =  loHomeLocal.getPostView(userSessionController.getLoggedInfo().getEmployee().getPkId());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public String getDateConvert(Date d){
		SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd");
		if(d!=null)
			return  format.format(d);
		
		return "";
	}
	public void panelupdate(){
		getSelectedEmployee().clear();
		getSelectedSubOrgaValue().clear();
		setSelecttext(null);
	}
	public String  bgColorString() {
		String bgcolors="";
		listbgColor  =  new ArrayList<>();
		listbgColor.add("#ed5565");
		listbgColor.add("#0088cc");
		listbgColor.add("#2baab1");
		listbgColor.add("#383f48");
		listbgColor.add("#8CC152");
		listbgColor.add("#E9573F");
		listbgColor.add("#D770AD");
		Random  random  =  new Random();
		for (int j = 0; j < userSessionController.getMenus().size(); j++) {
			bgcolors =	listbgColor.get(random.nextInt(listbgColor.size()));
		}
		return  bgcolors;
	}
	public String  columnDivide(int k){
		k++;
		if (k%6==0) return "</p><p hidden>";
		return "";

	}
	public List<Post> getLispostPkId() {
		return lispostPkId;
	}
	public void setLispostPkId(List<Post> lispostPkId) {
		this.lispostPkId = lispostPkId;
	}
	public List<Organization> getOrganizations() {
		return organizations;
	}
	public void setOrganizations(List<Organization> organizations) {
		this.organizations = organizations;
	}
	public UserSessionController getUserSessionController() {
		return userSessionController;
	}
	public void setUserSessionController(UserSessionController userSessionController) {
		this.userSessionController = userSessionController;
	}
	public ApplicationController getApplicationController() {
		return applicationController;
	}
	public void setApplicationController(ApplicationController applicationController) {
		this.applicationController = applicationController;
	}
	public ILogicHomeLocal getLoHomeLocal() {
		return loHomeLocal;
	}
	public void setLoHomeLocal(ILogicHomeLocal loHomeLocal) {
		this.loHomeLocal = loHomeLocal;
	}
	public List<Role> getRolelist() {
		return rolelist;
	}
	public void setRolelist(List<Role> rolelist) {
		this.rolelist = rolelist;
	}
	public List<SubOrganization> getSubOrganizations() {
		return subOrganizations;
	}
	public void setSubOrganizations(List<SubOrganization> subOrganizations) {
		this.subOrganizations = subOrganizations;
	}
	public List<Users> getUsers() {
		return users;
	}
	public void setUsers(List<Users> users) {
		this.users = users;
	}
	public List<Employee> getEmployees() {
		return employees;
	}
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	public List<Customer> getCustomers() {
		return customers;
	}
	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}
	public List<SubOrganization> getListsubOrganizations() {
		return listsubOrganizations;
	}
	public void setListsubOrganizations(List<SubOrganization> listsubOrganizations) {
		this.listsubOrganizations = listsubOrganizations;
	}
	public List<String> getListstring() {
		return liststring;
	}
	public void setListstring(List<String> liststring) {
		this.liststring = liststring;
	}
	public List<String> getSelectedSubOrgaValue() {
		return selectedSubOrgaValue;
	}
	public void setSelectedSubOrgaValue(List<String> selectedSubOrgaValue) {
		this.selectedSubOrgaValue = selectedSubOrgaValue;
	}
	public List<Employee> getEmployeelist() {
		return employeelist;
	}
	public void setEmployeelist(List<Employee> employeelist) {
		this.employeelist = employeelist;
	}
	public List<String> getSelectedEmployee() {
		return selectedEmployee;
	}
	public void setSelectedEmployee(List<String> selectedEmployee) {
		this.selectedEmployee = selectedEmployee;
	}
	public Post getPost() {
		if (post==null) {
			post  =  new Post();
			post.setStatus(Tool.ADDED);
		}
		else if(post==null) {
			post  =  new Post();
			post.setStatus(Tool.DELETE);
		}
		return post;
	}
	public void setPost(Post post) {
		this.post = post;
	}

	public Employee getEmployee() {
		if (employee==null) {
			employee  =  new Employee();
			employee.setStatus(Tool.ADDED);
		}
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public String getSelecttext() {
		return selecttext;
	}
	public void setSelecttext(String selecttext) {
		this.selecttext = selecttext;
	}
	public List<Post> getListpost() {
		return listpost;
	}
	public void setListpost(List<Post> listpost) {
		this.listpost = listpost;
	}
	public Post getPosts() {
		return posts;
	}
	public void setPosts(Post posts) {
		this.posts = posts;
	}
	public List<Post> getListpostgroup() {
		return listpostgroup;
	}
	public void setListpostgroup(List<Post> listpostgroup) {
		this.listpostgroup = listpostgroup;
	}
	public List<Post> getListemployee() {
		return listemployee;
	}
	public void setListemployee(List<Post> listemployee) {
		this.listemployee = listemployee;
	}

	public List<Post> getListpostEmployeePkId() {
		return listpostEmployeePkId;
	}
	public void setListpostEmployeePkId(List<Post> listpostEmployeePkId) {
		this.listpostEmployeePkId = listpostEmployeePkId;
	}
	public Date getSelectDate() {
		if (selectDate==null) {
			selectDate  =  new Date();
		}
		return selectDate;
	}
	public void setSelectDate(Date selectDate) {
		this.selectDate = selectDate;
	}
	public List<String> getListbgColor() {
		return listbgColor;
	}
	public void setListbgColor(List<String> listbgColor) {
		this.listbgColor = listbgColor;
	}
	public List<Post> getListreceiverPkId() {
		return listreceiverPkId;
	}
	public void setListreceiverPkId(List<Post> listreceiverPkId) {
		this.listreceiverPkId = listreceiverPkId;
	}
	public List<Post> getListPostMapPkId() {
		return listPostMapPkId;
	}
	public void setListPostMapPkId(List<Post> listPostMapPkId) {
		this.listPostMapPkId = listPostMapPkId;
	}
	public List<UpgradingLog> getUpgradeLogs() {
		if(upgradeLogs==null)
			upgradeLogs = new ArrayList<>();
		return upgradeLogs;
	}
	public void setUpgradeLogs(List<UpgradingLog> upgradeLogs) {
		this.upgradeLogs = upgradeLogs;
	}
	public UpgradingLog getSelectUpgradeLog() {
		if(selectUpgradeLog==null)
			selectUpgradeLog = new UpgradingLog();
		return selectUpgradeLog;
	}
	public void setSelectUpgradeLog(UpgradingLog selectUpgradeLog) {
		this.selectUpgradeLog = selectUpgradeLog;
	}
	public boolean isLoginUser() {
		return loginUser;
	}
	public void setLoginUser(boolean loginUser) {
		this.loginUser = loginUser;
	}

}
