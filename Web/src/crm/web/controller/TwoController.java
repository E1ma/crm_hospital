package crm.web.controller;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;

import crm.businessentity.*;
import crm.businesslogic.interfaces.ILogicTwoLocal;

@SessionScoped
@ManagedBean(name = "twoController")
public class TwoController implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB(beanName = "LogicTwo")
	ILogicTwoLocal logicTwo;
	
	@ManagedProperty(value = "#{applicationController}")
	private ApplicationController applicationController;
	
	@ManagedProperty(value = "#{userController}")
	private UserSessionController userSessionController;
	
	//changePassword begin
	private String oldPassword;
	private String newPassword;
	private String newPasswordConfirm;
	//changePassword end
	
	public UserSessionController getUserSessionController() {
		return userSessionController;
	}

	public void setUserSessionController(UserSessionController userSessionController) {
		this.userSessionController = userSessionController;
	}
	
	public String getOldPassword() {
		return oldPassword;
	}
	
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	
	public String getNewPassword() {
		return newPassword;
	}
	
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	public String getNewPasswordConfirm() {
		return newPasswordConfirm;
	}
	
	public void setNewPasswordConfirm(String newPasswordConfirm) {
		this.newPasswordConfirm = newPasswordConfirm;
	}
	
	public String changePassword(){
		try{
			
			if(!getNewPassword().equals(getNewPasswordConfirm())){
				getUserSessionController().showMessage(35);
				return "";
			}
			
			if(!getUserSessionController().getLoggedInfo().getUser().getPassword().equals(Tool.MD5(getOldPassword()))){
				getUserSessionController().showMessage(34);
				return "";
			}
			
			logicTwo.changePassword(getUserSessionController().getLoggedInfo(), getNewPassword());
			
			getUserSessionController().logout();
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('prifileDialog').hide();");
		}catch(Exception ex){
			
		}
		
		return "login";
	}

	public ApplicationController getApplicationController() {
		return applicationController;
	}
	
	public void setApplicationController(
			ApplicationController applicationController) {
		this.applicationController = applicationController;
	}
}
